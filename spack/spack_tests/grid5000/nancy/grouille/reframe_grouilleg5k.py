"""
Grouille - Nancy - Grid5000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The packages in ``spack/spack_tests/grid5000/nancy/grouille/configs/spack.yml`` can be installed
using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c spack/spack_tests/grid5000/nancy/grouille/reframe_geminig5k.py --run

"""

import reframe as rfm

from modules.reframe_extras import SpackBase

# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class G5kGouilleSpackEnv(SpackBase):
    """Test to create Spack env on grouille cluster on Grid5000 at Nancy site"""

    descr = ('Create Spack environment and install packages for '
             'grouille cluster on Grid5000 at Nancy')

    valid_systems = ['nancy-g5k:grouille-gcc9-ompi4-eth-smod-nvgpu']
    valid_prog_environs = ['builtin']
    maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    cluster = 'nancy-g5k-grouille'
    spack_root = '/home/mpaipuri/spack'
    
    # This cluster has AMD chip. No point in building packages with ICC toolchain
    build_with_icc = False

    time_limit = '0d3h00m0s'
