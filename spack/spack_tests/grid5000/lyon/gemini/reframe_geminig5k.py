"""
Gemini - Lyon - Grid5000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The packages in ``spack/spack_tests/grid5000/lyon/gemini/configs/spack.yml`` can be installed using
following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c spack/spack_tests/grid5000/lyon/gemini/reframe_geminig5k.py --run

"""

import reframe as rfm

from modules.reframe_extras import SpackBase

# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class G5kGeminiSpackEnv(SpackBase):
    """Test to create Spack env on gemini cluster on Grid5000 at Lyon site"""

    descr = 'Create Spack environment and install packages for gemini cluster on Grid5000 at Lyon'

    valid_systems = ['lyon-g5k:gemini-gcc9-ompi4-ib-smod-nvgpu']
    valid_prog_environs = ['builtin']
    maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    cluster = 'lyon-g5k-gemini'
    spack_root = '/home/mpaipuri/spack'

    time_limit = '0d3h00m0s'
