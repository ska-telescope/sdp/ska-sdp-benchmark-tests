"""
Marconi100
~~~~~~~~~~~~~~~~

The packages in ``spack/spack_tests/marconi100/configs/spack.yml`` can be installed using
following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c spack/spack_tests/marconi100/reframe_juwelsbooster.py --run

"""

import reframe as rfm

from modules.reframe_extras import SpackBase

# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class Marconi100SpackEnv(SpackBase):
    """Test to create Spack env on Marconi100 cluster"""

    descr = 'Create Spack environment and install packages for Marconi100 cluster'

    valid_systems = ['marconi100:login']
    valid_prog_environs = ['builtin']
    maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    cluster = 'marconi100'
    spack_root = '/m100_work/Ppp4x_5680/spack'
    local = True
    
    # This cluster has AMD chip. No point in building packages with ICC toolchain
    build_with_icc = False
