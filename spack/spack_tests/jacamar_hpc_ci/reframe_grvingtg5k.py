"""
Jacamar HPC CI using Grvingt cluster at Nancy Grid5000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The packages in ``spack/spack_tests/jacamar_hpc_ci/configs/spack.yml`` can be installed
using following commands.

.. important::

   We use this cluster only for CI runs. We deploy a SLURM cluster on these nodes using CentOS 8
   and run our benchmarks on weekly cadence. Hence, all the spack config files are made for Cent OS
   rather than Debian which is the default images on these nodes.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c spack/spack_tests/jacamar_hpc_ci/reframe_grvingtg5k.py --run

"""

import reframe as rfm

from modules.reframe_extras import SpackBase

# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class JacamarHpcCiSpackEnv(SpackBase):
    """Test to create Spack env on grvingt SLURM cluster on Grid5000 for Jacamar HPC CI"""

    descr = ('Create Spack environment and install packages for grvingt '
             'cluster for jacamar HPC CI')

    valid_systems = ['jacamar-hpc-ci:login']
    valid_prog_environs = ['builtin']
    maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    cluster = 'jacamar-hpc-ci-grvingt'
    spack_root = '/shared/home/mahendrapaipuri/spack'
    spack_build_cache_dir = '/shared/spack-cache'

    local = True
