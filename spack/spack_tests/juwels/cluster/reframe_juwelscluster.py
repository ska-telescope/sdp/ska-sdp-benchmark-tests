"""
JUWELS Cluster
~~~~~~~~~~~~~~~~

The packages in ``spack/spack_tests/juwels/cluster/configs/spack.yml`` can be installed using
following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c spack/spack_tests/juwels/cluster/reframe_juwelscluster.py --run

"""

import reframe as rfm

from modules.reframe_extras import SpackBase

# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class JClusterSpackEnv(SpackBase):
    """Test to create Spack env on JUWELS Cluster"""

    descr = 'Create Spack environment and install packages for JUWELS Cluster partition'

    valid_systems = ['juwels-cluster:cluster-login']
    valid_prog_environs = ['builtin']
    maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    cluster = 'juwels-cluster'
    spack_root = '/p/project/prpb106/spack'
    local = True
