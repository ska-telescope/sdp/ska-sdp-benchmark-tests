# Installing packages using Spack

---
**NOTE**

This is legacy bash script to deploy Spack software stack on different platforms. Although this script still works, we recommend the users to use Spack environments provided for each platform that is registered in the test repository. New platforms can be easily added using the provided Spack environment files as templates and changing them accordingly. Moreover, Spack software building process using this environment is orchestrated using ReFrame test. 

---

As already stated in the documentation, SKA SDP benchmark tests can be run using the software stack provided on the platform or we can build our own software stack using Spack in the user space. This second approach will give us more control how the packages are build in terms of versions, configuration, *etc*. In order to simplify the package deploying, a bootstrapping script is provided which will install all the packages that are needed to run the benchmark tests. However, some inputs from the user is necessary to install the packages correctly.

The script is a simple bash script that can be run via CLI. In order to check all the available options use `-h` which will give following output:

```
Log is saved to: /home/mpaipuri/work/ska-sdp-benchmark-tests/spack/spack_install.log
Usage: build-spack-modules.bash [OPTIONS]

    OPTIONS:

    -d | --dry-run              dry run mode
    -s | --with-scheduler       Built OpenMPI with scheduler support
    -i | --with-ib              Built UCX with IB transports
    -o | --with-opa             Built OpenMPI with psm2 support (Optimized for Intel Omni-path)
    -c | --with-cuda            Build OpenMPI and UCX with cuda and other cuda related packages
    -t | --use-tcl              Use tcl module files [Default is lmod]
    -h | --help                 prints this help and exits


```

- `--dry-run` option is particularly useful. It emits the commands that will be executed without actually executing them.
- `--with-scheduler` will add scheduler support to OpenMPI. This should be turned on for the platforms that have batch schedulers like SLURM, PBS, LSF, *etc*.
- `--with-ib` will add Infiniband to OpenMPI. Use this when platform has IB support.
- `--with-opa` will add Intel OmniPath (OPA) support to OpenMPI.
- `--with-cuda` will build CUDA and other NVIDIA related packages. This option also builds OpenMPI and UCX (fabrics support of OpenMPI) with CUDA support.
- `--use-tcl` will skip configuration steps related to LMod hierarchical module system. Most of the HPC platform uses hierarchical module system and thus, this option is made available just for completeness.

---
**NOTE**

Options `-i` and `-o` cannot be used together as Spack builds OpenMPI with only one fabrics support at a time.

---

For example, if we want to install all the packages with scheduler and Infiniband support use following command

```
./build-spack-modules.bash -s -i
```

---
**NOTE**

If the front end login nodes and compute nodes have same architecture, we can run this script on the login nodes. However, if the login nodes are different from the compute nodes, we should run it on a compute node so that packages will be build for the compute node architecture. This can be done simply by executing the script within batch scheduler job.

---

## Toolchain

The script first builds a compiler toolchain using the system provided compiler toolchain and use the Spack built toolchain to compile all packages. Currently, GCC 9.3.0 is used as default toolchain. This can be changed by setting `TOOLCHAIN` variable on the command line. For instance, if we want to use GCC 10.1.0 as toolchain use in above example,

```
TOOLCHAIN="10.1.0" ./build-spack-modules.bash -s -i
```

---
**NOTE**

Make sure the chosen toolchain is different from the one provided on the system. If the user decided to use default toolchain of GCC 9.3.0 the system has the same installed as well, unload the module before running the script. That way Spack picks up the compiler from the OS. Even if the compiler on the OS is the same, simply choose a different toolchain from the OS. The final objective is to build all the packages in the user space.

---

## Packages

Currently, the packages that the script can install are as follows along with the default versions used.


|        Package       |   Version   |  Variable |
|:--------------------:|:-----------:|:---------:|
|          GCC         |    9.3.0    | TOOLCHAIN |
|    Intel Compiler    |   2021.4.0  |    ICC    |
|        OpenMPI       |    4.1.1    |   OMPI_4  |
|       Intel MPI      | 2019.10.317 | INTEL_MPI |
|         CUDA         |    11.4.0   |    CUDA   |
|         NCCL         |   2.9.9-1   |    NCCL   |
|          UCX         |    1.10.1   |    UCX    |
|         HDF5         |    1.10.7   |    HDF5   |
|         FFTW         |    3.3.9    |    FFTW   |
|       Intel MKL      |   2021.3.0  |    MKL    |
|         CMAKE        |    3.21.1   |   CMAKE   |
|          GIT         |    2.31.1   |    GIT    |
|        GIT LFS       |    2.11.0   |  GIT_LFS  |
|          IOR         |    3.3.0    |    IOR    |
|     MetaData Test    |    1.9.3    |   MDTEST  |
| Intel MPI benchmarks |    2019.6   |    IMB    |
| OSU Micro benchmarks |    5.7.1    |    OSU    |


The column variable gives variable for each package that can be used to override the default value from the command line. If the user wants to change the versions of multiple packages, it can be done by modifying the script directly.

## Post install

Once the installation process is finished successfully, we need to source the `$HOME/.bashrc` script to get all the modules available to use. By default the module names will include micro architecture like broadwell, zen2, *etc*. This is especially useful in the cases where there is a common home directory mounted via NFS for multiple clusters with different architectures. One example is [grid5000](https://www.grid5000.fr/w/Hardware) where multiple clusters on each site has a common frontend and home directories are mounted on that frontend. If we want to build modules for different clusters on the same home directory, we will face conflicts with module files. Having micro architectures within the name of module files will resolve those conflicts. If the user do not want to have those micro architecture names, we can remove them by modifying `$HOME/.spack/modules.yaml` file and removing `{architecture.target}` from following lines.

```
projections:
   ^cuda: '{name}/{version}-{architecture.target}-{^cuda.name}-{^cuda.version}'
   ^mpi: '{name}/{version}-{architecture.target}-{^mpi.name}-{^mpi.version}'
   all: '{name}/{version}-{architecture.target}'
```

## Caveats

- When building CUDA and related modules, we must be careful about the compatibility between CUDA and available NVIDIA drivers on the node. More information on the [compatibility](https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html#cublas-11.4.0) can be found on the NVIDIA website. One way to find this out is to run `nvidia-smi` command on the node which will give both driver and CUDA version on that node. We can simply use that CUDA version to make sure we will not face any issues.
- Currently, Spack do not provide [ROCm](https://rocmdocs.amd.com/en/latest/) as a variant while building UCX. As a result UCX finds this out at the configuration time and if it finds ROCm support, UCX is built with it. However, GDRCopy is [incompatible](https://github.com/spack/spack/issues/26980) with ROCm and this will result in installation failure. This has been already reported to Spack and [PR](https://github.com/spack/spack/pull/26992) is waiting to be merged.
