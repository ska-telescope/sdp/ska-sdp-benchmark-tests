#!/bin/bash

# additionally: create some symlink in $PATH pointing to /sbin/ldconfig.
# otherwise gdrcopy won't install.

set -e

# redirect stdout and stderr to log file
LOG_LOCATION=$PWD
exec > >(tee -i $LOG_LOCATION/spack_install.log)
exec 2>&1

echo "$(tput setaf 1)Log is saved to$(tput sgr0): $(tput setaf 3)$LOG_LOCATION/spack_install.log$(tput sgr0)"

scriptname=`basename $0`

# Declare command line options
DRY_RUN="NO"
SCHEDULER="NO"
IB_SUPPORT="NO"
OPA_SUPPORT="NO"
CUDA_SUPPORT="NO"

# Spack install base Command
SPACK_INSTALL="spack install -j6"
SPEC=""
VARS=""
DEPS=""

# Declare variables
SYS_COMPILER=""
ARCH=""
ARCH_GEN_TAR=""
MICRO_ARCH=""
OS_ARCH=""
PLAT_ARCH=""
ARCH_GEN=""

# Software versions
: ${TOOLCHAIN="gcc@9.3.0"}  # Compiler toolchain
: ${ICC="2021.3.0"}  # Intel compilers
: ${OMPI_4="4.1.1"}  # OpenMPI 4
: ${INTEL_MPI="2019.10.317"}  # Intel MPI
: ${CUDA:="11.4.0"}  # CUDA
: ${NCCL="2.9.9-1"}  # NCCL libs
: ${NCCLTESTS="2.0.0"}  # NCCL perf tests
: ${UCX="1.10.1"}  # UCX
: ${HDF5="1.10.7"}  # HDF5 Libs
: ${FFTW="3.3.9"}  # FFTW Libs
: ${MKL="2021.3.0"}  # Intel MKL libs
: ${CMAKE="3.21.1"}  # CMAKE
: ${GIT="2.31.1"}  # Git
: ${GIT_LFS="2.11.0"}  # Git-lfs
: ${IOR="3.3.0"}  # IOR benchmark
: ${MDTEST="1.9.3"}  # MDtest benchmark
: ${IMB="2019.6"}  # Intel MPI benchmarks
: ${OSU="5.7.1"}  # OSU benchmarks

#
# This function prints the script usage form
#
usage()
{
    cat <<EOF
Usage: $(tput setaf 1)$scriptname$(tput sgr0) $(tput setaf 3)[OPTIONS]$(tput sgr0)

    $(tput setaf 3)OPTIONS:$(tput sgr0)

    $(tput setaf 3)-d | --dry-run$(tput sgr0)              dry run mode
    $(tput setaf 3)-s | --with-scheduler$(tput sgr0)       Built OpenMPI with scheduler support
    $(tput setaf 3)-i | --with-ib$(tput sgr0)              Built UCX with IB transports
    $(tput setaf 3)-o | --with-opa$(tput sgr0)             Built OpenMPI with psm2 support (Optimized for Intel Omni-path)
    $(tput setaf 3)-c | --with-cuda$(tput sgr0)            Build OpenMPI and UCX with cuda and other cuda related packages
    $(tput setaf 3)-h | --help$(tput sgr0)                 prints this help and exits

EOF
} # end of usage

# Execute command or print command to execute
exec_cmd()
{
   if [ "$DRY_RUN" != "YES" ]; then
     eval $CMD
   else
     echo "Command to be executed is $(tput setaf 3)$CMD$(tput sgr0)"
   fi
}

# Convenience wrapper to execuate spack install commands
exec_install_cmd()
{
   CMD="$SPACK_INSTALL $SPEC %$TOOLCHAIN $VARS $DEPS"
   exec_cmd
}

exec_cuda_install_cmd()
{
   CMD="bwrap --dev-bind / / --tmpfs /tmp $SPACK_INSTALL $SPEC %$TOOLCHAIN $VARS $DEPS"
   exec_cmd
}

# Check passed options
check_options()
{
  if [[ "$IB_SUPPORT" == "YES" && "$OPA_SUPPORT" == "YES" ]]; then
    echo "$(tput setaf 3)WARNING: Both Infiniband and Omni-path support were asked. Choose only one!!$(tput sgr0)"
    exit 1
  fi
}

# Get OpenMPI hash
get_openmpi_hash()
{
  # If two OpenMPIs are installed on the system (one with cuda and one without cuda)
  # we look at all OpenMPI hashs and get the right one depending on CUDA_SUPPORT
  if [[ "$CUDA_SUPPORT" == "YES" ]]; then
    VAR_LOOKUP="+cuda"
  else
    VAR_LOOKUP="~cuda"
  fi
  for LINE in 2 3 4 5
  do
    PKG_SPEC=$(spack find -lv openmpi@$OMPI_4 | awk -v LN="$LINE" 'FNR==LN{print $2}')
    if [[ $PKG_SPEC == *$VAR_LOOKUP* ]]; then
      OMPI_HASH=$(spack find -lv openmpi@$OMPI_4 | awk -v LN="$LINE" 'FNR==LN{print $1}')
      break
    else
      echo "Hash not found for package openmpi@$OMPI_4 with variant $VAR_LOOKUP. Exiting...."
      if [ "$DRY_RUN" != "YES" ]; then exit 1; fi
    fi
  done
}


# Get compiler of the platform
get_system_compiler()
{
   echo "$(tput setaf 1)Finding system compiler$(tput sgr0)"
   CMD="spack compiler find"
   exec_cmd
#   SYS_COMPILER=$(spack compiler list | awk 'END{print $0}')
#   SYS_COMPILER='gcc@11.2.0'
   echo -e "System compiler found is ${SYS_COMPILER}\n"
}

# Get platform architecture
get_architecture()
{
   echo "$(tput setaf 1)Getting system architecture for Spack$(tput sgr0)"
   ARCH=$(spack arch)
   PLAT_ARCH=$(spack arch -p)
   OS_ARCH=$(spack arch -o)
   MICRO_ARCH=$(spack arch -t)
   ARCH_GEN_TAR=$(spack arch --known-targets | grep -B 100 $MICRO_ARCH | awk '{print $3}' | grep . | awk 'END{print $1}')
   ARCH_GEN="${PLAT_ARCH}-${OS_ARCH}-${ARCH_GEN_TAR}"
   echo -e "Architecture found is ${ARCH}\n"
}

# Sets install_missing_compilers to true so that missing compilers will be built automatically
copy_config_file_to_home()
{
   echo "$(tput setaf 1)Copy Spack config file to user home for customizing$(tput sgr0)"
   cp $SPACK_ROOT/etc/spack/defaults/config.yaml $HOME/.spack/
   CMD="sed -i 's/install_missing_compilers: false/install_missing_compilers: true/gI' $HOME/.spack/config.yaml"
   exec_cmd
   echo -e "Changed install_missing_compilers to True in config file\n"
}

# Install base gcc
install_toolchain()
{
   echo "$(tput setaf 1)Installing GCC Toolchain $TOOLCHAIN$(tput sgr0)"
   SPEC="$TOOLCHAIN"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed GCC Toolchain $TOOLCHAIN\n"
}

# Install Intel OneAPI compilers
install_intel_compilers()
{
   echo "$(tput setaf 1)Installing Intel OneAPI compilers $ICC$(tput sgr0)"
   SPEC="intel-oneapi-compilers@$ICC"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed OpenMPI Intel OneAPI compilers $ICC\n"
}

# Install OpenMPI 4.1.1 with UCX
install_openmpi4()
{
   echo "$(tput setaf 1)Installing OpenMPI $OMPI_4$(tput sgr0)"
   SPEC="openmpi@$OMPI_4+thread_multiple"
   VARS=""
   DEPS=""
   if [ "$CUDA_SUPPORT" == "YES" ]; then
     CUDA_HASH=$(spack find -l cuda@$CUDA | awk 'END{print $1}')
     SPEC+="+cuda"
     DEPS+=" ^cuda/$CUDA_HASH"
     DEPS+=" ^hwloc+nvml"
   fi
   if [ "$SCHEDULER" == "YES" ]; then
     VARS+=" schedulers=auto"
   fi
   if [ "$OPA_SUPPORT" == "YES" ]; then
     VARS+=" fabrics=psm2"
   fi
   exec_install_cmd
   get_openmpi_hash
   echo -e "Installed OpenMPI $OMPI_4\n"
}

# Install Intel MPI with verbs
install_intel_mpi()
{
   echo "$(tput setaf 1)Installing Intel MPI $INTEL_MPI$(tput sgr0)"
   SPEC="intel-mpi@$INTEL_MPI"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed Intel MPI $INTEL_MPI\n"

   echo "$(tput setaf 1)Installing Intel Oneapi MPI $ICC$(tput sgr0)"
   SPEC="intel-oneapi-mpi@$ICC"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed Intel Oneapi MPI $ICC\n"
}

# Install HDF5 WITHOUT MPI support
install_hdf5()
{
   echo "$(tput setaf 1)Installing HDF5 $HDF5$(tput sgr0)"
   SPEC="hdf5@$HDF5~mpi"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed HDF5 libraries\n"
}

# Install FFTW WITHOUT MPI support and WITH OpenMP support
install_fftw()
{
   echo "$(tput setaf 1)Installing FFTW $FFTW$(tput sgr0)"
   SPEC="fftw@$FFTW~mpi+openmp"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed FFTW libraries\n"
}

# Install Intel MKL OneAPI
install_mkl()
{
   echo "$(tput setaf 1)Installing Intel MKL OneAPI $MKL libs$(tput sgr0)"
   SPEC="intel-oneapi-mkl@$MKL"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed MKL libraries\n"
}

# Install CMake
install_cmake()
{
   echo "$(tput setaf 1)Installing CMake $CMAKE$(tput sgr0)"
   SPEC="cmake@$CMAKE"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed CMake libraries\n"
}

# Install Git and Git LFS
install_git()
{
   echo "$(tput setaf 1)Installing Git $GIT and Git LFS $GIT_LFS$(tput sgr0)"
   SPEC="git@$GIT"
   VARS=""
   DEPS=""
   exec_install_cmd
   SPEC="git-lfs@$GIT_LFS"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed Git and Git LFS\n"
}

install_boost()
{
   echo "$(tput setaf 1)Installing BOOST 1.77.0$(tput sgr0)"
   export GNU_VERSION='9.3.0'
   SPEC="boost@1.77.0+python+numpy"
   VARS=""
   DEPS=""
   exec_install_cmd
   echo -e "Installed BOOST 1.77.0\n"
}

# Install IOR and MDTest
install_ior_mdtest()
{
   echo "$(tput setaf 1)Installing IOR $IOR and MDTest $MDTEST$(tput sgr0)"
   SPEC="ior@$IOR+hdf5"
   VARS=""
   DEPS="^openmpi/$OMPI_HASH"
   exec_install_cmd
   SPEC="mdtest@$MDTEST"
   VARS=""
   DEPS="^openmpi/$OMPI_HASH"
   exec_install_cmd
   echo -e "Installed IOR and MDTest\n"
}

# Install MPI benchmarks
install_imb()
{
   echo "$(tput setaf 1)Installing Intel MPI benchmarks $IMB$(tput sgr0)"
   SPEC="intel-mpi-benchmarks@$IMB"
   VARS=""
   DEPS="^openmpi/$OMPI_HASH"
   exec_install_cmd
   echo -e "Installed Intel MPI benchmarks\n"
}

# Install OSU benchmarks
install_osu()
{
   echo "$(tput setaf 1)Installing OSU benchmarks $OSU$(tput sgr0)"
   SPEC="osu-micro-benchmarks@$OSU"
   VARS=""
   DEPS="^openmpi/$OMPI_HASH"
   if [ "$CUDA_SUPPORT" == "YES" ]; then
     SPEC+="+cuda"
     DEPS+=" ^cuda/$CUDA_HASH"
   fi
   exec_install_cmd
   echo -e "Installed OSU benchmarks\n"
}

#######################
# GPU related packages
########################

# Install CUDA
install_cuda()
{
   echo "$(tput setaf 1)Installing CUDA $CUDA$(tput sgr0)"
   SPEC="cuda@$CUDA"
   VARS=""
   DEPS=""
   exec_cuda_install_cmd
   CUDA_HASH=$(spack find -l $SPEC | awk 'END{print $1}')
   echo -e "Installed CUDA compiler\n"
}

install_nccl()
{
   echo "$(tput setaf 1)Installing NCCL $NCCL library$(tput sgr0)"
   SPEC="nccl@$NCCL"
   VARS=""
   DEPS="^cuda/$CUDA_HASH"
   exec_install_cmd
   echo -e "Installed NCCL library\n"
   # echo "$(tput setaf 1)Installing NCCL-tests $NCCLTESTS$(tput sgr0)"
   # OMPI_HASH=$(spack find -l openmpi@$OMPI_4 | awk 'END{print $1}')
   # CMD="spack install -j$(nproc) nccl-tests@$NCCLTESTS %$COMPILER_TOOLCHAIN ^cuda/$CUDA_HASH ^openmpi/$OMPI_HASH"
   # exec_cmd
   # echo -e "Installed NCCL-tests\n"
}

# Add module.yaml file to user home directory
add_module_config()
{
   echo "$(tput setaf 1)Adding modules.yaml file to user home$(tput sgr0)"

   if [ "$DRY_RUN" != "YES" ]; then
     cat >$HOME/.spack/modules.yaml <<EOF
modules:
 default:
  enable::
   - tcl
  tcl:
   blacklist:
    - go-bootstrap
    - autoconf
    - automake
    - berkeley-db
    - bison
    - diffutils
    - expat
    - findutils
    - flex
    - gdbm
    - gettext
    - gmp
    - libbsd
    - libedit
    - libffi
    - libidn2
    - libiconv
    - libmd
    - libnl
    - libpciaccess
    - libsigsegv
    - libunistring
    - libtool
    - libxml2
    - m4
    - mpc
    - mpfr
    - ncurses
    - openssh
    - openssl
    - pcre2
    - py-docutils
    - py-setuptools
    - pkgconf
    - readline
    - tar
    - util-linux-uuid
    - util-macros
    - xz
    - zlib
   whitelist:
    - gcc
    - openmpi
    - cuda
   all:
    environment:
      set:
       '{name}_ROOT': '{prefix}'
    filter:
      environment_blacklist:
        - 'MODULEPATH'
   hash_length: 0
   projections:
#      ^cuda: '{name}/{version}-{^cuda.name}-{^cuda.version}'
      ^mpi: '{name}/{version}-{^mpi.name}-{^mpi.version}'
      all: '{name}/{version}'
EOF
    fi

   echo -e "Added modules.yaml file to user home\n"

}

# Create module files
create_module_files()
{
   echo "$(tput setaf 1)Creating module files$(tput sgr0)"
   CMD="spack module tcl refresh --delete-tree -y"
   exec_cmd
   echo -e "Created module files\n"
}

### Main script ###

# shortopts="h,d,s,i,o,c:"
# longopts="help,dry-run,with-scheduler,with-ib,with-opa,compiler-toolchain"
#
# eval set -- $(getopt -o ${shortopts} -l ${longopts} \
#                      -n ${scriptname} -- "$@" 2> /dev/null)

while [ $# -ne 0 ]; do
    case $1 in
        -h | --help)
            usage
            exit 0 ;;
        -d | --dry-run) DRY_RUN="YES"
        ;;
        -s | --with-scheduler) SCHEDULER="YES"
        ;;
        -i | --with-ib) IB_SUPPORT="YES"
        ;;
        -o | --with-opa) OPA_SUPPORT="YES"
        ;;
        -c | --with-cuda) CUDA_SUPPORT="YES"
        ;;
        *)
            echo "[ERROR] ${scriptname}: Unrecognized argument \`$1'" >&2
            usage
            exit 1 ;;
    esac
    shift
done

echo -e "Starting build process....\n"

# Check options
check_options

module swap PrgEnv-cray PrgEnv-gnu
module load daint-gpu spack-config

# Start installation process
# Get system compiler and architecture type. Move config to user home and customize
#get_system_compiler
get_architecture
copy_config_file_to_home

#install_toolchain
#spack module tcl refresh --delete-tree -y
#module load gcc/9.3.0

# Install Intel OneAPI compilers
# This will install gcc compiler toolchain as well
install_intel_compilers

# Install CUDA
if [ "$CUDA_SUPPORT" == "YES" ]; then
  install_cuda
  install_nccl
fi

# Install root specs MPI
install_openmpi4
install_intel_mpi

# Install dependencies
install_hdf5
install_fftw
install_mkl

# Install micro benchmarks
install_ior_mdtest
install_imb
install_osu

# Install utilities
install_cmake
install_git
install_boost

# Add modules.yaml file to user home to create modules
add_module_config
create_module_files

echo "module load git-lfs" >> ~/.bashrc
spack clean -a