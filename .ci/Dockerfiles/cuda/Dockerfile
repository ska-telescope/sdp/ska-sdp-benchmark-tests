FROM nvidia/cuda:11.3.0-devel-ubuntu20.04
MAINTAINER SKAO

# Make non noninteractive installation
ENV DEBIAN_FRONTEND=noninteractive

# tzdata needs this info
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -y && apt-get install -y build-essential \
                                            git \
                                            git-lfs \
                                            gcc \
                                            wget \
                                            curl \
                                            sudo \
                                            libhdf5-dev \
                                            libfftw3-dev \
                                            libopenmpi-dev \
                                            libblas-dev \
                                            liblapack-dev \
                                            npm \
                                            cmake \
                                            python3 \
                                            python3-pip \
                                            python3-sphinx \
                                            doxygen \
                                            libssl-dev \
                                            uuid-dev \
                                            libgpgme11-dev \
                                            squashfs-tools \
                                            libseccomp-dev \
                                            pkg-config \
                                            # libcufft-dev-11-3 \
                                            # cuda-nvrtc-dev-11-3 \
                                            && apt-get clean


COPY requirements.txt /ci/
COPY unittests/resources/requirements.txt /ci/requirements-tests.txt

# Include CUDA libs in LD_LIBRARY_PATH
ENV LD_LIBRARY_PATH=/usr/local/cuda-11.3/compat:/usr/local/cuda/lib64

# Include Conda in PATH
ENV PATH /opt/anaconda/bin:$PATH

# Install Miniconda
RUN curl -o miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-py38_4.10.3-Linux-x86_64.sh && \
     bash miniconda.sh -f -b -p /opt/anaconda && \
     /opt/anaconda/bin/conda clean -tipy && \
     rm -f miniconda.sh && conda init
#
# # Install python and pip
RUN conda install --yes -c conda-forge python=3.8 pip

# Add Python script that update perflogs
COPY .ci/Dockerfiles/update-perflogs.py /ci/

# Install JUnit files merger
RUN npm install -g junit-report-merger

# Install jupyter nbconvert
RUN pip3 install --no-cache-dir jupyter nbconvert

# Install ipython kernel for updating notebooks
RUN ipython kernel install

# Install pip dependencies for repository
RUN pip3 install --no-cache-dir -r /ci/requirements.txt

# Install pip dependencies for tests
RUN pip3 install --no-cache-dir -r /ci/requirements-tests.txt
