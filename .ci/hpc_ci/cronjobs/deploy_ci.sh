#!/bin/bash

#OAR -n ska-sdp-benchmark-tests-ci
#OAR -l host=17,walltime=08:00:00
#OAR -p cluster='grvingt'
#OAR -t deploy
#OAR -t destructive
#OAR -q production
#OAR -O /home/mpaipuri/cronjobs/run_ci/logs/ska-sdp-benchmark-tests-ci.%jobid%.out
#OAR -E /home/mpaipuri/cronjobs/run_ci/logs/ska-sdp-benchmark-tests-ci.%jobid%.err

###################################################################################################
###  The objective of this script is to reserve bare metals, provision them with Cent OS, Install
###  SLURM cluster and GitLab runner and then trigger a pipeline to run SKA-SDP specific
###  benchmarks in SKA SDP Benchmark tests repository (https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests)
###  This is done via crontab in cron jobs which are trigger every Sunday evening
###
###  This is OAR job script that that will be submitted to OAR scheduler as cronjob
###  This script does the following:
###     - Reserve resources on Grvingt cluster at Nancy site in deploy mode
###     - Install CentOS 8 on these pre-provisioned nodes
###     - Installs SLURM cluster with first node as login/manager node and rest as compute nodes
###     - Installs Lmod module system
###     - Installs GitLab runner and configures Jacamar CI as custom runner
###     - Triggers a pipeline to run benchmarks on SKA SDP Benchmark tests repository
####################################################################################################

# Functions
#
# Function for printing and executing commands
function _cmd() {
  echo "+ $@";
  eval "$@";
}

# Print message and exit with code 1
function _fail {
  echo $1 >&2
  exit 1
}

# Function to retry a command 5 times if it fails
# To guard against trasient failures
function _retry {
  local n=1
  local max=5
  local delay=60
  while true; do
    _cmd "$@" && break || {
      if [[ $n -lt $max ]]; then
        ((n++))
        echo "Command failed. Attempt $n/$max:"
        sleep $delay;
      else
        _fail "The command has failed after $n attempts."
      fi
    }
  done
}

# Generate hostfile for playbook
function _gen_hostfile {
  local nodes=$(uniq $OAR_NODEFILE)
  local count=1
  echo "Generating host file for ansible playbook"
  for _node in $nodes
  do
    if [[ $count == 1 ]]; then
        echo "[manager]" > $PLAYBOOK_ROOT/hostfile
        echo "$_node" >> $PLAYBOOK_ROOT/hostfile
        echo "[nfs]" >> $PLAYBOOK_ROOT/hostfile
        echo "$_node" >> $PLAYBOOK_ROOT/hostfile
    else
        if [[ $count == 2 ]]; then
           echo "[compute]" >> $PLAYBOOK_ROOT/hostfile
        fi
        echo "$_node" >> $PLAYBOOK_ROOT/hostfile
    fi
    count=$((count+1))
    done
}

# Sleep till the end of reservation
function _sleep {
  while true; do
    sleep 3600
  done
}

# Print date
echo "=============================================="
echo "JobID: $OAR_JOB_ID"
echo "Time: `date`"
echo "Running on master node: `hostname`"
echo "Current directory: `pwd`"
echo "=============================================="

# Source $HOME/.bashrc to get conda on PATH
source $HOME/.bashrc

# Set directories
WORKDIR=$HOME/cronjobs/run_ci/tmp
REPO_ROOT=$WORKDIR/ska-sdp-benchmark-tests
PLAYBOOK_ROOT=$REPO_ROOT/.ci/hpc_ci/ansible

# Remove working directory if already exists
_cmd rm -rf $WORKDIR
_cmd mkdir -p $WORKDIR

# Install Cent OS 8 on the provisioned nodes
# We use Grid5000 provided images to install CentOS
# Use TMP partition to have more disk space. The default deploy partition has only around 30 GB
_retry kadeploy3 -f $OAR_NODEFILE -e centos8-x64-min -p TMP -k

# Clone SKA SDP Benchmark tests repository
_cmd git clone --depth 1 https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests.git $REPO_ROOT

# Create conda environment and install dependencies
# _cmd conda create -n ansible python=3.8 -y
_cmd conda activate ansible
_cmd TMPDIR=$WORKDIR pip install --no-cache-dir -r $PLAYBOOK_ROOT/requirements.txt

# Install Ansible Galax requirements
_cmd ansible-galaxy install -r $PLAYBOOK_ROOT/galaxy-requirements.yml

# Change directory to where ansible playbook is located
_cmd cd $PLAYBOOK_ROOT

# Make host file with first node as manager and nfs server and rest as compute nodes
_gen_hostfile

# Replace config file with the one that has prefilled variables
cp $HOME/cronjobs/run_ci/files/config.yml $PLAYBOOK_ROOT/config.yml

# Start ansible play
_retry ansible-playbook playbook.yml --user root -i hostfile --skip-tags "kubernetes,shell"

# Sleep till the end of reservation time to let CI jobs run
_sleep
