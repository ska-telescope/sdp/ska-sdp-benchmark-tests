# List of required packages
ansible==5.0.1
netaddr==0.8.0
python-gitlab==2.10.1
requests==2.26.0