#!/usr/bin/env python3
import sys
import logging
import gitlab

logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

# Global variables
URL='https://gitlab.com'

def main(token):
    """
    Main driver function to remove inactive jacamar runners
    
    :param token: Private token
    :type token: str
    """
    
    gl = gitlab.Gitlab(URL, private_token=token, ssl_verify=True)

    for runner in gl.runners.list():
        try:
            if runner.description == 'jacamar-runner' and not runner.online:
                _log.info(
                    'Deleting inactive runner %s with id %s' % (runner.description, runner.id)
                )
                runner.delete()
        except Exception as ex:
            _log.exception(ex)
            

if __name__ == '__main__':
    
    if len(sys.argv) < 2:
        _log.info('Usage is python %s <PRIVATE_TOKEN>. Exiting....' % sys.argv[0])
        sys.exit(1)
        
    token = sys.argv[1]
    
    # Entry point
    main(token)
