#!/bin/bash

##############################################################################################
#
# Post install script to assign static IP address to IPoIB interfaces on Grvingt nodes
#
###############################################################################################

# Get IPv4 of enp24sf0 interface
IP_ADDR=$(ip -f inet addr show enp24s0f0 | awk '/inet / {print $2}' | cut -d'/' -f1)
# Split the address into four blocks
IFS='.' read -r -a IP_ADDR_BLKS <<< "${IP_ADDR}"

# Get Infiniband hardware address
HW_ADDR=$(ip link show ib0 | awk '/infiniband / {print $2}')

# Write network script to /etc/sysconfig/network-scripts/ifcfg-ib0
cat << EOF > /etc/sysconfig/network-scripts/ifcfg-ib0
DEVICE=ib0
TYPE=InfiniBand
ONBOOT=yes
HWADDR=${HW_ADDR}
BOOTPROTO=none
IPADDR=${IP_ADDR_BLKS[0]}.18.${IP_ADDR_BLKS[2]}.${IP_ADDR_BLKS[3]}
PREFIX=24
BROADCAST=172.18.76.255
IPV4_FAILURE_FATAL=yes
IPV6INIT=no
MTU=65520
CONNECTED_MODE=yes
NAME=ib0
EOF

# Restart the interface
ifdown ib0
ifup ib0
