# Automate benchmark tests using GitLab CI and SLURM cluster

The objective of this infrastructure is to automate the benchmarking of the SDP softwares defined within the SKA SDP Benchmark tests repository using GitLab CI. More details on this infrastructure can be found at this [confluence](https://confluence.skatelescope.org/pages/viewpage.action?pageId=148819296) page. Typically we use GitLab CI with docker executor to create isolated environment for each job in the CI. This is not the best solution for computationally demanding workflows that need distributed environment.

The high level overview of architecture is shown below:

![CI/CD environment](img/ci_cd_environment.png)

We use [Jacamar CI](https://ecp-ci.gitlab.io/docs/releasenotes/jacamar/jacamar_0.1.0.html) to address the gap between GitLab CI and HPC environment. Essentially, Jacamar CI is developed using GitLab's custom executor with several enhanced security features to run the CI jobs in an HPC environment. It submits the jobs to underlying batch scheduler, waits till the jobs are finished and brings back the artifacts to GitLab. At the same time, all the jobs are downscoped from runner space (usually with sudo rights) to user space and thus, providing both security and isolation of the CI jobs.

Currently, we do not have any access to cloud resources like Azure and hence, in order to implement this architecture we use [Grid5000](https://www.grid5000.fr/w/Grid5000:Home) resources. Grid5000 enables users to reserve bare-metal nodes in "deploy" mode where the user can install provision the nodes with an OS and do experiments. To make our life simpler, Grid5000 support also provides us with OS provisioning tools and different OS environments that includes CentOS, Debian, Ubuntu, *etc*. Note that we use Grid5000 resources on an ad-hoc basis which means we reserve the resources and provision the OS when needed. This environment is not running all the time and hence, we do not set up Grafana/Prometheus component in it. If we get permanent resources on Azure, we can use this setup and include Grafana setup to monitor the performance metrics.

Once we have the nodes with a base OS running, we need to install SLURM cluster on these nodes and for that we use [ansible](https://docs.ansible.com/), a configuration management tool. The folder `ansible/` has necessary role and tasks files that can install a SLURM cluster using first node in the job reservation as SLURM control/login node and rest as compute nodes. Although, the roles have support for both CentOS 7 and 8, **we strongly** recommend to use CentOS 8. Also, these roles are capable of installing Kubernetes, although this has not been well tested. The base roles of this playbook are taken from [Dell HPC](https://github.com/dellhpc/omnia) and they are modified according to our needs.

The user needs to update the `ansible/config.yml` file with appropriate passwords and tokens before setting up this environment.

---
**NOTE**

The roles will create a normal user with username that should match with GitLab user name for Jacamar CI downscoping to work. If there are no users with username same as GitLab handle found in the deployed SLURM cluster, CI job will exit as failure.

---

Essentially, the ansible roles do the following:

- Install all required prerequisites
- Setup a NFS client server
- Install SLURM and start SLURM services
- Install LMod module system
- Create user, enable passwordless ssh for user and set up conda for the user
- Install Gitlab runner on login node and configure it with Jacamar CI
- Remove inactive existing Jacamar CI runners for the project
- Trigger a CI pipeline to run benchmarks

If we want to set up this environment on cloud service like Azure, we need to complement the existing ansible roles with the roles that provision the virtual machines and install base OS. Thus, the provided ansible roles are generic enough and can be used for different platforms. The driver script to kickstart the deployment of this environment is provided in `cronjobs/deploy_ci.sh`. This is essentially a job script that should be submitted to OAR scheduler on Grid5000 cluster at Nancy site.

We simply add a cron job on the frontend of Nancy cluster to run this job script on a weekly cadence which in turn will deploy this environment and triggers CI pipeline to run benchmarks. 
