""""This file contains the configuration of lyon grid5000 and their partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
gemini_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'gemini-grid5000.json'), 'r')
)

lyon_g5k_system_config = {
    'name': 'lyon-g5k',
    'descr': 'Grid5000 is a large-scale and flexible testbed for experiment-driven '
             'research: https://www.grid5000.fr/w/Grid5000:Home. This is Lyon '
             'partition: https://www.grid5000.fr/w/Lyon:Hardware',
    'hostnames': ['flyon', 'gemini-*'],
    'modules_system': 'lmod',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Generic frontend node for lyon clusters',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': ['builtin', 'gnu'],
            'prepare_cmds': [
                'module purge',
            ],
        },
        {
            'name': 'gemini-gcc9-ompi4-ib-smod-nvgpu',
            'descr': 'Gemini cluster at the lyon site with 3x100Gb Infiniband (X-5), '
                     'Generic configuration provided by g5k (2 x Intel E5-2698 v4 Broadwell, '
                     '2.20GHz, 2 CPUs/node, 20 cores/CPU ) '
                     'https://www.grid5000.fr/w/Lyon:Hardware#gemini',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'gemini\'',
                '-t exotic',
            ],
            'environs': [
                'builtin',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'processor': {
                **gemini_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '70',
                    'num_devices': 8,
                },
            ],
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '512000000000',  # total memory in bytes
            },
        },
        {
            'name': 'gemini-gcc9-ompi4-ib-umod-nvgpu',
            'descr': 'Gemini cluster at the lyon site with 3x100Gb Infiniband (X-5), '
                     'gcc 9.3.0 and OpenMPI 4.1.1 with ucx support (2 x Intel E5-2698 v4 Broadwell, '
                     '2.20GHz, 2 CPUs/node, 20 cores/CPU ) '
                     'https://www.grid5000.fr/w/Lyon:Hardware#gemini',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'gemini\'',
                '-t exotic',
            ],
            'environs': [
                'builtin',
                'cng-test',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'gcc/9.3.0', ' git/2.31.1',
                'git-lfs/2.11.0', 'openmpi/4.1.1-cuda-11.2.0'
            ],
            'processor': {
                **gemini_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '70',
                    'num_devices': 8,
                },
            ],
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/lyon-g5k-gemini/lmod/linux*/Core',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '512000000000',  # total memory in bytes
            },
        },
        {
            'name': 'gemini-icc21-impi21-ib-umod-nvgpu',
            'descr': 'Gemini cluster at the lyon site with 3x100Gb Infiniband (X-5), '
                     'ICC 2021.4.0 and Intel MPI 2021.4.0 (2 x Intel E5-2698 v4 '
                     'Broadwell, 2.20GHz, 2 CPUs/node, 20 cores/CPU ) '
                     'https://www.grid5000.fr/w/Lyon:Hardware#gemini',
            'scheduler': 'oar',
            'launcher': 'mpiexec',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'dahu\'',
                '-t exotic',
            ],
            'environs': [
                'builtin',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'intel-oneapi-compilers/2021.4.0', ' git/2.31.1',
                'git-lfs/2.11.0', 'intel-oneapi-mpi/2021.4.0',
            ],
            'processor': {
                **gemini_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '70',
                    'num_devices': 8,
                },
            ],
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/lyon-g5k-gemini/lmod/linux*/Core',
                'mpiexec () { command $(which mpiexec) -genvall -f $OAR_NODEFILE '
                '-launcher ssh -launcher-exec /usr/bin/oarsh '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '512000000000',  # total memory in bytes
            },
        },
    ]
}
