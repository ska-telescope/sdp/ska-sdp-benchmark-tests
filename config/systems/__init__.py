""""This folder contains the configuration of systems and their partitions"""

__all__ = [
    'alaska',
    'csd3',
    'grenoble_g5k',
    'nancy_g5k',
]
