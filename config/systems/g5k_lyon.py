""""
This file contains the configuration of Grid5000 clusters at ENS Lyon
"""

import os
from pathlib import Path
import json

# Clusters topologies
clusters = ["flyon", "nova", "neowise", "gemini"]
g5k_lyon_topos = {}
root_dir = Path(__file__).parent.parent.parent
for cluster in clusters:
    g5k_lyon_topos[cluster] = json.load(
        open(os.path.join(root_dir, "topologies", f"g5k-lyon-{cluster}.json"), "r")
    )

# System configuration
g5k_lyon_system_config = {
    "name": "g5k-lyon",
    "descr": "Grid5000 cluster at ENS Lyon site",
    "hostnames": ["nova-*"],
    "modules_system": "lmod",
    "partitions": [
        # Front-end node
        {
            "name": "login",
            "descr": "Login node of Grid5000 at Lyon",
            "scheduler": "local",
            "launcher": "local",
            "environs": [
                "builtin",
            ],
            "processor": {
                **g5k_lyon_topos["flyon"],
            },
            "max_jobs": 4,
            "modules": [
                "gcc/10.4.0_gcc-10.4.0",
                "openmpi/4.1.4_gcc-10.4.0",
            ],
        },

        # Nova cluster (INTEL CPU)
        {
            "name": "nova-gcc9-ompi4-eth-smod",
            "descr": "Dell PowerEdge R430, " 
                    "Intel Xeon E5-2620, 64 GiB memory, "
                    "Broadwell, 2.10GHz, 8x Cores/CPU, 2x CPUs/Node, 22x Nodes, "
                    "eth0/enp5s0f0, Intel Ethernet 10G 2P X520 Adapter, "
                    "598 GB HDD RAID-0 (2 disks), "
                    "https://www.grid5000.fr/w/Lyon:Hardware#nova",            
            "scheduler": "oar",
            "launcher": "mpiexec",
            "time_limit": "0d8h0m0s",
            "access": [
                "-p nova"
            ],
            "max_jobs": 10,
            "environs": [
                "gnu",
                "numpy",
            ],
            "modules": [
            ],
            "processor": {
                **g5k_lyon_topos["nova"],
            },
            "prepare_cmds": [
            ],
            "extras": {
                "mem": "32000000000"
            },
            "devices": [
                {
                },
            ],
            "env_vars": [
                ["SCRATCH_DIR", "/tmp/"], 
            ],
        },

        # Neowise cluster (AMD GPU - AMD CPU)
        {
            "name": "neowise-gcc-ompi4-ib-smod",
            "descr": "AMD-Penguin Computing, "
                    "8x AMD Radeon Instinct MI50 32 GiB"
                    "AMD EPYC 7642, 512 GiB memory, "
                    "Zen 2, 48x Cores/CPU, 1x CPU/Node, 10x Nodes, "
                    "Ethernet 10 Gbps, Infiniband 100 Gbps"
                    "1.92 TB SSD NVME Samsung, ",
            "scheduler": "oar",
            "launcher": "mpiexec",
            "time_limit": "0d2h0m0s",
            "access": [
                "-t exotic",
                "-p neowise",
            ],
            "max_jobs": 10,
            "environs": [
                "gnu",
                "numpy",
            ],
            "modules": [],
            "processor": {
                **g5k_lyon_topos["neowise"],
            },
            "prepare_cmds": [],
            "extras": {
                "mem": "512000000000",
            },
            "devices": [
                {
                },
            ],
            "env_vars": [
                ["SCRATCH_DIR", "/tmp/"], 
            ],
        },

        # Gemini cluster (Nvidia DGX)
        {
            "name": "gemini-gcc9-ompi4-ib-smod",
            "descr": "Nvidia DGX-1,"
                    "2x Nodes Nvidia DGX-1, "
                    "8x Nvidia Tesla V100 GPU (Compute Capability: 7.0), "
                    "Intel Xeon E5-2698, 512 GiB, "
                    "Broadwell, 2.20Hz, 20x Cores/CPU, 2x CPUs/Node, 4x GPUs/Node, "
                    "Infiniband 100 Gbps, ",
            "scheduler": "oar",
            "launcher": "mpiexec",
            "time_limit": "0d2h0m0s",
            "access": [
                "-t exotic",
                "-p gemini",
            ],
            "max_jobs": 10,
            "environs": [
                "gnu",
                "numpy",
            ],
            "modules": [],
            "processor": {
                **g5k_lyon_topos["gemini"],
            },
            "prepare_cmds": [],
            "extras": {
                "mem": "512000000000",
            },
            "devices": [
                {
                },
            ],
            "env_vars": [
                ["SCRATCH_DIR", "/tmp/"]
            ]
        }
    ]
}

