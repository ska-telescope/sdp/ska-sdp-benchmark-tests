""""This file contains the configuration of Jean-Zay at IDRIS and its partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
jeanzay_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'jeanzay-login.json'), 'r')
)
jeanzay_compute_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'jeanzay-compute.json'), 'r')
)
jeanzay_system_config = {
    'name': 'jeanzay',
    'descr': 'Jean-Zay cluster at IDRIS',
    'hostnames': ['jean-zay*'],
    'modules_system': 'tmod4',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Login node of Jean-Zay @ IDRIS',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': [
                'builtin',
                'gnu',
            ],
            'processor': {
                **jeanzay_login_topo,
            },
            # TBC
            'max_jobs': 4,
            'modules': [
                'gcc/8.3.1', 'git/2.31.1',
                'git-lfs/3.0.2', #'openmpi/4.1.0'
            ],
        },
        # Partition with lots of CPU resources for short duration
        {
            'name': 'jeanzay-gcc8-ompi4-opa-smod-cpu-short',
            'descr': 'Jean-Zay CPU partition/qos for short jobs with many nodes',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d10h0m0s',
            'access': [
                # Uses the "normal" slurm queue (512 nodes / 20h time limit)
                '--partition=cpu_p1',
                '--qos=qos_cpu-t3',

                # Execution is exclusive for all jobs requiring more than one node
                # (make it exclusive for all jobs)
                '--exclusive',

                # SCOOP account on Jean Zay. Contact Shan Mignot <shan.mignot@oca.eu> for details
                '--account=nhx@cpu',

                # 40 cores per node, maximum 40 tasks (overwrites ntasks and ntasks-per-node
                # coming from the topology file)
                # Can be overwritten on a per-test basis
                '--ntasks-per-core=1',
                '--ntasks=40',
                '--ntasks-per-node=40'
            ],
            # Max number of concurrent tests (ie. benchmarks for us) run by reframe
            'max_jobs': 10,
            'environs': [
                'builtin',
                'gnu',
                'numpy',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'rapthor',
                'gnu-stream',
                'babel-stream-omp',
                'babel-stream-tbb',
                'ior',
                'intel-hpl'
            ],
            'modules': [
                'gcc/8.3.1',
                'git/2.31.1',
                'git-lfs/3.0.2',
            ],
            'processor': {
                **jeanzay_compute_topo,
            },
            'prepare_cmds': [
                'export RAPTHOR_DIR=/gpfswork/rech/nhx/ubr63rd/Rapthor/ska-sdp-wflow-low-selfcal',
                'export INPUT_MS_PATH=/gpfswork/rech/nhx/commun/LOFAR_code/LOFAR_data/test_rapthor/midbands_averaged.ms',
                'source $RAPTHOR_DIR/../../spack/spack181/share/spack/setup-env.sh',
            ],
            'extras': {
                'mem': '206158430208' # total memory in bytes (per node)
            },
            'devices': [
                {
                },
            ],
            'env_vars': [
                ['SCRATCH_DIR', str(os.getenv('ALL_CCFRSCRATCH'))],
                ['SDPTESTS_POWER_LIBPOWERSENSOR_PATH', '/users/mstutz/bin/libpowersensor/build'],
            ],
        },
        # Partition with less resources for long duration
        {
            'name': 'jeanzay-gcc8-ompi4-opa-smod-cpu-long',
            'descr': 'Jean-Zay CPU partition/qos for long jobs with few nodes',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d100h0m0s',
            'access': [
                # Uses a dedicated slurm queue (4 nodes / 100h time limit)
                '--partition=cpu_p1',
                '--qos=qos_cpu-t4',

                # Execution is exclusive for all jobs requiring more than one node
                # (make it exclusive for all jobs)
                '--exclusive',

                # SCOOP account on Jean Zay. Contact Shan Mignot <shan.mignot@oca.eu> for details
                '--account=nhx@cpu',

                # 40 cores per node, maximum 40 tasks (overwrites ntasks and ntasks-per-node
                # coming from the topology file)
                # Can be overwritten on a per-test basis
                '--ntasks-per-core=1',
                '--ntasks=40',
                '--ntasks-per-node=40'
            ],
            # Max number of concurrent tests (ie. benchmarks for us) run by reframe
            'max_jobs': 10,
            'environs': [
                'builtin',
                'gnu',
                'numpy',
                #'imaging-iotest',
                #'imaging-iotest-mkl',
                'rapthor',
            ],
            'modules': [
                'gcc/8.3.1',
                'git/2.31.1',
                'git-lfs/3.0.2',
            ],
            'processor': {
                **jeanzay_compute_topo,
            },
            'prepare_cmds': [
                'export RAPTHOR_DIR=/gpfswork/rech/nhx/ubr63rd/Rapthor',
                'source $RAPTHOR_DIR/../spack/spack181/share/spack/setup-env.sh',
            ],
            'extras': {
                'mem': '206158430208' # total memory in bytes (per node)
            },
            'devices': [
                {
                },
            ],
            'env_vars': [
                ['SCRATCH_DIR', str(os.getenv('ALL_CCFRSCRATCH'))],
                ['SDPTESTS_POWER_LIBPOWERSENSOR_PATH', '/users/mstutz/bin/libpowersensor/build'],
            ],
        },


        # Partition with GPU  resources
        {
            'name': 'jeanzay-gcc8-ompi4-opa-smod-gpu',
            'descr': 'Jean-Zay GPU partition',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d100h0m0s',
            'access': [
                # Uses a dedicated slurm queue
                '--partition=gpu_p13',
                '--qos=qos_gpu-t4',
                '--gres=gpu:1',

                # Execution is exclusive for all jobs requiring more than one node
                # (make it exclusive for all jobs)
                '--exclusive',

                # SCOOP account on Jean Zay. Contact Shan Mignot <shan.mignot@oca.eu> for details
                '--account=nhx@v100',

                # 40 cores per node, maximum 40 tasks (overwrites ntasks and ntasks-per-node
                # coming from the topology file)
                # Can be overwritten on a per-test basis
                '--ntasks-per-core=2',
            ],
            # Max number of concurrent tests (ie. benchmarks for us) run by reframe
            'max_jobs': 100,
            'environs': [
                'builtin',
                'numpy_cuda',
                'babel-stream-cuda',
                'idg-test',
                'nccl-test',
                'cng-test',

            ],
            'modules': [
                'gcc/8.3.1', 'git/2.31.1',
                'git-lfs/3.0.2', 'openmpi/4.1.0'
            ],
            'processor': {
                **jeanzay_compute_topo,
            },
            'prepare_cmds': [
            ],
            'extras': {
                'mem': '200952418304',# total memory in bytes (per node)
                'gpu_mem': '17070817280',
                'internet': True
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '60',  # compute capability of 6.0 (https://developer.nvidia.com/cuda-gpus)
                    'num_devices': 1,
                },
            ],
        },



    ]
}

""""This file contains the configuration of Jean-Zay at IDRIS and its partitions"""
