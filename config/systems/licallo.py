""""This file contains the configuration of Licallo at OCA a"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
licallo_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'licallo-login.json'), 'r')
)
licallo_compute_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'licallo-compute.json'), 'r')
)
licallo_system_config = {
    'name': 'licallo',
    'descr': 'Licallo cluster',
    'hostnames': ['pollux'],
    'modules_system': 'lmod', #cmd: module --version
    'partitions': [
        {
            'name': 'login',
            'descr': 'Login node of Licallo',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': [
                'builtin',
                'gnu',
            ],
            'processor': {
                **licallo_login_topo,
            },
            # TBC
            'max_jobs': 4,
            'modules': [
                'gcc/12.2.0',
                'git/2.39.1',
            ],
        },
        # Partition with lots of CPU resources for short duration
        {
            'name': 'licallo-gcc12-lmod-cpu-short',
            'descr': 'licallo CPU partition for short jobs with many nodes',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                # Uses the "normal" slurm queue 
                '--partition=short',
                #'--qos=',

                # Execution is exclusive for all jobs requiring more than one node
                # (make it exclusive for all jobs)
                '--exclusive',

                # SCOOP account on Licallo. Contact Shan Mignot <shan.mignot@oca.eu> for details
                #'--account= ?

                # 40 cores per node, maximum 40 tasks (overwrites ntasks and ntasks-per-node
                # coming from the topology file)
                # Can be overwritten on a per-test basis
                '--ntasks-per-core=1',
            ],
            # Max number of concurrent tests (ie. benchmarks for us) run by reframe
            'max_jobs': 10,
            'environs': [
                'builtin',
                'gnu',
                'numpy',
                'gnu-stream',
                'babel-stream-omp',
                'gnu-hpcg',
                'intel-hpcg',
                'intel-hpl',
                'imb',
            ],
            'modules': [
                'gcc/12.2.0', 
                'git/2.39.1',
                
            ],
            'processor': {
                **licallo_compute_topo,
            },
            'prepare_cmds': [
            ],
            'extras': {
                'mem': '201051394048' # total memory in bytes (per node) ==> cmd: free -b
            },
            'devices': [
                {
                },
            ],
            'env_vars': [
            ],
        },
       # Partition with less resources for long duration
        {
            'name': 'licallo-gcc12-lmod-cpu-long',
            'descr': 'licallo CPU for long jobs with few nodes',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '1d00h0m0s',
            'access': [
                # Uses a dedicated slurm queue (4 nodes / 100h time limit)
                '--partition=besteffort',
                #'--qos=', #cmd: scontrol show partition=<nom_de_la_partition>

                # Execution is exclusive for all jobs requiring more than one node
                # (make it exclusive for all jobs)
                '--exclusive',

                # SCOOP account on Licallo. Contact Shan Mignot <shan.mignot@oca.eu> for details
                #'--account=',

                # 40 cores per node, maximum 40 tasks (overwrites ntasks and ntasks-per-node
                # coming from the topology file)
                # Can be overwritten on a per-test basis
                '--ntasks-per-core=1',
            ],
            # Max number of concurrent tests (ie. benchmarks for us) run by reframe
            'max_jobs': 10,
            'environs': [
                'builtin',
                'gnu',
                'numpy',
            ],
            'modules': [
                'gcc/12.2.0',
                'git/2.39.1',
                
            ],
            'processor': {
                **licallo_compute_topo,
            },
            'prepare_cmds': [
            ],
            'extras': {
                'mem': '201051394048' # total memory in bytes (per node)
            },
            'devices': [
                {
                },
            ],
            'env_vars': [
            ],
        },

    ]
}

""""This file contains the configuration of Licallo and its partitions"""
