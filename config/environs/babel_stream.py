""""This file contains the environment config for babel stream benchmark"""

babel_stream_environ = [
    {
        'name': 'babel-stream-omp',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.1',
        ],
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
            # <end - alaska partitions>
            'grenoble-g5k:dahu-gcc9-ompi4-opa-umod',
            # <end - grenoble partitions>
            'juwels-booster:booster-gcc9-ompi4-ib-umod',
            # <end - juwels partitions>
            'nancy-g5k:gros-gcc9-ompi4-eth-umod',
            'nancy-g5k:grouille-gcc9-ompi4-eth-umod',
            # <end - nancy partitions>
        ],
    },
    {
        'name': 'babel-stream-omp',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs partitions>
        ],
    },

    {
        'name': 'babel-stream-omp',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.20.0',
        ],
        'target_systems': [
            'licallo',
            # <end - licallo partitions>
        ],
    },

    {
        'name': 'babel-stream-tbb',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.1',
            'intel-oneapi-tbb/2021.4.0',
        ],
        'target_systems': [
            'alaska:compute-icc21-impi21-roce-umod',
            # <end - alaska partitions>
            'grenoble-g5k:dahu-icc21-impi21-opa-umod',
            # <end - grenoble-g5k partitions>
            'juwels-cluster:batch-icc21-impi21-ib-umod',
            # <end - juwels partitions>
            'nancy-g5k:gros-icc21-impi21-eth-umod',
            # <end - nancy-g5k partitions>
        ],
    },
    {
        'name': 'babel-stream-tbb',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.4',
            'intel-oneapi-tbb/2021.4.0',
        ],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs partitions>
        ],
    },
    {
        'name': 'babel-stream-cuda',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.1', 'cuda/11.4.0',
        ],
        'target_systems': [
            'juwels-booster:booster-gcc9-ompi4-ib-umod-nvgpu',
            # <end - juwels partitions>
        ],
    },
    {
        'name': 'babel-stream-cuda',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.1', 'cuda/11.2.0',
        ],
        'target_systems': [
            'nancy-g5k:grouille-gcc9-ompi4-eth-umod-nvgpu',
            # <end - nancy partitions>
        ],
    },
    {
        'name': 'babel-stream-cuda',
        'modules': ['cudatoolkit/11.1.0_3.39-4.1__g484e319'],
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs partitions>
        ],
    },

    {
        'name': 'babel-stream-omp',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.3',
        ],
        'target_systems': [
            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-short',
            # <end - jeanzay partitions>
        ],
    },

    {
        'name': 'babel-stream-tbb',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.3',
            'intel-oneapi-tbb/2021.9',
        ],
        'target_systems': [
            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-short',
            # <end - jeanzay partitions>
        ],
    },

    {
        'name': 'babel-stream-cuda',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cmake/3.21.3',
            'cuda/11.2',
        ],
        'target_systems': [
            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-gpu',
            # <end - juwels partitions>
        ],
    },

    {
        'name': 'babel-stream-tbb',
        'cc': 'icc',
        'cxx': 'icpc',
        'ftn': 'ifort',
        'modules': [
            'cmake/3.26.2',
            'intel-oneapi/2023.1.0',
        ],
        'target_systems': [
            'licallo',
            # <end - licallo partitions>
        ],
    },

]
