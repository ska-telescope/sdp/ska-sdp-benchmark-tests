""""This file contains the environment config for HPCG benchmark"""

hpcg_environ = [
    {
        'name': 'intel-hpcg',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-oneapi-mkl/2021.3.0',
        ],
        'env_vars': [
            ['KMP_AFFINITY', 'granularity=fine,compact,1,0'],  # README file suggests this setting
            ['XHPCG_BIN', '$MKLROOT/benchmarks/hpcg/bin/xhpcg_avx2'],
        ],
        'target_systems': [
            'grenoble-g5k:dahu-icc21-impi21-opa-umod',
            # <end - grenoble-g5k partitions>
            'nancy-g5k:gros-icc21-impi21-eth-umod',
            # <end - nancy-g5k partitions>
        ],
    },
    {
        'name': 'intel-hpcg',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-oneapi-mkl/2021.3.0',
        ],
        'env_vars': [
            # README file suggests this setting
            ['KMP_AFFINITY', 'granularity=fine,compact,1,0'],
            # JUWELS cluster has Skylake processors. We use binary specific to that micro architecture
            ['XHPCG_BIN', '$MKLROOT/benchmarks/hpcg/bin/xhpcg_skx'],
            # ['UCX_TLS', 'ud,rc,dc,self']
        ],
        'target_systems': [
            'alaska:compute-icc21-impi21-roce-umod',
            # <end - alaska partitions>
            'juwels-cluster:batch-icc21-impi21-ib-umod',
            # <end juwels partitions>
        ],
    },
    {
        'name': 'intel-hpcg',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
        ],
        'env_vars': [
            ['KMP_AFFINITY', 'granularity=fine,compact,1,0'],  # README file suggests this setting
            ['XHPCG_BIN', '$MKLROOT/benchmarks/hpcg/bin/xhpcg_avx2'],
        ],
        'target_systems': [
            'cscs-daint:daint-icc21-impi21-ib-umod-gpu',
            # <end - cscs partitions>
        ],
    },
    {
        'name': 'gnu-hpcg',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'target_systems': [
            'juwels-booster:booster-gcc9-ompi4-ib-umod',
            # <end - juwels partitions>
        ],
    },
    {
        'name': 'gnu-hpcg',
        'cc': 'cc',
        'cxx': 'CC',
        'ftn': 'ftn',
        'modules':[
            'PrgEnv-gnu/6.0.10'
        ],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs-daint partitions>
        ],
    },
    {
        'name': 'xlc-hpcg',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'target_systems': [
            'marconi100:prod-xl16-smpi10-ib-smod-small-nvgpu',
            # <end - marconi100 partitions>
        ],
    },

            {
                'name': 'gnu-hpcg',
                'modules': [
                    'openmpi/4.1.5',
                ],
                'cc': 'mpicc',
                'cxx': 'mpicxx',
                'ftn': 'mpif90',
                'target_systems': [
                    'licallo',
                    # <end - licallo partitions>
        ],
            },

    {
        'name': 'intel-hpcg',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        

        'modules': [
            'intel-oneapi/2023.1.0',
            'intel-oneapi-mpi/2021.9.0',
        ],

        'env_vars': [
            ['KMP_AFFINITY', 'granularity=fine,compact,1,0'],  # README file suggests this setting
            ['XHPCG_BIN', '$MKLROOT/benchmarks/hpcg/bin/xhpcg_avx'],
        ],
        'target_systems': [
            'licallo',
            # <end - licallo partitions>

        ],
    },

    {
        'name': 'intel-hpcg',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-mpi',
            'intel-oneapi-mkl',
        ],
        'env_vars': [
            ['KMP_AFFINITY', 'granularity=fine,compact,1,0'],  # README file suggests this setting
            ['XHPCG_BIN', '$MKLROOT/benchmarks/hpcg/bin/xhpcg_avx2'],
        ],
        'target_systems': [
            'jeanzay',
            # <end - jeanzay partitions>
        ],
    },
    {
        'name': 'gnu-hpcg',
        'modules': [
            'openmpi',
        ],

        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'target_systems': [
            'jeanzay',
            # <end - jeanzay partitions>
        ],
    },

    

]

