funclib_environ = [
    {
        'name': 'funclib-test',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
           'cmake/3.21.4',
        ],
        'target_systems': [
            # todo: change
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu'
        ]
    }
]