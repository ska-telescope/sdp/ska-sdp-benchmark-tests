""""This file contains the environment config for GPU direct RDMA test"""


gpu_rdma_test_environ = [
    {
        'name': 'gpu-direct-rdma',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
           'osu-micro-benchmarks/5.7.1-cuda-11.4.0',
        ],
        'target_systems': [
            'juwels-booster:booster-gcc9-ompi4-ib-umod-nvgpu',
            # <end - juwels partitions>
        ],
    },
    {
        'name': 'gpu-direct-rdma',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
           'osu-micro-benchmarks/5.7.1',
        ],
        'target_systems': [
            'cscs-daint:daint-icc21-impi21-ib-umod-gpu',
            # <end - cscs daint partitions>
        ],
    },
]

