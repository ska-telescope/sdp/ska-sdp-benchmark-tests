""""This file contains the environment config for NCCL tests"""


nccl_test_environ = [
    {
        'name': 'nccl-test',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
           'cuda/11.4.0', 'nccl/2.9.9-1-cuda-11.4.0',
        ],
        'target_systems': [
            'juwels-booster:booster-gcc9-ompi4-ib-umod-nvgpu',
            # <end juwels partitions>
            'nancy-g5k:grouille-gcc9-ompi4-eth-umod-nvgpu',
            # <end - nancy partitions>
        ],
    },
    {
        'name': 'nccl-test',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cudatoolkit',
            'nccl/2.9.9-1',
        ],
        'target_systems': [
            'cscs-daint:daint-icc21-impi21-ib-umod-gpu',
            # <end - cscs daint partitions>
        ],
        'env_vars': [
           ['MPI_ROOT', '$CRAY_MPICH2_DIR'],
           ['CUDA_ROOT', '$CUDA_HOME']
        ],
    },
]
