""""This file contains the environment config for Python Numpy tests"""


numpy_environ = [
    {
        'name': 'numpy',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
            # <end - alaska partitions>
            'grenoble-g5k:dahu-gcc9-ompi4-opa-umod',
            # <end - grenoble partitions>
            'juwels-cluster:batch-gcc9-ompi4-ib-umod',
            # <end - juwels partitions>
            'nancy-g5k:gros-gcc9-ompi4-eth-umod',
            # <end - nancy partitions>
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs daint partitions>
            'jeanzay',
#            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-short',
#            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-long',
            # <end - jeanzay partitions>
            "g5k-lyon",
            # <end - g5k clusters>
            "licallo",
            # <end - licallo partitions>
            "csd3",
            # <end - csd3 partitions>
        ],
    },
]
