""""This file contains the environment config for stream benchmark"""


stream_environ = [
    {
        'name': 'intel-stream',
        'cc': 'icc',
        'cxx': 'icpc',
        'ftn': 'ifort',
        'modules': [
            'intel-oneapi-compilers/2021.4.0',
        ],
        'target_systems': [
            'alaska:compute-icc21-impi21-roce-umod',
            # <end - alaska partitions>
            'grenoble-g5k:dahu-icc21-impi21-opa-umod',
            # <end - grenoble-g5k partitions>
            'juwels-cluster:batch-icc21-impi21-ib-umod',
            # < end - juwels partitions>
            'nancy-g5k:gros-icc21-impi21-eth-umod',
            # <end - nancy-g5k partitions>
        ],
    },
    {
        'name': 'intel-stream',
        'cc': 'icc',
        'cxx': 'icpc',
        'ftn': 'ifort',
        'modules': [
            'intel/2022.1.0',
        ],
        'target_systems': [
            'cscs-daint:daint-icc21-impi21-ib-umod-gpu',
            # <end - cscs daint partitions>
        ],
    },
    {
        'name': 'gnu-stream',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'target_systems': [
            'juwels-booster:booster-gcc9-ompi4-ib-umod',
            # < end - juwels partitions>
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu'
            # <end - cscs daint partitions>
        ],
    },

     {
        'name': 'gnu-stream',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'target_systems': [
            'licallo'
            # <end - licallo partitions>
        ],
    },

    {
        'name': 'intel-stream',
        'cc': 'icc',
        'cxx': 'icpc',
        'ftn': 'ifort',
        'modules': [
            'intel-compilers/2021.9.0',
        ],
        'target_systems': [
            'jeanzay',
            # <end - jeanzay partitions>
        ],
    },
    {
        'name': 'gnu-stream',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'target_systems': [
            'jeanzay'
            # <end - jeanzay partitions>
        ],
    },
]
