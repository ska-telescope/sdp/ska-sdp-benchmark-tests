""""This file contains the environment config for CUDA Numpy tests"""


numpy_cuda_environ = [
    {
        'name': 'numpy_cuda',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
           'cuda/11.4.0'
        ],
        'target_systems': [
            'juwels-booster:booster-gcc9-ompi4-ib-umod-nvgpu',
            # <end - juwels partitions>
        ],
    },
        {
        'name': 'numpy_cuda',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
           'cudatoolkit/11.2.0_3.39-2.1__gf93aa1c'
        ],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs partitions>
        ],
    },
    {
        'name': 'numpy_cuda',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
           'cuda/11.2.0'
        ],
        'target_systems': [
            'nancy-g5k:grouille-gcc9-ompi4-eth-umod-nvgpu',
            # <end - nancy partitions>
        ],
    },

    # generic environment with no modules manager.
    {
        'name': 'numpy_cuda',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'target_systems': [
            'generic'
        ],
    }
]
