""""This file contains the environment config for IOR benchmark"""


ior_environ = [
    {
        'name': 'ior',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'ior/3.3.0-openmpi-4.1.1',
        ],
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
            # <end - alaska partitions>
            'juwels-cluster:batch-gcc9-ompi4-ib-umod',
            # <enc - juwels partitions>
        ],
    },
    {
        'name': 'ior',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'ior/3.3.0',
        ],
        'target_systems': [
            'cscs-daint:daint-icc21-impi21-ib-umod-gpu'
            # <end - cscs daint partitions>
        ],
    },
]
