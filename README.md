# SDP Benchmark Tests

This repository contains the micro and radio-astronomy application specific benchmarks that can be of use in system sizing and procurement of Science Data Processor (SDP). [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) is used as a framework to package the benchmarks which offers a high level of portability across systems and platforms. More details on how to configure the systems and run the benchmarks are provided in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/?badge=latest).

## Unit tests (experimental)

Use the modules `pytest` and its extension `pytest-xdist` to enable multithreading.

Compatible with unittest module writing.

To start the unit tests script:
```
source sdp-unittest.sh [-n nb_CPU] [-t test1,test2,test3] [-o] [-l loggingLevel]
# [-o] : enable the logging output
```

For the moment, only the CondaEnvManagerTest class is available.

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-benchmark-tests/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/?badge=latest)
