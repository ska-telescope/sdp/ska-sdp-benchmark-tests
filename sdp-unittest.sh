#!/bin/bash

echo "Welcome to the unittests script for the SDP-benchmark-tests : "

# You can add a test here without the extension .py
testList=("test_conda_env_manager test_base_class")

Help()
{
    # Display Help
    echo "Usage : [-h] [-n number] [-t string] [-o] [-l string]" >&2
    echo "Options :"
    echo "-h     Print help."
    echo "-n     Set the number of CPU for the unittests."
    echo "-t     Load one or multiple tests separate by a comma and without the extension .py (generic list by default)."
    echo "-o     Enable the output logging to debug (DEBUG level by default)."
    echo "-l     Choose your level of logging : INFO, WARNING, DEBUG, ERROR"
    echo
}

OPTIND=1
while getopts n:thlo arg
do
    case "$arg" in
	h)
	    Help
	    return 2> /dev/null; exit;;
	n)
	    nb_CPU=${OPTARG}
	    echo "$nb_CPU CPU selected for the unittests phase.";;
	t)
	    IFS=',' read -r -a $testList <<< "${OPTARG}"
	    echo "${OPTARG} loaded.";;
	o)
	    logging=true;;
	l)
	    level_logging=${OPTARG};;
	*)
	    echo "Usage : [-h] [-n number] [-t string] [-l boolean] [-ll string]" >&2
	    return 2> /dev/null; exit;;
    esac
done
shift $(($OPTIND -1))

if [ -z "$nb_CPU" ]
then
    nb_CPU=1
    echo "$nb_CPU CPU by default for the unittests phase."
fi

if [ -z "$logging" ]
then
    if [ -z "$level_logging" ]
    then
	logging_options="-o log_cli=true --log-level=DEBUG"
    else
	logging_options="-o log_cli=true --log-level=$level_logging"
    fi
else
    logging_options="-o log_cli=false"
fi


#Main loop for the tests
for testToCheck in $testList
do
    pytest $logging_options -n $nb_CPU unittests/${testToCheck}".py"
done
