import reframe as rfm
import reframe.utility.sanity as sn
from modules.base_class import RunOnlyBenchmarkBase

VALID_SYSTEMS = ["generic:default"]
VALID_PRGENVS = ["testing"]


def success_prerun():
    return True, "Successful prerun check"


def fail_prerun():
    return False, "Failed prerun check"


def success_postrun():
    return True, "Successful postrun check"


def fail_postrun():
    return False, "Failed postrun check"


def raise_exception():
    raise RuntimeError("Runtime Error")


@rfm.simple_test
class BaseClass_NoPrePostChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS


@rfm.simple_test
class BaseClass_SuccessfulPrerunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(success_prerun)


@rfm.simple_test
class BaseClass_FailingPrerunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(fail_prerun)


@rfm.simple_test
class BaseClass_SuccessfulPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_postrun_checks(success_postrun)


@rfm.simple_test
class BaseClass_FailingPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_postrun_checks(fail_postrun)


@rfm.simple_test
class BaseClass_SuccessfulBothChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(success_prerun)
        self.add_postrun_checks(success_postrun)


@rfm.simple_test
class BaseClass_SuccessfulPrerunFailingPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(success_prerun)
        self.add_postrun_checks(fail_postrun)


@rfm.simple_test
class BaseClass_FailingPrerunSuccessfulPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(fail_prerun)
        self.add_postrun_checks(success_postrun)


@rfm.simple_test
class BaseClass_FailingBothChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(fail_prerun)
        self.add_postrun_checks(fail_postrun)


@rfm.simple_test
class BaseClass_ExceptionPrerun_NoPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(raise_exception)


@rfm.simple_test
class BaseClass_ExceptionPrerun_SuccessfulPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(raise_exception)
        self.add_postrun_checks(success_postrun)


@rfm.simple_test
class BaseClass_ExceptionPrerun_FailingPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(raise_exception)
        self.add_postrun_checks(fail_postrun)


@rfm.simple_test
class BaseClass_NoPrerun_ExceptionPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_postrun_checks(raise_exception)


@rfm.simple_test
class BaseClass_SuccessfulPrerun_ExceptionPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(success_prerun)
        self.add_postrun_checks(raise_exception)


@rfm.simple_test
class BaseClass_FailingPrerun_ExceptionPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(fail_prerun)
        self.add_postrun_checks(raise_exception)


@rfm.simple_test
class BaseClass_ExceptionPrerun_ExceptionPostrunChecks_Test(RunOnlyBenchmarkBase):
    executable = "echo BaseClass working"
    sanity_patterns = sn.assert_true(1)
    valid_systems = VALID_SYSTEMS
    valid_prog_environs = VALID_PRGENVS

    def __init__(self):
        super().__init__()
        self.add_prerun_check(raise_exception)
        self.add_postrun_checks(raise_exception)
