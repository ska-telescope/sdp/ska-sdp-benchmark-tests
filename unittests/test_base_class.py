import unittest
import os
import logging
import subprocess

# INFO ERROR DEBUG WARNING
logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger(__name__)


def call(cmd: str):
    cmd_arr = cmd.split(' ')
    ret = subprocess.run(cmd_arr, capture_output=True)
    return ret.returncode, ret.stdout.decode('utf-8'), ret.stderr.decode('utf-8')


class BaseClassTest(unittest.TestCase):
    # when run via the unit test script, this points to the root of the repository
    prefix = os.getcwdb().decode("utf-8")

    # Base reframe command specifying the config and the base class test
    base_rfm_cmd = f"reframe -C {prefix}/reframe_config.py --system=generic --prgenv=testing " \
                   f"-c {prefix}/unittests/rfm_resources/rfm_BaseClassTest.py"

    def test_no_prerun_or_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_no_prerun_or_postrun_checks")

        test_name = "BaseClass_NoPrePostChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_successful_prerun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_successful_prerun_checks")

        test_name = "BaseClass_SuccessfulPrerunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_failing_prerun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_failing_prerun_checks")

        test_name = "BaseClass_FailingPrerunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_successful_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_successful_postrun_checks")

        test_name = "BaseClass_SuccessfulPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_failing_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_failing_postrun_checks")

        test_name = "BaseClass_FailingPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertIn("postrun checks failed", out, "Postrun checks are expected to fail, however they didn't.")
        self.assertEqual(code, 1, f"Got wrong return code: {code}, expected 1")  # return code must be 1 (failing)

    def test_successful_both_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_successful_both_checks")

        test_name = "BaseClass_SuccessfulBothChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_successful_prerun_failing_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_successful_prerun_failing_postrun_checks")

        test_name = "BaseClass_SuccessfulPrerunFailingPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertIn("postrun checks failed", out, "Postrun checks are expected to fail, however they didn't.")
        self.assertEqual(code, 1, f"Got wrong return code: {code}, expected 1")  # return code must be 1 (failing)

    def test_failing_prerun_successful_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_failing_prerun_successful_postrun_checks")

        test_name = "BaseClass_FailingPrerunSuccessfulPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_failing_prerun_failing_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_failing_prerun_failing_postrun_checks")

        test_name = "BaseClass_FailingBothChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")

        # No postrun checks will be done if the prerun checks don't succeed and the test gets skipped
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_exception_prerun_no_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_exception_prerun_no_postrun_checks")

        test_name = "BaseClass_ExceptionPrerun_NoPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")
        self.assertIn("Exception: ", out, "An exception is expected, however none was logged.")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_exception_prerun_successful_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_exception_prerun_successful_postrun_checks")

        test_name = "BaseClass_ExceptionPrerun_SuccessfulPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")
        self.assertIn("Exception: ", out, "An exception is expected, however none was logged.")
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_exception_prerun_failing_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_exception_prerun_failing_postrun_checks")

        test_name = "BaseClass_ExceptionPrerun_FailingPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")
        self.assertIn("Exception: ", out, "An exception is expected, however none was logged.")

        # No postrun checks will be done if the prerun checks don't succeed (-> Exception = Fail) and the test gets
        # skipped
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_no_prerun_exception_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_no_prerun_exception_postrun_checks")

        test_name = "BaseClass_NoPrerun_ExceptionPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertIn("postrun checks failed", out, "Postrun checks are expected to fail, however they didn't.")
        self.assertIn("Exception: ", out, "An exception is expected, however none was logged.")
        self.assertEqual(code, 1, f"Got wrong return code: {code}, expected 1")  # return code must be 1 (failing)

    def test_successful_prerun_exception_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_successful_prerun_exception_postrun_checks")

        test_name = "BaseClass_SuccessfulPrerun_ExceptionPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertNotIn("prerun checks failed", out, "Some prerun checks failed!")
        self.assertIn("postrun checks failed", out, "Postrun checks are expected to fail, however they didn't.")
        self.assertIn("Exception: ", out, "An exception is expected, however none was logged.")
        self.assertEqual(code, 1, f"Got wrong return code: {code}, expected 1")  # return code must be 1 (failing)

    def test_failing_prerun_exception_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_failing_prerun_exception_postrun_checks")

        test_name = "BaseClass_FailingPrerun_ExceptionPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")
        self.assertNotIn("Exception: ", out, "An exception is not expected, however one was raised.")

        # No postrun checks will be done if the prerun checks don't succeed (-> Exception = Fail) and the test gets skipped
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero

    def test_exception_prerun_exception_postrun_checks(self):
        logging.info("BaseClassTest")
        logging.info("--------------------\n")
        logging.info("test_exception_prerun_exception_postrun_checks")

        test_name = "BaseClass_ExceptionPrerun_ExceptionPostrunChecks_Test"
        code, out, err = call(f"{self.base_rfm_cmd} -n {test_name} -r")

        self.assertIn("prerun checks failed", out, "Prerun checks are expected to fail, however they didn't.")
        self.assertIn("Exception: ", out, "An exception is expected, however none was logged.")

        # No postrun checks will be done if the prerun checks don't succeed (-> Exception = Fail) and the test gets skipped
        self.assertNotIn("postrun checks failed", out, "Some postrun checks failed!")
        self.assertEqual(code, 0, f"Got non-zero return code: {code}")  # return code must be zero
