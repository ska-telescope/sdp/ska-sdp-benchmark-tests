.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

Welcome to documentation of SKA SDP Benchmark Tests
========================================================

This documentation contains introduction to SKA SDP benchmark tests, brief overview of `ReFrame <https://reframe-hpc.readthedocs.io/en/stable/>`_ and `Spack <https://spack-tutorial.readthedocs.io/>`_ which are building blocks of this repository, installation instructions and description of different tests provided.

.. toctree::
  :maxdepth: 2

  ../content/introduction
  ../content/installation
  ../content/workflows
  ../content/tools
  ../content/metrics
  ../content/benchmarks
  ../content/organisation
  ../content/modules
  ../content/contribute
  ../content/moddocs


:caption: Releases
  CHANGELOG.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
