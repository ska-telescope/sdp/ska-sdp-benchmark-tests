Level 0 Benchmark Tests
==============================================

CPU tests
-----------

.. automodule:: apps.level0.cpu.fft.reframe_fft
  :members:
.. automodule:: apps.level0.cpu.hpcg.reframe_hpcg
  :members:
.. automodule:: apps.level0.cpu.hpl.reframe_hpl
  :members:
.. automodule:: apps.level0.cpu.imb.reframe_imb
  :members:
.. automodule:: apps.level0.cpu.ior.reframe_ior
  :members:
.. automodule:: apps.level0.cpu.python.reframe_numpy
  :members:
.. automodule:: apps.level0.cpu.stream.reframe_stream
  :members:

GPU tests
-----------

.. automodule:: apps.level0.gpu.babel_stream.reframe_babelstream
  :members:
.. automodule:: apps.level0.gpu.fft_test.reframe_fft
  :members:
.. automodule:: apps.level0.gpu.gdr_test.reframe_gdr
  :members:
.. automodule:: apps.level0.gpu.nccl_test.reframe_nccltest
  :members:
.. automodule:: apps.level0.gpu.hippo_func_lib.reframe_funclib_test
  :members:


Level 1 Benchmark Tests
==============================================

.. automodule:: apps.level1.cng_test.reframe_cngtest
  :members:

.. automodule:: apps.level1.idg_test.reframe_idgtest
  :members:
  
.. automodule:: apps.level1.imaging_iotest.reframe_iotest
  :members:


Level 2 Benchmark Tests
==============================================

.. automodule:: apps.level2.rascil.reframe_rascil
  :members:
