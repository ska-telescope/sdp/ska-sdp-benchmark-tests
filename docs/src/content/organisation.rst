Organisation of repository
===================================

Core
--------------------------------

- The source code of benchmark tests can be found in the ``apps/`` folder. This folder is divided into three sub-folders ``level0/``, ``level1/`` and ``level2/``. These folders are further sub-divided where each benchmark test is placed into a folder named after it. Inside each benchmark folder, we typically find three different files namely, ``reframe_<testname>.py``, ``README.md`` and ``TESTNAME.ipynb``, where ``<testname>`` is the name of the benchmark. The logic of the test is included in ``reframe_<testname>.py``, details about the documentation can be found in ``README.md`` and finally, ``TESTNAME.ipynb`` can be used to plot and tabulate benchmark results.

- All the configuration related files are placed in ``config/`` folder and they are imported into ``reframe_config.py`` file. We find two sub-folders in ``config/`` folder namely, ``systems/`` and ``environs/``. All the system and partitions are defined in ``systems/`` folder and environments are defined in ``environs/`` folder. We keep one file for a given platform and define all the partitions of that platform in that file. Similarly, place a environment file for a given test and define all the environments for that test in the file.

- All the utility functions and extra classes defined out of ReFrame are placed in ``modules/`` folder.

- ReFrame tests use topological information of the platforms like number of sockets, cores, *etc*. Topological information of different platforms are placed in ``topologies/`` folder. These files are imported and added during configuration of system partitions.

Documentation
--------------------------------

All the documentation is provided in the ``docs/`` folder. The sources can be found at ``docs/src/content/`` folder where each section of documentation has a folder. Within each folder, ``rst`` files can be found that contain core documentation.

Misc
--------------------------------

- To deploy software stack using Spack, the bootstrap script is provided in ``spack/`` folder. There is a ``README`` file in the folder with the instructions on how to use the bootstrap script.
- The folder ``perflogs/`` contain all the performance metrics that are extracted for different benchmarks for different systems and partitions. These logs are used in plotting and tabulating the performance data in the Jupyter notebooks for each test.
- The folder ``helloworld/`` contains a simple ReFrame test taken from ReFrame documentation demonstrating the use case of its framework.
- The folder ``.ci/`` contains all the CI related files. It includes dockerfile to build image and all the auxiliary files used inside docker image.
