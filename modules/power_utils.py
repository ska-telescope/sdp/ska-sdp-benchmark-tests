import subprocess
import os

import cppyy as c

from datetime import datetime

class PowerUtils:

    sensors = {}

    def __init__(self, use_likwid=True, use_rapl=True, use_nvml=True, use_rocm=False, use_amdgpu=False, library_path=None):
        if not library_path:
            from ctypes.util import find_library
            # Assume this library is available on the system
            library_path = find_library('libpowersensor')
            if not library_path:
                raise Exception("Libpowersensor not found! Check $LD_LIBRARY_PATH")

        # load libpowersensor
        c.add_library_path(library_path)
        c.add_include_path(f"{library_path}/include")
        c.include("powersensor.h")
        c.load_library("libpowersensor")
        self.powersensor = c.gbl.powersensor

        if use_likwid:
            self.sensors['likwid'] = PowerSensor(self.powersensor.likwid.LikwidPowerSensor.create())
        if use_rapl:
            self.sensors['rapl'] = PowerSensor(self.powersensor.rapl.RaplPowerSensor.create())
        if use_nvml:
            self.sensors['nvml'] = PowerSensor(self.powersensor.nvml.NVMLPowerSensor.create(0))
        if use_rocm:
            self.sensors['rocm'] = PowerSensor(self.powersensor.rocm.ROCMPowerSensor.create(0))
        if use_amdgpu:
            self.sensors['amdgpu'] = PowerSensor(self.powersensor.amdgpu.AMDGPUPowerSensor.create(0))

    def Joules(self, start_state, end_state):
        return self.powersensor.PowerSensor.Joules(start_state, end_state)

    def Watt(self, start_state, end_state):
        return self.powersensor.PowerSensor.Watt(start_state, end_state)

    def seconds(self, start_state, end_state):
        return self.powersensor.PowerSensor.seconds(start_state, end_state)

    def mark(self, start_state, current_state, name: str, tag: int):
        self.powersensor.PowerSensor.mark(start_state, current_state, name, tag)

    def read_all(self):
        states: dir = {}
        for key in self.sensors:
            states[key] = self.sensors[key].read()
        return states

    def measure_all(self, start_states: dir, end_states: dir):
        measurements = []
        if not all([k in end_states for k in start_states]):
            raise ValueError("Not all elements from start_states are in end_states, "
                             "suggesting that they were not created with the "
                             "same PowerSensor configuration.")

        for key in start_states:
            m = {
                'sensor': key,
                'joules': self.Joules(start_states[key], end_states[key]),
                'watt': self.Watt(start_states[key], end_states[key]),
                'seconds': self.seconds(start_states[key], end_states[key])
            }
            measurements.append(m)
        return measurements


class PowerSensor:
    sensor = None

    def __init__(self, sensor):
        self.sensor = sensor

    def read(self):
        return self.sensor.read()


if __name__ == "__main__":
    import sys

    use_likwid = False
    use_rapl = False
    use_nvml = False
    use_rocm = False
    use_amdgpu = False
    out_file = os.getenv('SDPTESTS_ROOT', '.') + "/perfmetrics/test-out.log"

    curr_arg = 1
    executable_found = False
    while not executable_found:
        next_param = sys.argv[curr_arg].strip()
        curr_arg += 1
        if next_param.startswith("--output="):
            out_file = next_param.split('=')[1]
        elif next_param == '--use-likwid':
            use_likwid = True
        elif next_param == '--use-rapl':
            use_rapl = True
        elif next_param == '--use-nvml':
            use_nvml = True
        elif next_param == '--use-rocm':
            use_rocm = True
        elif next_param == '--use-amdgpu':
            use_amdgpu = True
        else:
            executable_found = True
            curr_arg -= 1

    library_path = os.getenv('SDPTESTS_POWER_LIBPOWERSENSOR_PATH', "/home/manuel/Code/SKA/power/libpowersensor/build")

    powerUtils = PowerUtils(use_likwid=use_likwid,
                            use_rapl=use_rapl,
                            use_nvml=use_nvml,
                            use_rocm=use_rocm,
                            use_amdgpu=use_amdgpu,
                            library_path=library_path)

    # Read start value
    starts = powerUtils.read_all()

    # Execute passed application
    subprocess.run(sys.argv[curr_arg:])

    # Read end value
    ends = powerUtils.read_all()

    # Print differences
    measurements = powerUtils.measure_all(starts, ends)

    os.makedirs(os.path.dirname(out_file), exist_ok=True)
    with open(out_file, 'a') as f:
        for entry in measurements:
            for key in entry:
                if key == 'sensor':
                    continue
                f.write(f"{datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')}|{entry['sensor']}_{key}={entry[key]}\n")
