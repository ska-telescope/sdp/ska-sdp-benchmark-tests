""" Extra functionality for reframe"""

import os
import sys
import re
import subprocess
import shutil
import socket
import json
import distutils.dir_util
import yaml
import uuid

import reframe as rfm
from modules.base_class import BenchmarkBase, RunOnlyBenchmarkBase
from modules.conda_env_manager import CondaEnvManager
from modules.utils import emit_conda_env_cmds
from reframe.core.buildsystems import BuildSystem  # pylint: disable=import-error
from reframe.core.builtins import variable, run_before, run_after
from reframe.core.deferrable import deferrable
from reframe.core.logging import getlogger  # pylint: disable=import-error
from reframe.core.backends import getlauncher  # pylint: disable=import-error
from reframe.core.exceptions import SpawnedProcessError  # pylint: disable=import-error
from reframe.core.exceptions import SpawnedProcessTimeout  # pylint: disable=import-error
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error
import modules.conda_env_manager as cem

import modules

# pylint: disable=C0116,E0401,E0602,E1101,W0201


def patch_launcher_command(job):
    """Patch the job launcher command for mpirun and mpiexec. We remove
       -np/-n arguments as we pass them manually inside ReFrame test
    """
    if job.launcher.registered_name == 'local':
        return ['']
    else:
        return [job.launcher.registered_name]


def patch_job_names(job, stagedir):
    """Patch the job name, stdout and stderr to remove special characters injected 
       by fixtures
    """
    # Define illegal characters
    illegals = re.compile(r'[%~+:^@]')

    # Patch job.name, job.stdout, job.stderr, job.script_filename
    job._name = illegals.sub('_', job.name)
    job._stdout = illegals.sub('_', job.stdout)
    job._stderr = illegals.sub('_', job.stderr)
    job._script_filename = illegals.sub('_', job.script_filename)
    job._workdir = illegals.sub('_', job.workdir)

    # Patch stagedir
    stagedir = illegals.sub('_', stagedir)

    return job, stagedir


class UsesInternet(rfm.RegressionMixin):
    """ Mixin

        Classes using this Mixin are ensured to have access to internet.
    """

    @run_after('setup')
    def set_use_internet(self):
        if 'internet' in self.current_partition.extras and not self.current_partition.extras['internet']:
            # if the partition does not have internet, set to run locally (on login nodes)
            self.local = True
            self.build_locally = True


class CondaSetupTest(rfm.RunOnlyRegressionTest, UsesInternet):
    env_name = variable(str, value="rfm_DefaultEnv")
    conda_packages = variable(list, value=[])
    pip_packages = variable(list, value=[])
    recreate_env = variable(bool, value=True)

    def __init__(self):
        super().__init__()
        self.maintainers = [
            'Manuel Stutz (manuel.stutz@fhnw.ch)'
        ]
        self.executable = "echo noop"

        # run conda env setup on login node
        self.local = True
        self.env_manager: cem.CondaEnvManager = None

    @run_after('setup')
    def create_conda_env_manager(self):
        self.env_manager = cem.CondaEnvManager(self.env_name)

    @run_before('run')
    def setup_conda_env(self):
        if self.recreate_env and self.env_manager.does_exist():
            self.env_manager.remove()
        self.env_manager.create()
        for p in self.conda_packages:
            self.env_manager.install_conda(p)
        for p in self.pip_packages:
            self.env_manager.install_pip(p)
        self.prerun_cmds = self.env_manager.emit_conda_activate_cmds().split(';')

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_not_found('error', self.stderr),
            sn.assert_not_found('Error', self.stderr)
        ])


class CachedRunTest(rfm.RegressionTest):
    """ Mixin.

        Classes using this can be derive from ``rfm.RunOnlyRegressionTest``
        Assumes saved output files are in a directory ``cache/`` in same parent directory
        (and with same directory tree) as the ``output/`` and ``stage/`` directories.

        set ``self.use_cache`` to True or a path relative to cwd.
    
        NB Any class using this MUST NOT change self.executable in a method decorated with
        ``@rfm.run_before('run')`` as that will override functionality here.
    """

    @run_before('run')
    def no_run(self):
        """Turn the run phase into a no-op"""

        if self.use_cache:
            with open(os.path.join(self.stagedir, 'noop.sh'), 'w') as noop:
                noop.write('#!/bin/bash\necho "noop $@"\n')
            self.executable = "./noop.sh"

    @run_after('run')
    def copy_saved_output(self):
        """ Copy saved output files to stage dir. """

        if self.use_cache:
            # find the part of the path common to both output and staging:
            rtc = rfm.core.runtime.runtime()
            tree_path = os.path.relpath(self.outputdir, rtc.output_prefix)

            cache_root = 'cache' if self.use_cache is True else self.use_cache

            saved_output_dir = os.path.join(cache_root, tree_path)

            if not os.path.exists(saved_output_dir) or not os.path.isdir(saved_output_dir):
                raise ValueError(
                    f"cached output directory {os.path.abspath(saved_output_dir)} "
                    f"does not exist or isn't a directory"
                )

            distutils.dir_util.copy_tree(saved_output_dir, self.stagedir)


class CachedCompileOnlyTest(rfm.CompileOnlyRegressionTest):
    """ A compile-only test with caching of binaries between ``reframe`` runs.
    
        Test classes derived from this class will save ``self.executable`` to a
        ``./builds/{system}/{partition}/{environment}/{self.name}`` directory after compilation.
        However if this path (including the filename) exists before compilation
        (i.e. on the next run):

          - No compilation occurs
          - ``self.sourcesdir`` is set to this directory, so ``reframe`` will copy the
               binary to the staging dir (as if compilation had occured)
          - A new attribute ``self.build_path`` is set to this path (otherwise None)

        Note that ``self.sourcesdir`` is only manipulated if no compilation occurs, so compilation
        test-cases which modify this to specify the source directory should be fine.
    """
    @run_before('compile')
    def conditional_compile(self):
        """Point sourcedir to build_dir if it exists"""
        build_dir = os.path.abspath(os.path.join(
            'builds', self.current_system.name, self.current_partition.name,
            self.current_environ.name, self.name
        ))
        build_path = os.path.join(build_dir, self.executable)
        if os.path.exists(build_path):
            self.build_path = build_path
            getlogger().info(f'found exe at {self.build_path}')
            self.build_system = NoBuild()
            self.sourcesdir = build_dir # means reframe will copy the exe back in
        else:
            self.build_path = None

    @run_after('compile')
    def copy_executable(self):
        """Copy executables to build_dir if it compiles"""
        if not self.build_path: # i.e. only if actually did a compile:
            self.exes_dir = os.path.join('builds', self.current_system.name,
                                         self.current_partition.name, self.current_environ.name,
                                         self.name
                                         )
            exe_path = os.path.join(self.stagedir, self.executable)
            build_path = os.path.join(self.exes_dir, self.executable)
            build_dir = os.path.dirpath(build_path) # self.executable might include a directory
            if not os.path.exists(build_dir):
                os.makedirs(build_dir)
            shutil.copy(exe_path, build_path)
            getlogger().info(f'copied exe to {build_path}')


class NoBuild(BuildSystem):
    """ A no-op build system """
    def __init__(self):
        super().__init__()

    def emit_build_commands(self, environ):
        return []


class CachedGitCloneTest(rfm.RunOnlyRegressionTest):
    """ A git clone-only test with caching between the ``reframe`` runs.

        This test clones the git repository and copies the repository to ``$PWD/src`` folder on the
        first run. Once the ``$PWD/src`` is there with repository inside it, it simply sets this
        directory as ``self.sourcesdir`` so new cloning is avoided for the next run.

        If a new clone is needed the user simply need to remove $PWD/src before running reframe.

        We need to set few variables for this test to work

           - ``repo_name``: Name of the repository
           - ``repo_url``: URL of git repository

    """

    @run_after('init')
    def check_req_variables(self):
        """Check essential variables if they are defined"""
        if not hasattr(self, 'repo_url'):
            raise NameError(f'test classes derived from {type(self)} must define self.repo_url')

    @run_before('run')
    def set_sources(self):
        """Set source dir"""
        self.src_path = os.path.abspath(os.path.join(self.prefix, 'src'))
        self.cache_dir = os.path.abspath(
            os.path.join('cache', self.current_system.name,
                         self.current_partition.name, self.current_environ.name,
                         self.name)
        )
        # If src_path or cache_dir does not exist, git clone repo into stage dir
        # If both exists, src_path will take precedence
        if not os.path.exists(self.src_path) and not os.path.isdir(
                self.cache_dir):
            self.sourcesdir = self.repo_url
        elif os.path.exists(self.src_path):
            # First check if src_path exists, we simply set the source dir
            # variable and do a no-op
            self.sourcesdir = self.src_path
        elif os.path.exists(self.cache_dir):
            # Next check if cache_dir exists, we simply set the source dir
            # variable and do a no-op
            self.sourcesdir = self.cache_dir

    @run_before('run')
    def set_executable(self):
        """Set a noop exe"""
        # Make it a no op test
        self.executable = 'echo'
        self.executable_opts = ['noop']

    @run_after('run')
    def copy_sources_to_cache(self):
        """Copy sources to cache dir"""
        # Copy sources to cache so we dont clone for following runs
        if not os.path.exists(self.src_path) and not os.path.isdir(
                self.cache_dir):
            distutils.dir_util.copy_tree(self.stagedir, self.cache_dir)


class FetchSourcesBase(RunOnlyBenchmarkBase, UsesInternet):
    """Fixture to fetch source code"""

    def __init__(self):
        super().__init__()
        self.cem = CondaEnvManager(env_name=self.cnd_env_name, install_perfmon=False)

    @run_after('init')
    def set_vars(self):
        self.local = True
        self.executable = 'echo noop'

    @run_after('setup')
    def set_conda_prerun_cmds(self):
        """Emit conda env prerun commands"""
        # Clear PYTHONPATH temporarily
        self.prerun_cmds += ['export PYTHONPATH=']

        if hasattr(self, 'recreate_environment') and self.recreate_environment and self.cem.does_exist():
            self.cem.remove()
        self.cem.create()
        self.prerun_cmds += self.cem.emit_conda_activate_cmds().split(';')

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns"""
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)


class PerfmonMixin(rfm.RegressionMixin):

    @run_after('setup')
    def create_perfmetrics_dir(self):
        """Create a dir for storing perfmetrics"""
        self.perfmetrics_dir = os.path.abspath(
            os.path.join('perfmetrics', self.current_system.name, self.current_partition.name,
                         self.current_environ.name, self.name))
        # Remove directory if already exists
        shutil.rmtree(self.perfmetrics_dir, ignore_errors=True)
        # Create perfmeterics directory
        os.makedirs(self.perfmetrics_dir, exist_ok=True)

    @run_after('setup')
    def create_fake_job_id_local_scheduler(self):
        """If scheduler is local, make a fake job ID"""
        if self.current_partition.scheduler.registered_name == 'local':
            self.env_vars['LOCAL_JOB_ID'] = \
                str(modules.utils.generate_random_number(5))

    @run_after('setup')
    def patch_job_launcher(self):
        """Monkey mock the job launcher command"""
        # Monkey mocking the actual job launcher command to remove -np/-n
        # argument.
        self.launcher_name = self.job.launcher.registered_name
        if self.launcher_name in ['mpirun', 'mpiexec']:
            self.job.launcher.command = patch_launcher_command

    @run_after('setup')
    def get_scheduler_env_vars(self):
        """Get scheduler specific env variables"""
        self.scheduler_env_vars = modules.utils.get_scheduler_env_list(
            self.current_partition.scheduler.registered_name
        )

    @run_before('run')
    def export_env_variables(self):
        """Make a string of env variables that will be exported to all nodes"""
        # On some platforms (like grid5000) env variables are not exported to all nodes in the
        # reservation automatically.
        # We use OpenMPI's MCA parameter mca_base_env_list to export them
        self.env_vars['OMPI_MCA_mca_base_env_list'] = \
            f'\"PATH;LD_LIBRARY_PATH;{";".join(self.env_vars.keys())}\"'

    @run_before('run')
    def get_mpi_launcher_cmd(self):
        """Get MPI launcher command with additional options"""
        if self.launcher_name == 'mpirun':
            self.launcher_cmd = f'mpirun -np {self.num_nodes} -npernode 1 '
        elif self.launcher_name == 'mpiexec':
            self.launcher_cmd = f'mpiexec -n {self.num_nodes} -ppn 1'
        elif self.launcher_name == 'srun':
            self.launcher_cmd = f'srun --ntasks {self.num_nodes} --ntasks-per-node 1 '

    @run_before('run')
    def emit_perfmon_cmds(self):
        # Perfmon is expected to be be installed in the current python env (ska-sdp-benchmark-tests)
        if os.getenv('MONITOR_METRICS', 'NO') == 'YES':
            if 'g5k' in self.current_system.name:
                # On Grid5000 clusters to monitor perf metrics
                self.prerun_cmds += [
                    f'{self.launcher_cmd} sudo-g5k sysctl -w kernel.perf_event_paranoid=2'
                ]
            jid = self.scheduler_env_vars['job_id']
            self.prerun_cmds += [
                f'{self.launcher_cmd} perfmon -i 10 -d {self.perfmetrics_dir} --export pickle '
                f'--verbose &',
                'export MON_PID=$!',
                f'while [[ ! -f ".ipc-${jid}" ]] || [[ `cat .ipc-${jid}` != "INPROGRESS" ]]; do sleep 1; done',
                # wait for perfmon to be started
                f'echo "Perfmon started, continuing with the task!"',
            ]

    @run_after('setup')
    def set_postrun_cmds(self):
        """Set postrun commands"""
        if os.getenv('MONITOR_METRICS', 'NO') == 'YES':
            self.postrun_cmds = [
                f"echo \"FINISHED\" > .ipc-${self.scheduler_env_vars['job_id']}",
                'wait $MON_PID',
            ]


class MultiRunMixin(rfm.RegressionMixin):
    """This mixin creates a uniquely identifiable ID for tests that are run multiple times.
    Over all tests that are run with one reframe call, this yields the same unique key."""

    unique_key = variable(str, value=str(uuid.uuid4()))

    @run_after('setup')
    def set_unique_run_id(self):
        self.tags |= {
            f"unique_key={self.unique_key}"
        }


class AppBase(BenchmarkBase, MultiRunMixin):#, PerfmonMixin):
    """ A base test class that includes common methods for application reframe tests.

        This base class defines several methods that are common to different reframe tests
    """

    # Set default params
    launcher_cmd = ""

    def __init__(self):
        super().__init__()
        if not hasattr(self, 'num_nodes'):
            self.num_nodes = 1
        if not hasattr(self, 'max_num_nodes'):
            self.max_num_nodes = 1

    @property
    @deferrable
    def num_tasks_assigned(self):
        """Get number of job tasks"""
        return self.job.num_tasks

    @property
    @deferrable
    def run_command_emitted(self):
        """Get job run command"""
        return self.job.launcher.run_command(self.job)

    @run_after('setup')
    def get_max_nodes_partition(self):
        """Get maximum number of available nodes in partition"""
        self.max_num_nodes = SchedulerInfo(rfm_partition=self.current_partition,
                                           exclude_states='down').num_nodes

    @run_after('setup')
    def check_test_feasibility(self):
        """Check if test is feasible based on number of nodes requested"""
        self.skip_if(
            self.num_nodes > self.max_num_nodes,
            msg=f'Number of requested nodes {self.num_nodes} is higher than '
                f'{self.max_num_nodes} that are available on the partition'
        )

    @run_after('setup')
    def set_num_tasks_reservation(self):
        """Set number of tasks for job reservation"""
        # This method sets number of tasks for the reservation. Here ONLY resources are reserved
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
        else:
            self.num_tasks_per_node = self.current_partition.processor.num_cpus
        self.num_tasks = self.num_nodes * self.num_tasks_per_node
        self.tags |= {f'num_cores={self.num_tasks}'}

    @run_before('run')
    def set_git_commit_tag(self):
        """Get git commit hash"""
        git_ref_src = osext.git_repo_hash(short=False, wd=self.sourcesdir)
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {
            f'git={git_ref}',
            f'git_src={git_ref_src}',
        }


class SpackBase(rfm.RunOnlyRegressionTest):
    """A base class that includes common methods for deploying software stack using Spack"""

    # Variable to define Spack git URL
    spack_url = variable(str, value='https://github.com/spack/spack.git')

    # Variable to define Spack version
    spack_ver = variable(str, value='v0.17.0')

    # Variable to define compiler toolchain
    gcc_toolchain = variable(str, value='gcc@9.3.0')
    intel_toolchain = variable(str, value='intel-oneapi-compilers@2021.4.0')

    # Variable to define cluster name
    cluster = variable(str)

    # Variable to define spack root
    spack_root = variable(str)

    # Variable to define location of spack build cache
    spack_build_cache_dir = variable(str, value='')

    # Variable to define spack build cache mirror name
    spack_build_cache_mirror_name = variable(str, value='')

    # Build packages with icc toolchain
    # We override this in main test if processor is other than Intel
    build_with_icc = variable(bool, value=True)

    # Variable to define valid cluster names
    valid_clusters = variable(
        list, value=['juwels-cluster', 'juwels-booster', 'alaska', 'grenoble-g5k-dahu',
                     'grenoble-g5k-troll', 'lyon-g5k-gemini', 'nancy-g5k-gros',
                     'nancy-g5k-grouille', 'marconi100', 'jacamar-hpc-ci-grvingt', 'cscs-daint']
    )

    # number of build jobs to use, defaults to all processors
    num_jobs = "$(nproc)"

    @run_after('init')
    def check_cluster_var(self):
        """Checks if cluster name is valid and skips the test if not"""
        self.skip_if(
            self.cluster not in self.valid_clusters,
            msg=f'Please choose a valid cluster name from {",".join(self.valid_clusters)}'
        )

    @run_after('init')
    def check_build_cache(self):
        """Checks if mirror name is defined if build cache is provided"""
        if self.spack_build_cache_dir and not self.spack_build_cache_mirror_name:
            self.spack_build_cache_mirror_name = 'mymirror'

    @run_after('init')
    def set_keep_files(self):
        """Keep spack config file in output"""
        self.keep_files = [f'{self.cluster}.yml']

    def export_spack_root(self):
        """Modify .bashrc to export SPACK_ROOT env variable"""
        spack_root_env = f'export SPACK_ROOT={self.spack_root}'
        spack_source = f'source {self.spack_root}/share/spack/setup-env.sh'
        bashrc_path = os.path.join(os.path.expanduser('~'), '.bashrc')
        # If ~/.bashrc not found, warning message pops up so that user can add to
        # these lines to appropriate shell
        try:
            bashrc_contents = [line.rstrip() for line in open(bashrc_path, 'r').readlines()]
            if spack_root_env not in bashrc_contents:
                with open(bashrc_path, 'a') as f:
                    f.write(f'\n\n{spack_root_env}\n\n')
        except FileNotFoundError:
            getlogger().warning(
                '$HOME/.bashrc not found. Please add lines "%s" and "%s" '
                'to appropriate shell profile' % (spack_root_env, spack_source)
            )

    def apply_patch(self):
        """Apply patches to Spack"""
        # Spack patches directory
        patch_dir = os.path.join(modules.utils.sdp_benchmark_tests_root(), 'spack', 'patches')

        # Get all patches in the directory
        patches = next(os.walk(patch_dir), (None, None, []))[2]
        with osext.change_dir(self.spack_root):
            for patch in patches:
                # Ignore hidden files and non patch files
                if not patch.endswith('.patch') or patch.startswith('.'):
                    continue
                patch_file = os.path.join(patch_dir, patch)
                try:
                    osext.run_command(f'git apply {patch_file} -R --check', check=True, shell=True)
                    getlogger().debug('Patch %s already applied' % patch_file)
                except (SpawnedProcessError, SpawnedProcessTimeout):
                    getlogger().debug('Applying patch %s' % patch_file)
                    osext.run_command(f'git apply {patch_file}', check=True, shell=True)

    @run_after('setup')
    def emit_spack_installation_cmds(self):
        """Commands to check if Spack is installed on the host"""
        if not os.path.isdir(self.spack_root):
            getlogger().debug('Spack installation not found. This test will install Spack at %s'
                              % self.spack_root)
            os.makedirs(self.spack_root, exist_ok=True)
            osext.git_clone(url=self.spack_url, targetdir=self.spack_root,
                            opts=['--depth 1', f'--branch {self.spack_ver}'])

            # Add SPACK_ROOT env variable and source spack env lines to $HOME/.bashrc
            # Let the user add this to their profile
            self.export_spack_root()

        # Apply patches to Spack. We are currently using a stable v0.17.0 version
        # Current issues are fixed in patches wile upstream PRs go through
        self.apply_patch()

        self.prerun_cmds.append(f'source {self.spack_root}/share/spack/setup-env.sh')

    @run_after('setup')
    def set_num_tasks(self):
        """Reserve all cores on the node if job is remote"""
        if not self.local:
            if self.current_partition.scheduler.registered_name == 'oar':
                self.num_tasks_per_node = (self.current_partition.processor.num_cpus //
                                           self.current_partition.processor.num_cpus_per_core)
            else:
                self.num_tasks_per_node = self.current_partition.processor.num_cpus
            self.num_tasks = self.num_tasks_per_node

    @run_after('setup')
    def emit_spack_add_mirror_cmds(self):
        """Commands to add mirrors to spack if build cache is found"""
        if self.spack_build_cache_dir:
            cmd_str = f'source {self.spack_root}/share/spack/setup-env.sh && spack mirror list'
            cmd_out = osext.run_command(cmd_str, check=True, shell=True).stdout.splitlines()
            all_mirrors = []
            for line in cmd_out:
                all_mirrors.append(line.split()[0])
            if self.spack_build_cache_mirror_name not in all_mirrors:
                self.prerun_cmds.append(f'spack mirror add {self.spack_build_cache_mirror_name} '
                                        f'{self.spack_build_cache_dir}')
            self.prerun_cmds.append(f'spack buildcache update-index -d '
                                    f'{self.spack_build_cache_dir}')
            self.prerun_cmds.append(f'spack buildcache keys --install --trust')
            pgpg_key_path = os.path.join(self.spack_build_cache_dir, 'build_cache', '_pgp')
            for (dir_path, _, filenames) in os.walk(pgpg_key_path):
                for filename in filenames:
                    if filename.endswith('pub'):
                        pgpg_key_file = os.path.join(dir_path, filename)
                        self.prerun_cmds.append(f'spack gpg trust {pgpg_key_file}')

    @run_after('setup')
    def emit_spack_env_rm_cmds(self):
        """Commands to removes Spack env if already exists"""
        self.env_dir = f'{self.spack_root}/var/spack/environments/{self.cluster}'
        if os.path.isdir(self.env_dir):
            self.prerun_cmds.append(f'spack env remove {self.cluster} -y')

    @run_after('setup')
    def merge_spack_configs(self):
        """Merge all seperate Spack config files into spack.yaml and place it stage dir"""
        input_spec_file = os.path.join(self.prefix, 'configs', 'spack.yml')
        self.spack_spec_file = os.path.join(self.stagedir, f'{self.cluster}.yml')
        modules.utils.merge_spack_configs(input_spec_file, self.spack_spec_file)

    @run_after('setup')
    def emit_spack_env_create_cmds(self):
        """Commands to create spack env"""
        self.prerun_cmds.append(f'spack env create {self.cluster} {self.spack_spec_file}')
        self.prerun_cmds.append(f'spack env activate {self.cluster}')

    @run_after('setup')
    def get_sys_compiler(self):
        """Read system compiler from config files"""
        with open(self.spack_spec_file) as f:
            spack_cfg = yaml.load(f, Loader=yaml.FullLoader)
        self.sys_compiler = spack_cfg['spack']['compilers'][0]['compiler']['spec']

    @run_after('setup')
    def emit_compiler_install_cmds(self):
        """Commands to install GCC 9.3.0 and Intel 2021.4.0 and adds to compiler.yml"""
        self.prerun_cmds.append(
            f'spack install -j{self.num_jobs} {self.gcc_toolchain} %{self.sys_compiler}'
        )
        self.prerun_cmds.append(f'spack compiler add $(spack location -i {self.gcc_toolchain})')
        if self.build_with_icc:
            self.prerun_cmds.append(
                f'spack install -j{self.num_jobs} {self.intel_toolchain} %{self.sys_compiler} --no-checksum'
            )
            self.prerun_cmds.append(
                f'spack compiler add $(spack location -i {self.intel_toolchain})'
                f'/compiler/latest/linux/bin/intel64'
            )
            self.prerun_cmds.append(
                f'spack compiler add $(spack location -i {self.intel_toolchain})'
                f'/compiler/latest/linux/bin'
            )
        self.prerun_cmds.append('spack concretize -f')

    @run_before('run')
    def set_local_launcher(self):
        """Set launcher to be local"""
        self.job.launcher = getlauncher('local')()

    @run_before('run')
    def set_executable(self):
        """Set executable and options"""
        self.executable = 'spack'
        self.executable_opts = ['install', f'-j{self.num_jobs}']

    @run_before('run')
    def set_postrun_cmds(self):
        """Set post run commands that generate module files"""
        if re.match(r'lmod*', str(self.current_system.modules_system)):
            mod_system = 'lmod'
        elif re.match(r'tmod*', str(self.current_system.modules_system)):
            mod_system = 'tcl'
        self.postrun_cmds = [
            f'spack module {mod_system} refresh --delete-tree -y',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns"""
        self.sanity_patterns = sn.all(
            [sn.assert_not_found(f'==> Error:', self.stderr)]
        )


def slurm_node_info(partition=None):
    """
    Get information about slurm nodes. Returns a sequence of dicts, one per node with keys/values
    all strs as follows:

      - "NODELIST": name of node
      - "NODES": number of nodes
      - "PARTITION": name of partition, * appended if default
      - "STATE": e.g. "idle"
      - "CPUS": str number of cpus
      - "S:C:T": Extended processor information: number of sockets, cores, threads in format "S:C:T"
      - "MEMORY": Size of memory in megabytes
      - "TMP_DISK": Size of temporary disk space in megabytes
      - "WEIGHT": Scheduling weight of the node
      - "AVAIL_FE": ?
      - "REASON": The reason a node is unavailable (down, drained, or draining states) or "none"

    See ``man sinfo`` for ``sinfo --Node --long`` for full details.

    :param partition: Name of slurm partition to query, else information for all partitions is
                      returned.
    :type partition: str
    :returns: nodes
    :rtype: list
    """

    sinfo_cmd = ['sinfo', '--Node', '--long']
    if partition:
        sinfo_cmd.append(f'--partition={partition}')
    nodeinfo = subprocess.run(sinfo_cmd, capture_output=True, check=True).stdout.decode('utf-8')

    nodes = []
    lines = nodeinfo.split('\n')
    header = lines[1].split()  # line[0] is date/time
    for line in lines[2:]:
        line = line.split()
        if not line:
            continue
        nodes.append({})
        for ci, key in enumerate(header):
            nodes[-1][key] = line[ci]

    return nodes


def hostlist_to_hostnames(s):
    """
    Convert a Slurm 'hostlist expression' to a list of individual node names. Uses `scontrol`
    command.

    :param s: Host list
    :type s: str
    :returns: Hostnames
    :rtype: list
    """

    hostnames = subprocess.run(
        ['scontrol', 'show', 'hostnames', s], capture_output=True, text=True, check=True
    ).stdout.split()

    return hostnames


class SchedulerInfo(object):
    """
    Information from the scheduler.

    The returned object has attributes:

        - ``num_nodes``: number of nodes
        - ``sockets_per_node``: number of sockets per node
        - ``pcores_per_node``: number of physical cores per node
        - ``lcores_per_node``: number of logical cores per node

    If ``rfm_partition`` is None the above attributes describe the **default** scheduler
    partition. Otherwise the following ``sbatch`` directives in the `access` property of the
    ReFrame partition will affect the information returned:

        - ``--partition``
        - ``--exclude``

    :param rfm_partition: ``reframe.core.systems.SystemPartition``
    :param exclude_states: sequence of str, exclude nodes in these Slurm node states
    :param only_states: sequence of str, only include nodes in these Slurm node states
    """

    def __init__(self, rfm_partition, exclude_states=None, only_states=None):

        self.rfm_partition = rfm_partition
        self.exclude_states = exclude_states
        self.only_states = only_states

        # Call scheduler specific method
        try:
            getattr(self, f'{self.rfm_partition.scheduler.registered_name}_info')()
        except AttributeError:
            raise NotImplementedError(
                f'Scheduler {self.rfm_partition.scheduler.registered_name} implementation not found'
            )

    def slurm_info(self):
        """Get SLURM scheduler info"""

        slurm_partition_name = None
        slurm_excluded_nodes = []
        exclude_states = [] if self.exclude_states is None else self.exclude_states
        only_states = [] if self.only_states is None else self.only_states
        if self.rfm_partition is not None:
            for option in self.rfm_partition.access:
                if '--partition=' in option:
                    _, slurm_partition_name = option.split('=')
                if '--exclude' in option:
                    _, exclude_hostlist = option.split('=')
                    slurm_excluded_nodes = hostlist_to_hostnames(exclude_hostlist)

        # filter out nodes we don't want:
        nodeinfo = []
        for node in slurm_node_info(slurm_partition_name):
            if slurm_partition_name is None and not node['PARTITION'].endswith(
                    '*'):  # filter to default partition
                continue
            if node['NODELIST'] in slurm_excluded_nodes:
                continue
            if node['STATE'].strip('*') in exclude_states:
                continue
            if only_states and node['STATE'] not in only_states:
                continue
            if self.rfm_partition.name.startswith('daint-'):
                if node['AVAIL_FE'].split(',')[0] != 'gpu':
                    continue
            nodeinfo.append(node)

        self.num_nodes = len(nodeinfo)
        cpus = [n['S:C:T'] for n in nodeinfo]
        if not len(set(cpus)) == 1:
            raise ValueError(
                f'Cannot summarise CPUs - description differs between nodes:\n{cpus}'
            )
        sockets, cores, threads = [int(v) for v in
                                   cpus[0].split(':')]  # nb each is 'per' the preceeding
        self.sockets_per_node = sockets
        self.pcores_per_node = sockets * cores
        self.lcores_per_node = sockets * cores * threads

    def squeue_info(self):
        return self.slurm_info()

    def oar_info(self):
        """Get OAR scheduler info"""

        oar_partition_name = None
        if self.rfm_partition is not None:
            for option in self.rfm_partition.access:
                if '-p cluster=' in option:
                    _, oar_partition_name = option.replace('\'', '').split('=')

        allNodeInfo = json.loads(
            subprocess.run(['oarnodes', '-J'], capture_output=True,
                           text=True, check=True).stdout.rstrip()
        )
        nodeinfo = []
        listnodes = subprocess.run(
            ['oarnodes', '-l'], capture_output=True, text=True, check=True
        ).stdout.rstrip().splitlines()
        for node_name in listnodes:
            if oar_partition_name and oar_partition_name in node_name:
                node = {}
                for _, properties in allNodeInfo.items():
                    if (properties['host'] == node_name and
                            'core_count' in properties.keys() and
                            'thread_count' in properties.keys() and
                            'cpu_count' in properties.keys()):
                        node['NAME'] = node_name
                        node['SOCKETS'] = properties['cpu_count']
                        node['CORES'] = properties['core_count']
                        node['THREADS'] = properties['thread_count']
                nodeinfo.append(node)
        self.num_nodes = len(nodeinfo)
        self.sockets_per_node = nodeinfo[0]['SOCKETS']
        self.pcores_per_node = nodeinfo[0]['CORES']
        self.lcores_per_node = nodeinfo[0]['THREADS']

    def local_info(self):
        """Get info when scheduler is local"""

        # check if OAR_NODEFILE env exists
        if 'OAR_NODEFILE' in os.environ.keys():
            # read the file
            with open(os.environ['OAR_NODEFILE'], 'rb') as nf:
                nodeinfo = nf.splitlines()
        elif 'SLURM_JOB_NODELIST' in os.environ.keys():
            nodeinfo = subprocess.run(['scontrol', 'show', 'hostnames',
                                       os.environ['SLURM_JOB_NODELIST']],
                                      capture_output=True, text=True, check=True
                                      ).stdout.rstrip().splitlines()
        elif 'PBS_NODEFILE' in os.environ.keys():
            with open(os.environ['PBS_NODEFILE'], 'rb') as nf:
                nodeinfo = nf.splitlines()
        else:
            nodeinfo = [socket.gethostname()]
        self.num_nodes = len(nodeinfo)
        self.sockets_per_node = self.rfm_partition.processor.num_sockets
        self.pcores_per_node = (self.rfm_partition.processor.num_cpus /
                                self.rfm_partition.processor.num_cpus_per_core)
        self.lcores_per_node = self.rfm_partition.processor.num_cpus

    def __str__(self):
        descr = ['%s=%s' % (k, getattr(self, k))
                 for k in ['num_nodes', 'sockets_per_node', 'pcores_per_node', 'lcores_per_node']]

        return 'SchedulerInfo(%s)' % (', '.join(descr))


if __name__ == '__main__':

    # pylint: disable=W0621
    # will need something like:
    # $ PYTHONPATH=$PWD:$PWD/reframe:$PWD/reframe/external python modules/reframe_extras.py
    class scheduler:
        """Define scheduler object"""
        def __init__(self):
            self.registered_name = 'slurm'

    class rfm_par:
        """Define partition object"""
        def __init__(self, scheduler):
            self.scheduler = scheduler
            self.access = ['--partition=full']

    if len(sys.argv) == 1:
        rfm_part = rfm_par(scheduler())
        print(SchedulerInfo(rfm_partition=rfm_part))
    else:
        print(SchedulerInfo(sys.argv[1]))
