# pylint: disable=C0301
"""
Custom Script Benchmark
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

This test runs a custom script which is provided by the user.
The script path is provided as a test variable from the CLI.

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test uses the variable system to pass an executable / script to the test.
Let's say I want to run the binary omp-stream with some params and I know where it's located,
I can invoke the test like this:

.. code-block:: bash
  reframe -C reframe_config.py -c apps/reframe_custom_script_test.py \
    -S script="~/BabelStream/build/omp-stream --arraysize $((10**9)) --numtimes 10" \
    -r


The run script is passed to the variable "script" and can contain arbitrary code.

To change the time limit you can use the parameter "time". By default, the time limit set in the partition
configuration is used:
.. code-block:: bash
  reframe -C reframe_config.py -c apps/reframe_custom_script_test.py \
    -S script="~/BabelStream/build/omp-stream --arraysize $((10**9)) --numtimes 10" \
    -S time="0d2h0m0s" \
    -r

This time limit needs to be at most as high as the partition supports. It can be in a string in the format
"<days>d<hours>h<minutes>m<seconds>s" or in seconds.
"""
# pylint: enable=C0301

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
from modules.base_class import RunOnlyBenchmarkBase
from reframe.core.backends import getlauncher  # pylint: disable=import-error

from reframe.core.builtins import variable, run_after, run_before
from modules.utils import filter_systems_by_env

# pylint: disable=C0116,E0401,E0602,E1101,W0201

ENV_NAME = "rfm_FftCpuTest"


@rfm.simple_test
class CustomScriptTest(RunOnlyBenchmarkBase):
    descr = 'Custom Script Test'

    # The script which should be run. By default, no operation is performed.
    script = variable(str, value="-c noop")

    # Specifies the launcher. Usually local, replaced with "srun" or "mpirun" in an MPI context.
    launcher = variable(str, value="local")

    # Time limit for the job. If unset, the default time limit of the partition is used.
    time = variable(str, value="")

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['custom_script']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Manuel Stutz (manuel.stutz@fhnw.ch)'
        ]
        self.num_nodes = 1

    @run_after('setup')
    def set_time_limit(self):
        # Only set the time_limit if a variable is passed.
        # Otherwise, take the default limit as configured in the partition configuration.
        if self.time != "":
            self.time_limit = self.time

    @run_after('setup')
    def set_launcher(self):
        """Set launcher according to variable."""
        self.job.launcher = getlauncher(self.launcher)()

    @run_after('setup')
    def set_executable(self):
        """Set executable and its options"""
        script_parts = self.script.split(' ')
        self.executable = script_parts[0]

        if len(script_parts) > 1:
            self.executable_opts = script_parts[1:]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns
        """
        self.sanity_patterns = sn.all([
            sn.assert_not_found(r'Error', self.stdout),
            sn.assert_not_found(r'Error', self.stderr),
        ])
