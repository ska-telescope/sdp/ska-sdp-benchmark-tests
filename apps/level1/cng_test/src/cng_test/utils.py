"""Utility functions"""

import numpy as np
from py3nvml.py3nvml import *


def get_num_gpus():
    """
    Get number of NVIDIA GPUs on the node

    :return: Number of GPUs
    :rtype: int
    """

    try:
        # Initialise NVML
        nvmlInit()

        # Number of devices
        num_gpus = nvmlDeviceGetCount()

        # Teardown NVML
        nvmlShutdown()
    except NVMLError:
        return 0
    
    return num_gpus


def pretty_print_results(config, results):
    """
    Pretty print results in a tabular format

    :param config: Config dict
    :param results: List of results
    :return: None
    """

    # pylint: disable=C0301
    # Print config details
    print('-----------------------------------------------------------------------------------------')
    print(f"# Image size:                            {config['scale'] * 1024} x {config['scale'] * 1024}")
    print(f"# Pixel size (in degrees):               {config['pixsize'] * 180 / np.pi:.3e}")
    print(f"# Field of view (in degrees):            {config['scale'] * 1024 * config['pixsize'] * 180 / np.pi:.3e}")
    print(f"# Minimum frequency:                     {config['min_freq']:.3e}")
    print(f"# Maximum frequency:                     {config['max_freq']:.3e}")
    print(f"# Number of baselines:                   {config['nbaselines']}")
    print(f"# Integration interval (in sec):         {config['interval']}")
    print(f"# Precision:                             {config['precision']}")
    print(f"# Accuracy:                              {config['epsilon']}")
    print(f"# Number of iterations:                  {config['niter']}")

    # Find lengths of row elements
    length_list = [len(str(element)) for row in results for element in row]

    # Find column width
    column_width = max(length_list)

    print('==========================================================================================')
    print('            CUDA NIFTY Benchmark results using synthetic SKA1 MID dataset                 ')
    print('==========================================================================================')
    for row in results:
        _, _, _, _, _, _, status = row
        if status == 'Pass':
            print('# ' + ''.join(str(element).ljust(column_width + 2) for element in row[1:-2]))

    print('-----------------------------------------------------------------------------------------')
    print('# End of table')

    all_status = [r[-1] for r in results[1:]]
    if 'Pass' not in all_status:
        print('# All tests have failed')
    elif 'Fail' in all_status:
        print('# Some tests have failed')
    else:
        print('# All tests have successfully finished')
    # pylint: enable=C0301
