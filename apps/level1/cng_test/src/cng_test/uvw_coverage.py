"""This module contains all the supporting functions for coordinate transformations"""

# All these functions are stolen from RASCIL.
# https://gitlab.com/ska-telescope/external/rascil

import os
import numpy as np

from astropy import units
from astropy.coordinates import EarthLocation
from astropy.coordinates import SkyCoord


# Global variables
DATA_DIR = os.path.abspath('cfgdata')
LOW_LOCATION = EarthLocation(
        lon=116.76444824 * units.deg, lat=-26.824722084 * units.deg, height=300.0
    )
MID_LOCATION = EarthLocation(
    lon=21.443803 * units.deg, lat=-30.712925 * units.deg, height=1053.000000
)


def generate_baselines(nant):
    """
    Generate mapping from antennas to baselines

    :param nant: Number of antennas
    :return: List of tuples of baselines
    """

    baselines = []
    for ant1 in range(0, nant):
        for ant2 in range(ant1, nant):
            baselines.append((ant1, ant2))

    return baselines


def xyz_to_uvw(xyz, ha, dec):
    """
    Rotate :math:`(x,y,z)` positions in earth coordinates to
    :math:`(u,v,w)` coordinates relative to astronomical source
    position :math:`(ha, dec)`. Can be used for both antenna positions
    as well as for baselines.

    Hour angle and declination can be given as single values or arrays
    of the same length. Angles can be given as radians or astropy
    quantities with a valid conversion.

    :param xyz: :math:`(x,y,z)` co-ordinates of antennas in array
    :param ha: hour angle of phase tracking centre (:math:`ha = ra - lst`)
    :param dec: declination of phase tracking centre.
    """

    # return eci_to_uvw(xyz, ha, dec)
    x, y, z = np.hsplit(xyz, 3)  # pylint: disable=unbalanced-tuple-unpacking

    # Two rotations:
    #  1. by 'ha' along the z axis
    #  2. by '90-dec' along the u axis
    u = x * np.cos(ha) - y * np.sin(ha)
    v0 = x * np.sin(ha) + y * np.cos(ha)
    w = z * np.sin(dec) - v0 * np.cos(dec)
    v = z * np.cos(dec) + v0 * np.sin(dec)

    return np.hstack([u, v, w])


def xyz_at_latitude(local_xyz, lat):
    """
    Rotate local XYZ coordinates into celestial XYZ coordinates. These
    coordinate systems are very similar, with X pointing towards the
    geographical east in both cases. However, before the rotation Z
    points towards the zenith, whereas afterwards it will point towards
    celestial north (parallel to the earth axis).

    :param lat: target latitude (radians or astropy quantity)
    :param local_xyz: Array of local XYZ coordinates
    :return: Celestial XYZ coordinates
    """

    # return enu_to_eci(local_xyz, lat)
    x, y, z = np.hsplit(local_xyz, 3)  # pylint: disable=unbalanced-tuple-unpacking

    lat2 = np.pi / 2 - lat
    y2 = -z * np.sin(lat2) + y * np.cos(lat2)
    z2 = z * np.cos(lat2) + y * np.sin(lat2)

    return np.hstack([x, y2, z2])


def lla_to_ecef(lat, lon, alt):
    """
    Convert WGS84 spherical coordinates to ECEF cartesian coordinates.
    :param lat: Latitude
    :param lon: Longitude
    :param alt: Altitude
    :return: ECEF cartesian coordinates
    """
    WGS84_a = 6378137.00000000
    WGS84_b = 6356752.31424518

    N = WGS84_a ** 2 / np.sqrt(
        WGS84_a ** 2 * np.cos(lat) ** 2 + WGS84_b ** 2 * np.sin(lat) ** 2
    )

    x = (N + alt) * np.cos(lat) * np.cos(lon)
    y = (N + alt) * np.cos(lat) * np.sin(lon)
    z = ((WGS84_b ** 2 / WGS84_a ** 2) * N + alt) * np.sin(lat)

    return x, y, z


def ecef_to_enu(location, xyz):
    """
    Convert ECEF coordinates to ENU coordinates relative to reference location.
    :param location: Current WGS84 coordinate
    :param xyz: ECEF coordinate
    :result : enu
    """
    # ECEF coordinates of reference point
    lon = location.geodetic[0].to(units.rad).value
    lat = location.geodetic[1].to(units.rad).value
    alt = location.geodetic[2].to(units.m).value
    x, y, z = np.hsplit(xyz, 3)  # pylint: disable=unbalanced-tuple-unpacking

    center_x, center_y, center_z = lla_to_ecef(lat, lon, alt)

    delta_x, delta_y, delta_z = x - center_x, y - center_y, z - center_z
    sin_lat, cos_lat = np.sin(lat), np.cos(lat)
    sin_lon, cos_lon = np.sin(lon), np.cos(lon)

    e = -sin_lon * delta_x + cos_lon * delta_y
    n = -sin_lat * cos_lon * delta_x - sin_lat * sin_lon * delta_y + cos_lat * delta_z
    u = cos_lat * cos_lon * delta_x + cos_lat * sin_lon * delta_y + sin_lat * delta_z

    return np.hstack([e, n, u])


def generate_ska1_mid_uv_coverage(interval):
    """
    Read SKA1 MID antenna config file and generate uv coverage data

    :param interval: Integration interval in sec
    :returns: Array of uvw coordinates
    """

    # SKA MID latitude and phase center
    latitude = MID_LOCATION.geodetic[1].to("rad").value
    phasecentre = SkyCoord(
        ra=+180.0 * units.deg, dec=-45.0 * units.deg, frame="icrs", equinox="J2000"
    )

    # Snapshot times
    times = np.arange(-4 * 3600.0, +4 * 3600, interval) * np.pi / 43200.0
    ntimes = len(times)

    # Path to the antenna config file
    ant_file = os.path.join(DATA_DIR, 'ska1mid.cfg')

    # Load antenna data
    ant_coord = np.genfromtxt(ant_file, skip_header=5, usecols=[0, 1, 2], delimiter=" ",
                              dtype=np.float64)
    ant_names = np.genfromtxt(ant_file, dtype="str", skip_header=5, usecols=[4], delimiter=" ")
    ant_coord = ecef_to_enu(MID_LOCATION, ant_coord)
    ant_coord = xyz_at_latitude(ant_coord, latitude)

    # Get total number of antennas and generate baseline tuples
    num_ants = len(ant_names)
    baselines = generate_baselines(num_ants)
    nbaselines = len(baselines)

    # Initialise the uvw coordinates
    uvw = np.zeros([ntimes, nbaselines, 3], dtype=np.float64)
    for iha, ha in enumerate(times):
        # Loop over all pairs of antennas. Note that a2>a1
        ant_pos = xyz_to_uvw(ant_coord, ha, phasecentre.dec.rad)
        for ibaseline, (a1, a2) in enumerate(baselines):
            uvw[iha, ibaseline, :] = ant_pos[a2, :] - ant_pos[a1, :]

    # Reshape uvw into 2d array
    uvw = uvw.reshape([ntimes * nbaselines, 3])

    # Flip u and w (Not sure why! This is done like this in RASCIL)
    uvw[:, 0] *= -1.0
    uvw[:, 2] *= -1.0

    return uvw
