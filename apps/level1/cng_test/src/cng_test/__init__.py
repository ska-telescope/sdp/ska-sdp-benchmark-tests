"""Core module for CUDA NIFTY gridder benchmark test"""

from cng_test.arg_parser import parse_args
from cng_test.set_logger import set_logger
from cng_test.core import cng_test_entrypoint

from cng_test._version import (
    __version__,
    __version_info__,
)

__author__ = 'Mahendra Paipuri'
__email__  = 'mahendra.paipuri@inria.fr'


def main():
    """Main test body"""

    # Parse command line arguments
    config = parse_args()

    # Set logger
    set_logger(config)

    # Entry point to the test
    cng_test_entrypoint(config)
