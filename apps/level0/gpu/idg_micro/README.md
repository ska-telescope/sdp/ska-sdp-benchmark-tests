# Imaging Domain Test (IDG) benchmarks

This app contains the benchmarks defined for IDG (de)gridder. More details on implemented benchmarks, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#idg-test).

The Jupyter notebook `IDGMICRO.ipynb` can be used to plot all the performance metrics that are monitored and extracted during benchmarking tests. The notebook must be invoked from the current directory. If not, please change the repository root and ReFrame root directories in the notebook preamble.
