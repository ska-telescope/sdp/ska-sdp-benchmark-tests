# pylint: disable=C0301
"""
Babel Stream benchmark
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

Babel Stream is inspired from `STREAM <http://www.cs.virginia.edu/stream/ref.html>`_ benchmark to
measure the memory bandwidth on GPUs. It supports several other programming models for CPUs as
well. More details can be found in the `documentation <https://uob-hpc.github.io/BabelStream/>`_.

.. note::
  Although the benchmark supports various programming models, currently the test uses only OMP,
  TBB and CUDA models.

Test variants
~~~~~~~~~~~~~~~~~~~~~~~~

Currently, three different variants of the benchmark are included in the test. They are

- ``omp``: Using OpenMP threading model
- ``tbb``: Using Intel's TBB model
- ``cuda``: Using CUDA model for GPUs

The test is parameterised for these models and a specific test can be chosen at the runtime using
``-t`` flag on CLI. An example is shown in the :ref:`bs usage`.

Test configuration
~~~~~~~~~~~~~~~~~~~~~~~~

Like STREAM benchmark, Babel stream uses 3 arrays of size ``N`` for different kernels. The size
of the arrays that will be used in the benchmark kernels can be configured at the run time using
``mem_size`` variable. Currently, the default value for ``mem_size`` is 0.4, which means the
array size is chosen in such a way that all three arrays will occupy 40 % of total memory available.

.. note::
  Depending on the GPU, sometimes we might get an error saying not enough space availble to store
  buffers. Decrease the ``mem_size`` in that case to allocate smaller arrays.


.. _bs usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/babel_stream/reframe_babelstream.py --run --performance-report

To run only ``omp`` variant and skip rest of the models, use ``-t`` flag as follows:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/babel_stream/reframe_babelstream.py -t omp$ --run --performance-report

To change the default value of ``mem_size`` during runtime, use ``-S`` flag. For example to use 30% of total memory:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/babel_stream/reframe_babelstream.py -S mem_size=0.3 --run --performance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
from modules.base_class import BenchmarkBase
from reframe.core.backends import getlauncher  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error

from modules.reframe_extras import MultiRunMixin
from modules.utils import filter_systems_by_env
from reframe.core.builtins import parameter, variable, run_after, run_before, performance_function

# pylint: disable=C0116,E0401,E0602,E1101,W0201

# Global variables
# Maximum int that is multiple of 1024
INT_MAX = 2147482624


@rfm.simple_test
class BabelStreamTest(BenchmarkBase, MultiRunMixin):
    """Babel stream test main class"""

    descr = 'Babel Stream test to measure memory bandwidth on GPUs and CPUs'

    # Variants of the test
    prg_model = parameter(['omp', 'tbb', 'cuda'])

    # Variables to define number of nodes
    num_nodes = variable(int, value=1)

    # Percentage of total memory to be used in array size
    mem_use = variable(float, value=0.4)

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            f'babel-stream-{self.prg_model}'
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        self.sourcesdir = 'https://github.com/UoB-HPC/BabelStream.git'
        self.exclusive_access = True
        self.time_limit = '0d1h00m0s'
        self.num_times = 20
        self.tags |= {
            self.prg_model
        }
        if 'g5k' in self.current_system.name:
            self.build_locally = False

    @run_after('setup')
    def set_num_tasks(self):
        """Set number of tasks for job"""
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
        else:
            self.num_tasks_per_node = self.current_partition.processor.num_cpus
        self.num_tasks = self.num_nodes * self.num_tasks_per_node

    @run_after('setup')
    def set_array_size(self):
        """Set array size to be a certain percentage of main memory"""
        try:
            if self.prg_model == 'cuda':
                aval_memory = int(self.current_partition.extras['gpu_mem'])
            else:
                aval_memory = int(self.current_partition.extras['mem'])
        except (KeyError, AttributeError):
            self.skip(msg='Memory info in system partition not found. '
                          'Please add memory info at current_partition.extras["mem"] '
                          'and/or current_partition.extras["gpu_mem"]')
        # For babel stream INT_MAX is maximum acceptable int
        # For CUDA model, array_size must be multiple of 1024
        self.array_size = min(int(self.mem_use * aval_memory / 8 / 3 / 1024) * 1024, INT_MAX)

    @run_after('setup')
    def set_launcher(self):
        """Set launcher to local to avoid appending mpirun or srun"""
        self.job.launcher = getlauncher('local')()

    @run_after('setup')
    def build_executable(self):
        """Set build system and config options"""
        self.build_system = 'CMake'
        self.build_system.builddir = os.path.join(
            self.stagedir, 'build'
        )

        # cscs-daint, licallo and jeanzay requires model to be lowercase ("omp", "tbb" and "cuda") while others seem to require uppercase
        allowed_systems = ["cscs-daint", "licallo", "jeanzay"]
        pm = self.prg_model
        if self.current_system.name not in allowed_systems:
            pm = pm.upper()
        self.build_system.config_opts = [
            f'-DMODEL={pm}',
        ]

        if self.prg_model == 'tbb':
            self.build_system.config_opts += [
                '-DONE_TBB_DIR=$TBBROOT',
            ]
        if self.prg_model == 'cuda':
            self.build_system.config_opts += [
                '-DCMAKE_CUDA_COMPILER=$(which nvcc)',
                f'-DCUDA_ARCH=sm_{self.current_partition.devices[0].arch}',
                '-DMEM=MANAGED',
            ]

    @run_before('run')
    def export_env_vars(self):
        """Export env variables using OMPI_MCA param for OpenMPI"""
        # This is needed for g5k clusters as env vars are not exported to all nodes in
        # the reservation
        self.env_vars['OMPI_MCA_mca_base_env_list'] = \
            f'\"PATH;LD_LIBRARY_PATH;{";".join(self.env_vars.keys())}\"'
            
    @run_before('run')
    def set_my_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'git={osext.git_repo_hash(short=False, wd=self.prefix)}',
            f'num_nodes={self.num_nodes}',
            f'array_size={self.array_size}',
            f'prg_model={self.prg_model}',
        }

    @run_before('run')
    def set_executable(self):
        """Set name of executable and runtime options"""
        self.executable = f'./build/{self.prg_model}-stream'
        self.executable_opts = [
            f'-s {self.array_size}',
            f'-n {self.num_times}',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """
        Set sanity patterns. Example stdout:

        .. code-block:: text

            # BabelStream
            # Version: 3.4
            # Implementation: OpenMP
            # Running kernels 100 times
            # Precision: double
            # Array size: 268.4 MB (=0.3 GB)
            # Total size: 805.3 MB (=0.8 GB)
            # Function    MBytes/sec  Min (sec)   Max         Average
            # Copy        69666.441   0.00771     0.01172     0.00783
            # Mul         67689.368   0.00793     0.01323     0.00811
            # Add         75708.142   0.01064     0.01792     0.01090
            # Triad       76265.085   0.01056     0.01411     0.01071
            # Dot         103668.530  0.00518     0.01109     0.00547
        """
        self.sanity_patterns = sn.all([
            sn.assert_found('Copy', self.stdout),
            sn.assert_found('Mul', self.stdout),
            sn.assert_found('Add', self.stdout),
            sn.assert_found('Triad', self.stdout),
            sn.assert_found('Dot', self.stdout),
        ])

    @performance_function('MB/s')
    def extract_bw(self, kind='Copy'):
        """Extract bandwidth metric"""
        return sn.extractsingle(rf'{kind}\s+(?P<value>\S+)\s+\S+', self.stdout, 'value', float)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance metrics"""
        self.perf_variables = {
            'Copy': self.extract_bw(),
            'Mul': self.extract_bw(kind='Mul'),
            'Add': self.extract_bw(kind='Add'),
            'Triad': self.extract_bw(kind='Triad'),
            'Dot': self.extract_bw(kind='Dot'),
        }

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf metrics"""
        self.reference = {
            '*': {
                'Copy': (None, None, None, 'MB/s'),
                'Mul': (None, None, None, 'MB/s'),
                'Add': (None, None, None, 'MB/s'),
                'Triad': (None, None, None, 'MB/s'),
                'Dot': (None, None, None, 'MB/s'),
            }
        }
