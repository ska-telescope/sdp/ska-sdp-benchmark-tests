# pylint: disable=C0301
"""
GPUDirect RDMA Benchmark Tests
---------------------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

The GPUDirect RDMA (GDR) technology exposes GPU memory to I/O devices by enabling the direct
communication path between GPUs in two remote systems. This feature eliminates the need to use the
system CPUs to stage GPU data in and out intermediate system memory buffers. As a result the
end-to-end latency is reduced and  the sustained bandwidth is increased (depending on the PCIe
topology).

The GDRCopy (GPUDirect RDMA Copy) library leverages the GPUDirect RDMA APIs to create CPU memory
mappings of the GPU memory. The advantage of a CPU driven copy is the very small overhead involved.
That is helpful when low latencies are required.

.. note::
  OSU micro benchmark suite is used to test the GDR capabilities in the current test setting.
  A more lower level verbs tests can also be used if the user wishes to remove the overhead imposed
  by MPI.

.. _gdr benchmarks:

Included benchmarks
~~~~~~~~~~~~~~~~~~~~~~~~

Currently, the test includes following categories of benchmarks:

Type of benchmark:

- ``bw``: Uni directional bandwidth test
- ``bibw``: Bi directional bandwidth test
- ``latency``: Latency test

Communication type:

- ``D_D``: Device to device
- ``D_H``: Device to host
- ``H_D``: Host to device

By default all the combination of tests will be performed. Both types of tests are parameterised and
the user can select one or more of these tests at the run time using tags. This will be discussed
in :ref:`gdr usage`.

Each of these tests will be executed in four different modes:

- GPUDirect RDMA and GDR Copy Enabled
- GPUDirect RDMA Enabled and GDR Copy Disabled
- GPUDirect RDMA Disabled and GDR Copy Enabled
- GPUDirect RDMA and GDR Copy Disabled

This will enable us investigate the effect of each component on the bandwidth and latency.

Benchmark configuration
~~~~~~~~~~~~~~~~~~~~~~~~

There are two important variables for this test that need to be taken care of. They are

- ``net_adptr``: Network adapter to use (default: ``mlx5_0:1``)
- ``ucx_tls``: UCX transport modes (default: ``['rc', 'cuda_copy']``)

The value for ``net_adptr`` can be passed in two different ways:

- In the system/partition configuration as the key value pair in ``extras`` field.
- Other option would be to use ``-S`` flag at CLI to set the variable.

**The value defined using** ``-S`` **flag has precedence over the system configuration value**.
If none of them are set, default value is used in the test. An example on how to define in ``extras``
is as follows:

.. code-block:: yaml

   'extras': {
                'interconnect': '100',  # in Gb/s
                'gpu_mem': '42505076736',  # in bytes
                'gdr_test_net_adptr': 'mlx5_0:1',  # NIC that has end-to-end connectivity for GDR test
            }


An optimal settings of these variables is necessary in order to leverage the available bandwidth of
Infiniband (IB) stack. We should choose the network adapter that has end-to-end connectivity with
GPUs.

.. tip::
    We can get this information from ``nvidia-smi topo -m`` command output. A typical output from
    this command can be as follows:

    .. code-block:: bash

                 GPU0    mlx5_0  mlx5_1  mlx5_2  mlx5_3  mlx5_4  mlx5_5  CPU Affinity    NUMA Affinity
         GPU0     X      NODE    NODE    PIX     PIX     PIX     PIX     0-19    0
         mlx5_0  NODE     X      PIX     NODE    NODE    NODE    NODE
         mlx5_1  NODE    PIX      X      NODE    NODE    NODE    NODE
         mlx5_2  PIX     NODE    NODE     X      PIX     PIX     PIX
         mlx5_3  PIX     NODE    NODE    PIX      X      PIX     PIX
         mlx5_4  PIX     NODE    NODE    PIX     PIX      X      PIX
         mlx5_5  PIX     NODE    NODE    PIX     PIX     PIX      X

         Legend:

          X    = Self
          SYS  = Connection traversing PCIe as well as the SMP interconnect between NUMA nodes (e.g., QPI/UPI)
          NODE = Connection traversing PCIe as well as the interconnect between PCIe Host Bridges within a NUMA node
          PHB  = Connection traversing PCIe as well as a PCIe Host Bridge (typically the CPU)
          PXB  = Connection traversing multiple PCIe bridges (without traversing the PCIe Host Bridge)
          PIX  = Connection traversing at most a single PCIe bridge
          NV#  = Connection traversing a bonded set of # NVLinks

    We have to make sure to use adapter with the PIX attribute (single PCIe bridge). In this case
    mlx5_2, mlx5_3, mlx5_4 and mlx5_5 are directly connected to PCI express and we can choose any of them

Similarly, for the case of UCX transport methods, we can choose the ones that are available on the
system. This information can be gathered using ``ucx_info -d`` which lists all the available
transport methods. These default variables can be overridden from CLI which will be shown in
:ref:`gdr usage`.

.. _gdr usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/gdr_test/reframe_gdr.py --run --performance-report

If we want to set ``ucx_tls`` to ``['dc', 'cuda-copy']`` and ``net_adptr`` to ``mlx5_3:1`` we can
use ``-S`` flag as follows

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/gdr_test/reframe_gdr.py -S net_adptr=mlx5_3:1 -S ucx_tls=dc,cuda_copy --run --performance-report

Similarly, if we want to restrict the tests to only ``D_D`` (device to device) and ``bw``
(uni bandwidth), we can use tags as follows

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/gdr_test/reframe_gdr.py -t D_D -t bw$ --run --peformance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import reframe as rfm
from modules.base_class import RunOnlyBenchmarkBase
from reframe.core.builtins import variable, parameter, run_after, run_before, performance_function
from reframe.core.logging import getlogger  # pylint: disable=import-error
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.typecheck as types  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error

from modules.utils import filter_systems_by_env
from modules.reframe_extras import patch_launcher_command, MultiRunMixin


# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class GpuDirectRdmaTest(RunOnlyBenchmarkBase, MultiRunMixin):
    """GPU Direct RDMA test to benchmark bandwidth and latency between inter node GPUs"""

    descr = 'Test to measure bandwidth and latency between RDMA enabled inter node GPUs'

    # Variable to define network adapter to use
    # Two places to set this variable
    # - current_partition.extras['gdr_test_net_adptr']
    # - CLI using `-S net_adptr=<port_name>`
    # CLI definition will take precedence over partition defined variable
    # If none of them are set, a default of mlx5_0:1 is used
    net_adptr = variable(str, type(None), value=None)

    # Variable to define UCX transport modes
    ucx_tls = variable(types.List[str], value=[
        'rc', 'cuda_copy'
    ])

    # Variable to define number of nodes in the reservation
    num_nodes = variable(int, value=2)

    # Parameterisation of benchmark test
    benchmark = parameter(['bw', 'bibw', 'latency'])

    # Parameterisation of communication type
    comm_type = parameter(['D_D', 'D_H', 'H_D'])

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'gpu-direct-rdma',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        self.num_tasks = self.num_nodes
        self.num_tasks_per_node = 1
        self.sourcesdir = None
        self.exclusive_access = True
        self.time_limit = '0d2h00m0s'
        self.tags |= {self.benchmark, self.comm_type}

    @run_after('init')
    def gen_msg_sizes(self):
        """Generate the message sizes used in benchmark in bytes"""
        min_msg_bytes = 1
        n_msg_sizes = 23
        self.msg_sizes = [min_msg_bytes * 2 ** i for i in range(n_msg_sizes)]

    @run_after('init')
    def override_net_adptr_from_sys_config(self):
        """Override network adapter variable if found in sys config"""
        if self.net_adptr is None:
            try:
                self.net_adptr = self.current_partition.extras['gdr_test_net_adptr']
            except (AttributeError, KeyError):
                self.net_adptr = 'mlx5_0:1'
        getlogger().debug(f'net_adptr is defined as %s' % self.net_adptr)

    @run_after('setup')
    def set_git_commit_tag(self):
        """Fetch git commit hash"""
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {f'git={git_ref}'}

    @run_after('setup')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'benchmark={self.benchmark}',
            f'communication_type={self.comm_type}',
            f'network_adapter={self.net_adptr}',
            f'ucx_tls={self.ucx_tls}',
            f'num_nodes={self.num_nodes}',
        }

    @run_after('setup')
    def set_env_variables(self):
        """Set environment variables"""
        self.variables = {
            'UCX_NET_DEVICES': self.net_adptr,
            'CUDA_VISIBLE_DEVICES': '0',
            'UCX_RNDV_THRESH': '32768',
        }
        
    @run_after('setup')
    def patch_job_launcher(self):
        """Monkey mock the job launcher command"""
        # Monkey mocking the actual job launcher command to remove -np/-n
        # argument.
        if self.job.launcher.registered_name in ['mpirun', 'mpiexec']:
            self.job.launcher.command = patch_launcher_command

    @run_after('setup')
    def set_test_cases(self):
        """Define all the test cases and corresponding env variables"""
        ucx_tls = ",".join(self.ucx_tls)
        self.cases = {
            'RDMA_GDR_Copy_Enabled':
                f'export UCX_IB_GPU_DIRECT_RDMA=1 UCX_TLS={ucx_tls},gdr_copy',
            'RDMA_Enabled_GDR_Copy_Disabled':
                f'export UCX_IB_GPU_DIRECT_RDMA=1 UCX_TLS={ucx_tls}',
            'RDMA_Disabled_GDR_Copy_Enabled':
                f'export UCX_IB_GPU_DIRECT_RDMA=0 UCX_TLS={ucx_tls},gdr_copy',
            'RDMA_GDR_Copy_Disabled':
                f'export UCX_IB_GPU_DIRECT_RDMA=0 UCX_TLS={ucx_tls}',
        }
        # In case if scheduler does not export env variables, we export with OpenMPI MCA parameter
        for key, val in self.cases.items():
            self.cases[key] = " ".join(
                [val, 'OMPI_MCA_mca_base_env_list=\"PATH;LD_LIBRARY_PATH;UCX_NET_DEVICES;'
                      'CUDA_VISIBLE_DEVICES;UCX_RNDV_THRESH;UCX_IB_GPU_DIRECT_RDMA;UCX_TLS\"']
            )

    @run_after('setup')
    def add_launcher_options(self):
        """Add job launcher options"""
        self.launcher_name = self.job.launcher.registered_name
        if self.launcher_name == 'mpirun':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [
                f'-np {self.num_tasks}', f'-npernode {self.num_tasks_per_node}',
                f'--map-by dist --mca rmaps_dist_device "{self.net_adptr}"',
            ]
        elif self.launcher_name == 'mpiexec':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [
                f'-n {self.num_tasks}', f'-ppn {self.num_tasks_per_node}',
            ]
        elif self.launcher_name == 'srun':
            # Same goes with --nodes=<num_tasks_job> flag
            self.job.launcher.options = [
                f'--ntasks={self.num_tasks}',
                f'--ntasks-per-node={self.num_tasks_per_node}',
                '--accel-bind=n',
            ]

    @run_before('run')
    def set_executable(self):
        """Set executable and options"""
        self.executable = f'osu_{self.benchmark}'
        self.executable_opts = [
            '-x 100', '-i 1000', f"-d cuda {self.comm_type.replace('_', ' ')}",
        ]

    @run_before('run')
    def get_full_job_cmd(self):
        """Get full job command to use it in different tests"""
        cmd = self.job.launcher.run_command(self.job)
        exec_opts = ' '.join(self.executable_opts)
        self.full_job_cmd = f'{cmd} {self.executable} {exec_opts}'

    @run_before('run')
    def set_prerun_cmds(self):
        """Set prerun commands. Set env variables for case of RDMA and GDR copy enabled"""
        case = 'RDMA_GDR_Copy_Enabled'
        self.prerun_cmds = [
            f'echo "Test with {case} started"',
            self.cases[case],
        ]

    @run_before('run')
    def set_postrun_cmds(self):
        """Set post run commands. Run rest of the cases"""
        self.rem_cases = list(self.cases.keys()).copy()
        self.rem_cases.remove('RDMA_GDR_Copy_Enabled')
        self.postrun_cmds = [
            f'echo "Test with RDMA_GDR_Copy_Enabled finished"'
        ]
        for case in self.rem_cases:
            self.postrun_cmds.append(f'echo "Test with {case} started"')
            self.postrun_cmds.append(self.cases[case])
            self.postrun_cmds.append(self.full_job_cmd)
            self.postrun_cmds.append(f'echo "Test with {case} finished"')

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:

        .. code-block:: text

            # Test with RDMA_GDR_Copy_Enabled started
            # OSU MPI-CUDA Bandwidth Test v5.7.1
            # Send Buffer on DEVICE (D) and Receive Buffer on DEVICE (D)
            # Size      Bandwidth (MB/s)
            # 1                       1.89
            # 2                       3.80
            # 4                       7.65
            # 8                      15.25
            # Test with RDMA and GDR Copy Enabled finished

        """
        self.sanity_patterns = sn.all([
            sn.assert_found(f'{case} finished', self.stdout) for case in self.cases.keys()
        ])

    def parse_stdout(self, msg_size, case):
        """Read the stdout file to extract perf metrics

        Args:
            msg_size (int): Size of the message in bytes
            case (str): Test case

        Returns:
            float: Metric value
        """
        with open(self.stdout.evaluate()) as f:
            for line in f:
                if f'Test with {case} started' in line:
                    while f'Test with {case} finished' not in line:
                        line = next(f)
                        row = line.split()
                        if str(msg_size) == row[0]:
                            return float(row[1])
        return 0

    @performance_function('MB/s')
    def extract_bw(self, msg_size=1, case='RDMA_GDR_Copy_Enabled'):
        """Performance function to extract uni bandwidth"""
        return self.parse_stdout(msg_size, case)

    @performance_function('MB/s')
    def extract_bibw(self, msg_size=1, case='RDMA_GDR_Copy_Enabled'):
        """Performance function to extract bi bandwidth"""
        return self.parse_stdout(msg_size, case)

    @performance_function('us')
    def extract_latency(self, msg_size=1, case='RDMA_GDR_Copy_Enabled'):
        """Performance function to extract latency"""
        return self.parse_stdout(msg_size, case)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            f'1_RDMA_GDR_Copy_Enabled': getattr(self, f'extract_{self.benchmark}')(),
        }
        for case in self.rem_cases:
            self.perf_variables[f'1_{case}'] = getattr(
                self, f'extract_{self.benchmark}'
            )(case=case)
        for case in self.cases.keys():
            for msg_size in self.msg_sizes[1:]:
                self.perf_variables[f'{msg_size}_{case}'] = \
                    getattr(self, f'extract_{self.benchmark}')(msg_size, case)

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf values"""
        self.reference = {
            '*': {
                '*': (None, None, None, 'MB/s'),
            }
        }
