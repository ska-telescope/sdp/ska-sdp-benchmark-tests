# GPUDirect RDMA (GDR) benchmark

This app contains GPUDirect RDMA (GDR) benchmark test. More details on benchmark, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#gpuirect-rdma-benchmark-tests).

The Jupyter notebook `GDR.ipynb` can be used to tabulate and plot bandwidth and latency metrics that are extracted during benchmarking tests. The notebook must be invoked from the `ska-sdp-benchmark-tests` root directory.
