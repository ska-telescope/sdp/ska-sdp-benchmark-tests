# See the LICENSE file at the top-level directory of this distribution.

"""
.py file to run DFT functions from ska-sdp-func.
Two versions of the DFT are avaiable and can be run together or individually.
Runs CPU version of function and GPU version if GPU is present.
Accepts command line arguments for input variables, as shown below.
Outputs elapsed time for CPU and GPU (if available) separately.

ska-sdp-func must be installed as detailed here:
https://developer.skao.int/projects/ska-sdp-func/en/latest/install.html

usage: reframe_test_dft.py [-h] [-s START] [-n STEP] [-p {1,4}] [-c CHANNELS] [-b BASELINES] [-t TIMES] [-x NUM_COMPONENTS] [-v {0,1,2} [-d {0,1,2}]

DFT function benchmark test

optional arguments:
  -h, --help                                            show this help message and exit
  -s START, --start START                               Channel start in Hz
  -n STEP, --step STEP                                  Channel step in Hz
  -p {1,4}, --pols {1,4}                                Number of polarisations, 1 or 4 allowed
  -c CHANNELS, --channels CHANNELS                      Number of channels
  -b BASELINES, --baselines BASELINES                   Number of baselines
  -t TIMES, --times TIMES                               Number of times
  -x NUM_COMPONENTS, --num_components NUM_COMPONENTS    Number of components
  -v {0,1,2}, --version {0,1,2}                         Version of DFT to test, 0 = v00, 1 = v01, 2 = both versions
  -d {0,1,2}, --device {0,1,2}                          Device to run the DFT on, 0 = CPU, 1 = GPU, 2 = CPU and GPU

Output format for each DFT version and device =

v0X DFT on XPU:
Channel Start                   XXXXXXXXX Hz
Channel Step                    XXXXXXXXX Hz
Channels                        XXXX
Polarisations                   X
Baselines                       XXXX
Times                           XXXX
Components                      XXXX
Elapsed Time                    XXXX s
"""

import time
import argparse
import numpy

try:
    import cupy
except ImportError:
    cupy = None

from ska_sdp_func.visibility import dft_point_v00, dft_point_v01

C_0 = 299792458.0


# Create the parser
my_parser = argparse.ArgumentParser(description='DFT function benchmark test')

my_parser.add_argument('-s',
                       '--start',
                       action='store',
                       help='Channel start in Hz',
                       type=int,
                       default=100e6)

my_parser.add_argument('-n',
                       '--step',
                       action='store',
                       help='Channel step in Hz',
                       type=int,
                       default=100e3)

my_parser.add_argument('-p',
                       '--pols',
                       action='store',
                       choices=[1, 4],
                       help='Number of polarisations, 1 or 4 allowed',
                       type=int,
                       default=4)

my_parser.add_argument('-c',
                       '--channels',
                       action='store',
                       help='Number of channels',
                       type=int,
                       default=10)

my_parser.add_argument('-b',
                       '--baselines',
                       action='store',
                       help='Number of baselines',
                       type=int,
                       default=351)

my_parser.add_argument('-t',
                       '--times',
                       action='store',
                       help='Number of times',
                       type=int,
                       default=10)

my_parser.add_argument('-x',
                       '--num_components',
                       action='store',
                       help='Number of components',
                       type=int,
                       default=20)

my_parser.add_argument('-v',
                       '--version',
                       action='store',
                       choices=[0, 1, 2],
                       help='Version of DFT to test, 0 = v00, 1 = v01, 2 = both versions',
                       type=int,
                       default=2)

my_parser.add_argument('-d',
                       '--device',
                       action='store',
                       choices=[0, 1, 2],
                       help='Device to run the DFT on, 0 = CPU, 1 = GPU, 2 = CPU and GPU',
                       type=int,
                       default=2)

# Execute parse_args()
args = my_parser.parse_args()

channel_start = args.start
channel_step = args.step
pols = args.pols
channels = args.channels
baselines = args.baselines
times = args.times
components = args.num_components
dft_version = args.version
device = args.device


# Version v00 DFT
def test_dft_v00(num_pols, num_channels, num_baselines,
                 num_times, num_components, device):
    """Test DFT function."""

    v00_cpu_end_time = 0 
    v00_gpu_end_time = 0

# Create input and output arrays
    fluxes = (
        numpy.random.random_sample([num_components, num_channels, num_pols])
        + 0j
    )
    directions = numpy.random.random_sample([num_components, 3])
    uvw_lambda = numpy.random.random_sample(
        [num_times, num_baselines, num_channels, 3]
    )
    vis = numpy.zeros(
        [num_times, num_baselines, num_channels, num_pols],
        dtype=numpy.complex128,
    )

    if device in (0, 2):
    # Run DFT test on CPU, using numpy arrays.

        print("Testing DFT v00 on CPU from ska-sdp-func...")
        # CPU start time
        v00_cpu_start_time = time.time()
        dft_point_v00(directions, fluxes, uvw_lambda, vis)
        # CPU end time
        v00_cpu_end_time = time.time() - v00_cpu_start_time

    # Run DFT test on GPU, using cupy arrays.
    if device in (1, 2):
        if cupy:
            # Create input and output arrays
            fluxes_gpu = cupy.asarray(fluxes)
            directions_gpu = cupy.asarray(directions)
            uvw_lambda_gpu = cupy.asarray(uvw_lambda)
            vis_gpu = cupy.zeros(
                [num_times, num_baselines, num_channels, num_pols],
                dtype=numpy.complex128,
            )

            print("Testing DFT v00 on GPU from ska-sdp-func...")
            # GPU start time
            v00_gpu_start_time = time.time()
            dft_point_v00(directions_gpu, fluxes_gpu, uvw_lambda_gpu, vis_gpu)
            # GPU end time
            v00_gpu_end_time = time.time() - v00_gpu_start_time

        else:
            print('\nGPU unavailable')

    return v00_cpu_end_time, v00_gpu_end_time


# Verson v01 DFT
def test_dft_v01(channel_start_hz, channel_step_hz, num_pols, num_channels,
                 num_baselines, num_times, num_components, device):

    """Test DFT function."""

    v01_cpu_end_time = 0 
    v01_gpu_end_time = 0

    # Create input and output arrays
    fluxes = (
        numpy.random.random_sample([num_components, num_channels, num_pols])
        + 0j
    )
    directions = numpy.random.random_sample([num_components, 3])
    uvw = numpy.random.random_sample([num_times, num_baselines, 3])
    vis = numpy.zeros(
        [num_times, num_baselines, num_channels, num_pols],
        dtype=numpy.complex128,
    )

    if device in (0, 2):
        # Run DFT test on CPU, using numpy arrays.

        print("Testing DFT v01 on CPU from ska-sdp-func...")
        # CPU start time
        v01_cpu_start_time = time.time()
        dft_point_v01(
            directions, fluxes, uvw, channel_start_hz, channel_step_hz, vis
        )
        # CPU end time
        v01_cpu_end_time = time.time() - v01_cpu_start_time

    if device in (1, 2):
        # Run DFT test on GPU, using cupy arrays.
        if cupy:
            # Create input and output arrays
            fluxes_gpu = cupy.asarray(fluxes)
            directions_gpu = cupy.asarray(directions)
            uvw_gpu = cupy.asarray(uvw)
            vis_gpu = cupy.zeros(
                [num_times, num_baselines, num_channels, num_pols],
                dtype=numpy.complex128,
            )
            print("Testing DFT v01 on GPU from ska-sdp-func...")
            # GPU start time
            v01_gpu_start_time = time.time()
            dft_point_v01(
                directions_gpu,
                fluxes_gpu,
                uvw_gpu,
                channel_start_hz,
                channel_step_hz,
                vis_gpu,
            )
            # GPU end time
            v01_gpu_end_time = time.time() - v01_gpu_start_time

        else:
            print('\nGPU unavailable')

    return v01_cpu_end_time, v01_gpu_end_time


def print_details(version_used, device, elapsed_time, channel_start, channel_step, 
                    pols, channels, baselines, times, components):

    print(f'\n{version_used} DFT on {device}:')
    print(f'Channel Start \t\t\t{channel_start} Hz')
    print(f'Channel Step \t\t\t{channel_step} Hz')
    print(f'Channels \t\t\t{channels}')
    print(f'Polarisations \t\t\t{pols}')
    print(f'Baselines \t\t\t{baselines}')
    print(f'Times \t\t\t\t{times}')
    print(f'Components \t\t\t{components}')
    print(f'Elapsed Time \t\t\t{elapsed_time:.5f} s')


if dft_version in (0, 2):
    v00_cpu_time, v00_gpu_time = test_dft_v00(pols, channels, baselines,
                                                times, components, device)

    if device in (0, 2):
        print_details('v00', 'CPU', v00_cpu_time, channel_start, channel_step, 
                        pols, channels, baselines, times, components)

    if device in (1, 2) and cupy:
        print_details('v00', 'GPU', v00_gpu_time, channel_start, channel_step, 
                    pols, channels, baselines, times, components)

if dft_version in (1, 2):
    v01_cpu_time, v01_gpu_time = test_dft_v01(channel_start, channel_step,
                                                pols, channels, baselines,
                                                times, components, device)

    if device in (0, 2):
        print_details('v01', 'CPU', v01_cpu_time, channel_start, channel_step, 
                        pols, channels, baselines, times, components)

    if device in (1, 2) and cupy:
        print_details('v01', 'GPU', v01_gpu_time, channel_start, channel_step, 
                        pols, channels, baselines, times, components)
