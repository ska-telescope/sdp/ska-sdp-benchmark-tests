# pylint: disable=C0301
"""
Funclib Test
-------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

This test runs functions from `ska-sdp-func <https://gitlab.com/ska-telescope/sdp/ska-sdp-func>`.
Currently implemented are tests for DFT and Phase Rotation.

Test variables
~~~~~~~~~~~~~~~~~~~~~~~~

The DFT test supports two different polarisations as parameters.

The Phaserotation test supports two parameters, which are configured as tuples in one ReFrame parameter.
Those two parameters are "baselines" which are the number of baselines to be tested and "times".

Environment variables
~~~~~~~~~~~~~~~~~~~~~~~~

By default, the test will create a ``conda`` environment and run inside it for the
sake of isolation. This can be controlled using env variable ``CREATE_CONDA_ENV``.
By setting it to ``NO``, the test WILL NOT create a ``conda`` environment.

Similarly, the performance metrics are monitored using the `perfmon <https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon>`_ toolkit. If
the user does not want to monitor metrics, it can be achieved by setting
``MONITOR_METRICS=NO``.

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The tests can be run using the following commands:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/hippo_func_lib/reframe_funclib_test.py --run --performance-report

If we want to change the variables to non default values, we should use ``-S`` flag. For example, if we want to run only 5 major cycles and 64 frequency channels, use

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/hippo_func_lib/reframe_funclib_test.py -S start=1000000 --run --performance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
from modules.base_class import CompileOnlyBenchmarkBase, RunOnlyBenchmarkBase

from modules.reframe_extras import FetchSourcesBase, AppBase, UsesInternet
from modules.utils import filter_systems_by_env
from reframe.core.builtins import variable, parameter, fixture, run_before, performance_function, run_after
from reframe.core.backends import getlauncher
import modules.conda_env_manager as cem

# pylint: disable=C0116,E0401,E0602,E1101,W0201

# Global variables
# Define varibales that will be shared across tests here
# Each test will run in its own conda environment. We define name of that environment here
ENV_NAME = 'rfm_FunclibTest'


class FunclibTestDownload(FetchSourcesBase):
    """Fixture to fetch ska-sdp-func source code"""

    descr = 'Fetch source code of SKA-SDP-Func'
    sourcesdir = 'https://gitlab.com/ska-telescope/sdp/ska-sdp-func.git'
    cnd_env_name = ENV_NAME

    def __init__(self):
        super().__init__()
        self.postrun_cmds = ['git checkout 0.0.4-testing']


class FunclibTestBuild(CompileOnlyBenchmarkBase, UsesInternet):
    """Funclib test compile test"""

    descr = 'Compile ska-sdp-func from sources'

    # Share resource from fixture
    funclib_test_src = fixture(FunclibTestDownload, scope='session')

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['funclib-test']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Manuel Stutz (manuel.stutz@fhnw.ch)'
        ]
        self.executable = "echo noop"
        self.time_limit = '0d0h15m0s'
        # Cross compilation is not possible on certain g5k clusters. We force
        # the job to be non-local so building will be on remote node
        if 'g5k' in self.current_system.name:
            self.build_locally = False
        self.env_manager = cem.CondaEnvManager(ENV_NAME)

    @run_before('compile')
    def set_sourcedir(self):
        """Set source path based on dependencies"""
        self.sourcesdir = self.funclib_test_src.stagedir

    @run_before('compile')
    def set_prebuild_cmds(self):
        """Make local lib dirs"""
        self.lib_dir = os.path.join(self.stagedir, 'local')
        self.prebuild_cmds = [
            'export PYTHONPATH=',
            *self.env_manager.emit_conda_activate_cmds().split(';'),
            f'mkdir -p {self.lib_dir}',
        ]

    @run_before('compile')
    def set_build_system_attrs(self):
        """Set build directory and config options"""
        self.build_system = 'CMake'
        self.build_system.builddir = os.path.join(self.stagedir, 'build')
        self.build_system.config_opts = [
            f'-DCMAKE_INSTALL_PREFIX={self.lib_dir}',
        ]
        if 'cscs-daint' in self.current_system.name:
            # cross compiling on daint login nodes only works with specified CUDA_ARCH.
            self.build_system.config_opts.append(f"-DCUDA_ARCH=6.0")
        self.build_system.max_concurrency = 8

    @run_before('compile')
    def set_postbuild_cmds(self):
        """Install libs"""
        self.env_manager.install_conda('conda-forge::cupy')

        self.postbuild_cmds = [
            'make install',
            f'export SKA_SDP_FUNC_LIB_DIR={self.lib_dir}',
            f'python -m pip install ..'
        ]
        if self.build_locally and 'cscs-daint' in self.current_system.name:
            # cross compiling on daint login nodes only works with specified CUDA_ARCH.
            self.postbuild_cmds.insert(len(self.postbuild_cmds) - 1, f'export CMAKE_ARGS="-DCUDA_ARCH=6.0"')

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns"""
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)


@rfm.simple_test
class FunclibDftTest(RunOnlyBenchmarkBase):
    funclib_build_src = fixture(FunclibTestBuild, scope="environment")

    # Parameters
    # -b BASELINES, --baselines BASELINES   Number of baselines
    # -c CHANNELS,  --channels CHANNELS     Number of channels
    # Those two parameters are only required in specific configurations, so we use one parameter
    # with all required permutations and add a format-function for nice visual representation
    conf = parameter(
        [(64, 64), (64, 128), (64, 256), (64, 512), (64, 1024), (64, 2048), (64, 4096), (64, 8192), (64, 16384),
         (128, 64), (256, 64), (512, 64), (1024, 64), (2048, 64)],
        fmt=lambda a: f"baselines={a[0]},channels={a[1]}")

    # Variables
    #   -s START, --start START                              Channel start in Hz
    start = variable(int, value=int(100e6))

    #   -n STEP, --step STEP                                 Channel step in Hz
    step = variable(int, value=int(100e3))

    #   -p POLS, --pols POLS                                 Number of polarisations
    polarisation = variable(int, value=1)

    #   -t TIMES, --times TIMES                              Number of times
    times = variable(int, value=10)

    #   -x NUM_COMPONENTS, --num_components NUM_COMPONENTS   Number of components
    num_components = variable(int, value=20)

    #   -v {0,1,2}, --version {0,1,2}                        Version of DFT to test, 0 = v00, 1 = v01, 2 = both versions
    version = variable(int, value=2)

    #   -d {0,1,2}, --device {0,1,2}                         Device to run the DFT on, 0 = CPU, 1 = GPU, 2 = CPU and GPU
    device = variable(int, value=2)

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['funclib-test']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Manuel Stutz (manuel.stutz@fhnw.ch)'
        ]
        self.exclusive_access = True
        self.time_limit = '0d0h15m0s'
        self.sourcepath = "run_dft.py"
        self.prerun_cmds = [
            'export PYTHONPATH=',
        ]

        self.baselines = self.conf[0]
        self.channels = self.conf[1]

        self.executable = "python"
        self.executable_opts = [
            "run_dft.py",
            f"--start={self.start}",
            f"--step={self.step}",
            f"--pols={self.polarisation}",
            f"--channels={self.channels}",
            f"--baselines={self.baselines}",
            f"--times={self.times}",
            f"--num_components={self.num_components}",
            f"--version={self.version}",
            f"--device={self.device}",
        ]
        self.env_manager = cem.CondaEnvManager(ENV_NAME)

    @run_after('setup')
    def set_conda_cmds(self):
        self.prerun_cmds += self.env_manager.emit_conda_activate_cmds().split(';')

    @run_after('setup')
    def set_launcher(self):
        """Set launcher to local as it is no multi-node application"""
        self.job.launcher = getlauncher('local')()

    @run_after('setup')
    def set_tags(self):
        self.tags |= {
            f'start={self.start}',
            f"step={self.step}",
            f"pols={self.polarisation}",
            f"channels={self.channels}",
            f"baselines={self.baselines}",
            f"times={self.times}",
            f"num_components={self.num_components}",
            f"version={self.version}",
            f"device={self.device}",
        }

    @run_before('sanity')
    def set_sanity_patterns(self):
        """
        Expected Output
        For every device and every version, the following block is printed to stdout:

        .. code-block:: text

            v0X DFT on XPU:
            Channel Start                   XXXXXXXXX Hz
            Channel Step                    XXXXXXXXX Hz
            Channels                        XXXX
            Polarisations                   X
            Baselines                       XXXX
            Times                           XXXX
            Components                      XXXX
            Elapsed Time                    XXXX s
        """

        patterns = [
            sn.assert_not_found('error', self.stderr),
            sn.assert_not_found('Error', self.stderr),
        ]

        if self.version in (0, 2) and self.device in (0, 2):
            patterns.append(sn.assert_found('v00 DFT on CPU:', self.stdout))
        if self.version in (0, 2) and self.device in (1, 2):
            patterns.append(sn.assert_found('v00 DFT on GPU:', self.stdout))
        if self.version in (1, 2) and self.device in (0, 2):
            patterns.append(sn.assert_found('v01 DFT on CPU:', self.stdout))
        if self.version in (1, 2) and self.device in (1, 2):
            patterns.append(sn.assert_found('v01 DFT on GPU:', self.stdout))
        self.sanity_patterns = sn.all(patterns)

    @performance_function('s')
    def extract_v00_cpu_time(self):
        return sn.extractsingle(rf'^Elapsed Time\s+(?P<elapsed_time>\S+)\s+s', self.stdout, 'elapsed_time', float, 0)

    @performance_function('s')
    def extract_v00_gpu_time(self):
        return sn.extractsingle(rf'^Elapsed Time\s+(?P<elapsed_time>\S+)\s+s', self.stdout, 'elapsed_time', float, 1)

    @performance_function('s')
    def extract_v01_cpu_time(self):
        return sn.extractsingle(rf'^Elapsed Time\s+(?P<elapsed_time>\S+)\s+s', self.stdout, 'elapsed_time', float, 2)

    @performance_function('s')
    def extract_v01_gpu_time(self):
        return sn.extractsingle(rf'^Elapsed Time\s+(?P<elapsed_time>\S+)\s+s', self.stdout, 'elapsed_time', float, 3)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance metrics"""
        self.perf_variables = {}
        if self.version in (0, 2) and self.device in (0, 2):
            self.perf_variables.update({'elapsed_time_v00_cpu': self.extract_v00_cpu_time()})
        if self.version in (0, 2) and self.device in (1, 2):
            self.perf_variables.update({'elapsed_time_v00_gpu': self.extract_v00_gpu_time()})
        if self.version in (1, 2) and self.device in (0, 2):
            self.perf_variables.update({'elapsed_time_v01_cpu': self.extract_v01_cpu_time()})
        if self.version in (1, 2) and self.device in (1, 2):
            self.perf_variables.update({'elapsed_time_v01_gpu': self.extract_v01_gpu_time()})

    @run_before('performance')
    def set_reference_values(self):
        """Set reference performance values."""
        self.reference = {
            '*': {
                '*': (None, None, None, 's'),
            },
        }


@rfm.simple_test
class PhaserotTest(RunOnlyBenchmarkBase):
    phaserot_build_src = fixture(FunclibTestBuild, scope="environment")

    # Parameters
    # -b BASELINES, --baselines BASELINES   Number of baselines
    # -t TIMES, --times TIMES               Number of times
    # Those two parameters are only required in specific configurations, so we use one parameter
    # with all required permutations and add a format-function for nice visual representation
    conf = parameter([(64, 10), (64, 20), (64, 30), (64, 40), (64, 50),
                      (128, 10), (256, 10), (512, 10), (1024, 10), (2048, 10)],
                     fmt=lambda a: f"baselines={a[0]},times={a[1]}")

    # Variables
    # -s START, --start START               Channel start in Hz
    start = variable(int, value=int(100e6))

    # -n STEP, --step STEP                  Channel step in Hz
    step = variable(int, value=int(100e3))

    # -p POLS, --pols POLS                  Number of polarisations
    polarisation = variable(int, value=1)
    # -c CHANNELS, --channels CHANNELS      Number of channels
    channels = variable(int, value=10)

    # -d {0,1,2}, --device {0,1,2}          Device to run the Phase rotation on, 0 = CPU, 1 = GPU, 2 = CPU and GPU
    device = variable(int, value=2)

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['funclib-test']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Manuel Stutz (manuel.stutz@fhnw.ch)'
        ]
        self.exclusive_access = True
        self.time_limit = '0d0h15m0s'
        self.sourcepath = "run_phase_rotation.py"
        self.prerun_cmds = [
            'export PYTHONPATH='
        ]

        self.baselines = self.conf[0]
        self.times = self.conf[1]

        self.executable = "python"
        self.executable_opts = [
            "run_phase_rotation.py",
            f"--start={self.start}",
            f"--step={self.step}",
            f"--pols={self.polarisation}",
            f"--channels={self.channels}",
            f"--baselines={self.baselines}",
            f"--times={self.times}",
            f"--device={self.device}",
        ]
        self.env_manager = cem.CondaEnvManager(ENV_NAME)

    @run_after('setup')
    def set_conda_cmds(self):
        self.prerun_cmds += self.env_manager.emit_conda_activate_cmds().split(';')

    @run_after('setup')
    def set_launcher(self):
        """Set launcher to local as it is no multi-node application"""
        self.job.launcher = getlauncher('local')()

    @run_after('setup')
    def set_tags(self):
        self.tags |= {
            f'start={self.start}',
            f"step={self.step}",
            f"pols={self.polarisation}",
            f"channels={self.channels}",
            f"baselines={self.baselines}",
            f"times={self.times}",
            f"device={self.device}",
        }

    @run_before('sanity')
    def set_sanity_patterns(self):
        """
        Expected Output
        For every device the following block is printed to stdout:

        .. code-block:: text

            Phase Rotation on XPU:
            Channel Start                   XXXXXXXXX Hz
            Channel Step                    XXXXXXXXX Hz
            Channels                        XXXX
            Polarisations                   X
            Baselines                       XXXX
            Times                           XXXX
            Elapsed Time                    XXXX s
        """

        patterns = [
            sn.assert_not_found('error', self.stderr),
            sn.assert_not_found('Error', self.stderr),
        ]

        if self.device in (0, 2):
            patterns.append(sn.assert_found('Phase Rotation on CPU:', self.stdout))
        if self.device in (1, 2):
            patterns.append(sn.assert_found('Phase Rotation on GPU:', self.stdout))
        self.sanity_patterns = sn.all(patterns)

    @performance_function('s')
    def extract_cpu_time(self):
        return sn.extractsingle(rf'^Elapsed Time\s+(?P<elapsed_time>\S+)\s+s', self.stdout, 'elapsed_time', float, 0)

    @performance_function('s')
    def extract_gpu_time(self):
        return sn.extractsingle(rf'^Elapsed Time\s+(?P<elapsed_time>\S+)\s+s', self.stdout, 'elapsed_time', float, 1)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance metrics"""
        self.perf_variables = {}
        if self.device in (0, 2):
            self.perf_variables.update({'elapsed_time_cpu': self.extract_cpu_time()})
        if self.device in (1, 2):
            self.perf_variables.update({'elapsed_time_gpu': self.extract_gpu_time()})

    @run_before('performance')
    def set_reference_values(self):
        """Set reference performance values."""
        self.reference = {
            '*': {
                '*': (None, None, None, 's'),
            },
        }
