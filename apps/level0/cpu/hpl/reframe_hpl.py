# pylint: disable=C0301
"""
HPL benchmark
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

This is HPL microbenchmark test with using a single node as default test parameter. It is used as
reference benchmark to provide data for the `Top500 <http://top500.org/>`_ list and thus rank to
supercomputers worldwide. HPL rely on an efficient implementation of the Basic Linear Algebra
Subprograms (BLAS).

.. note::
  Currently, the implemented test uses the optimized version of benchmark shipped by Intel MKL
  library.

Test types
~~~~~~~~~~~~~~~~~~~~~~~~

Currently, two different tests are defined namely,

- ``HplGnuTest``: Based on GNU toolchain for non Intel processors
- ``HplMklTest``: Using benchmark shipped out of Intel MKL library for Intel processors

On Intel chips, we can use the precompiled binary that comes out-of-the-box from Intel MKL
library. But for non Intel systems, we need to compile using GNU tool chain with customized
make file. In the directory ``makes/``, we provide the make file for AMD chips using BLIS
as BLAS library. We can choose which test to run during runtime using CLI which is discussed in
:ref:`hpl usage`.


Test configuration file
~~~~~~~~~~~~~~~~~~~~~~~~

The prerequisite to run HPL benchmark is ``HPL.dat`` file that contains several benchmark
parameters. A sample configuration file looks like

.. code-block:: text

    HPLinpack benchmark input file
    Innovative Computing Laboratory, University of Tennessee
    HPL.out      output file name (if any)
    6            device out (6=stdout,7=stderr,file)
    4            # of problems sizes (N)
    29 30 34 35  Ns
    4            # of NBs
    1 2 3 4      NBs
    0            PMAP process mapping (0=Row-,1=Column-major)
    3            # of process grids (P x Q)
    2 1 4        Ps
    2 4 1        Qs
    16.0         threshold
    3            # of panel fact
    0 1 2        PFACTs (0=left, 1=Crout, 2=Right)
    2            # of recursive stopping criterium
    2 4          NBMINs (>= 1)
    1            # of panels in recursion
    2            NDIVs
    3            # of recursive panel fact.
    0 1 2        RFACTs (0=left, 1=Crout, 2=Right)
    1            # of broadcast
    0            BCASTs (0=1rg,1=1rM,2=2rg,3=2rM,4=Lng,5=LnM)
    1            # of lookahead depth
    0            DEPTHs (>=0)
    2            SWAP (0=bin-exch,1=long,2=mix)
    64           swapping threshold
    0            L1 in (0=transposed,1=no-transposed) form
    0            U  in (0=transposed,1=no-transposed) form
    1            Equilibration (0=no,1=yes)
    8            memory alignment in double (> 0)

More details on each parameter can be found in the
`tuning <https://www.netlib.org/benchmark/hpl/tuning.html#tips>`_ section of benchmark
documentation. This `link <https://www.advancedclustering.com/act_kb/tune-hpl-dat-file/>`_
can be used to generate a ``HPL.dat`` file for a given runtime configuration. Another useful link
in this context is `here <http://hpl-calculator.sourceforge.net/hpl-calculations.php>`_.

Currently, the test supports automatic generation of ``HPL.dat`` file based on the system
configuration. The class ``GenerateHplConfig`` in ``modules.utils`` is used for this 
purpose. The problem size of HPL is dependent on the available system memory and it is
generally recommended to set a size that occupies at least 80% of the system memory. It means
for systems that have big memory, a very huge problem size can be generated which can take a very 
long for the benchmark to run. Thus, we capped the system memory to 200 GB to avoid these very long
run times.

For the intel processors, we use **one MPI process** per node but for AMD chips, we use number of L3
caches as number of MPI processes and use number of cores attached to each L3 cache as number of OpenMP
threads.


.. _hpl usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpl/reframe_hpl.py --run --performance-report


We can set number of nodes on the CLI using:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpl/reframe_hpl.py --run --performance-report -S num_nodes=2

To choose a particular test during runtime using ``-n`` option as follows:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpl/reframe_hpl.py --run --performance-report -n HplGnuTest

We can run multiple tests corresponding to different MPI configurations
.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  vary_MPI=1 reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpl/reframe_hpl.py --run --performance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error
from modules.base_class import BenchmarkBase, RunOnlyBenchmarkBase

from modules.utils import GenerateHplConfig
from modules.utils import filter_systems_by_env
from modules.reframe_extras import patch_launcher_command, MultiRunMixin
from reframe.core.builtins import variable, run_after, run_before, performance_function, parameter

# pylint: disable=C0116,E0401,E0602,E1101,W0201,W0231

# Global variables
# Define NB for different architectures
HPL_NB = {
    'sandybridge': 256,
    'broadwell': 192,
    'haswell': 192,
    'skylake_avx512': 384,
}


class HplMixin(rfm.RegressionMixin):
    """Common methods and attributes for HPL main tests"""
    
    vary_MPI = variable(int, value=int(os.getenv('vary_MPI', default=0)))
    
    # Variables to define number of nodes
    if vary_MPI == 1:
        # number of nodes is set to 1, 2 and 4, based on the range() function with values (0, 3).
        num_nodes = parameter(1 << i for i in range(0, 3))
    else:
        # Default value number of node = 1
        num_nodes = variable(int, value=1)


    @run_after('init')
    def test_settings(self):
        """Common test settings"""
        self.exclusive_access = True
        self.time_limit = '0d2h00m0s'
        # Maintainers
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
    
    @run_after('setup')
    def set_git_commit_tag(self):
        """Fetch git commit hash"""
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {f'git={git_ref}'}

    @run_before('run')
    def export_env_vars(self):
        """Export env variables using OMPI_MCA param for OpenMPI"""
        # This is needed for g5k clusters as env vars are not exported to all nodes in
        # the reservation
        self.env_vars['OMPI_MCA_mca_base_env_list'] = \
            f'\"PATH;LD_LIBRARY_PATH;{";".join(self.env_vars.keys())}\"'

    @run_before('run')
    def generate_config(self):
        """Generate HPL config file and place it in stagedir"""
        try:
            node_mem = self.current_partition.extras['mem']
        except (KeyError, AttributeError):
            self.skip('Could not retrieve memory info of the partition. Please '
                      'add info at extras["mem"] in partition config.')
        arch = self.current_partition.processor.arch
        hpl_config = GenerateHplConfig(num_nodes=self.hpl_args[0],
                                       num_procs=self.hpl_args[1],
                                       nb=HPL_NB.get(arch, 192),
                                       mem=node_mem)
        contents = hpl_config.get_config()

        # Write contents to file
        with open(os.path.join(self.stagedir, 'HPL.dat'), 'w') as cfg:
            cfg.write(contents)
            
    @run_before('run')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'num_procs={self.num_tasks}',
        }
            
    @run_before('sanity')
    def set_sanity_patterns(self):
        """
        Set sanity patterns. Example stdout:

        .. code-block:: text

           # Finished      1 tests with the following results:
           #               1 tests completed and passed residual checks,
           #               0 tests completed and failed residual checks,
           #               0 tests skipped because of illegal input values
           # --------------------------------------------------------------------------------
           # End of Tests.

        """
        self.sanity_patterns = sn.all([
            sn.assert_found('End of Tests.', self.stdout),
            sn.assert_found('0 tests completed and failed residual checks', self.stdout),
            sn.assert_found('0 tests skipped because of illegal input values.', self.stdout)
        ])

    @performance_function('Gflop/s')
    def extract_gflops(self):
        """Performance extraction function for Gflops. Sample stdout:

        .. code-block: text

           # WC00C2R2      115584   192     1     1            2021.86            5.09167e+02

        """
        return sn.extractsingle(
            r'W(.*)[R|C]\S+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d[\d.]+\s+(\d[\d.eE+]+)',
            self.stdout, 2, float
        )

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            'Gflop/s': self.extract_gflops(),
        }

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf variables"""
        self.reference = {
            '*': {
                'Gflop/s': (None, None, None, 'Gflop/s'),
            }
        }


@rfm.simple_test
class HplGnuTest(BenchmarkBase, HplMixin, MultiRunMixin):
    """Main class of HPL test based on GNU toolchain"""

    descr = 'HPL reference benchmark based on GNU toolchain'
    
    # Architecture
    arch = variable(str, value='Linux_AMD_BLIS')

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['gnu-hpl']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        # Cross compilation might not be always possible on Grid50000 clusters
        if 'g5k' in self.current_system.name:
            self.build_locally = False

    @run_after('setup')
    def set_num_tasks(self):
        """Set number of processes and threads based on L3 cache"""
        for cache in self.current_partition.processor.topology['caches']:
            if cache['type'] == 'L3':
                l3cache = cache
                break
        # Number of MPI processes are number of L3 caches
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
            self.num_mpi_tasks_per_node = (self.current_partition.processor.num_cpus 
                                           // l3cache['num_cpus'])
            self.num_mpi_tasks = self.num_nodes * self.num_mpi_tasks_per_node
            self.job.launcher.command = patch_launcher_command
            self.job.launcher.options = [
                f'-np {self.num_mpi_tasks}', f'-npernode {self.num_mpi_tasks_per_node}'
            ]
            self.hpl_args = [self.num_nodes, self.num_mpi_tasks]  # [num_nodes, num_tasks]
        else:
            self.num_tasks_per_node = (self.current_partition.processor.num_cpus 
                                       // l3cache['num_cpus'])
            self.job.launcher.options = [f'-npernode {self.num_tasks_per_node}']

            # [num_nodes, num_tasks]
            self.hpl_args = [self.num_nodes, self.num_nodes * self.num_tasks_per_node]
        self.num_cpus_per_task = l3cache['num_cpus']
        self.num_tasks = self.num_nodes * self.num_tasks_per_node
        
    @run_after('setup')
    def set_job_env_vars(self):
        """Set job specific env variables"""
        self.env_vars = {
            'OMP_NUM_THREADS': str(self.num_cpus_per_task),
            'OMP_PROC_BIND': 'TRUE',
            'OMP_PLACES': 'cores',
        }
        
    @run_after('setup')
    def download_hpl(self):
        """Download HPL 2.3 source code"""
        osext.run_command(f'wget http://www.netlib.org/benchmark/hpl/hpl-2.3.tar.gz '
                          f'-P {self.stagedir}', check=True)
        osext.run_command(f'tar -xf {self.stagedir}/hpl-2.3.tar.gz -C {self.stagedir} '
                          '--strip-components=1', check=True)
        
    @run_after('setup')
    def set_topdir_makefile(self):
        """Set TOPDIR var in Makefile and copy to stagedir"""
        makefile = f'Make.{self.arch}'
        with open(os.path.join(self.stagedir, makefile), 'w') as outfile:
            with open(os.path.join(self.prefix, 'makes', makefile)) as infile:
                for line in infile:
                    if 'TOPdir       = <REPLACE_ME>' in line:
                        line = f'TOPdir       = {self.stagedir}\n'
                    outfile.write(line)
                    
    @run_before('compile')
    def emit_prebuild_cmds(self):
        """Make clean if already exists"""
        self.prebuild_cmds = [f'make arch={self.arch} clean_arch_all']
                    
    @run_before('compile')
    def build_executable(self):
        """Set build system and config options"""
        self.build_system = 'Make'
        self.build_system.options = [f'arch={self.arch}']
        self.build_system.max_concurrency = 8
        
    @run_before('run')
    def set_executable(self):
        """Set executable name"""
        self.executable = f'bin/{self.arch}/xhpl'

    @run_before('run')
    def job_launcher_opts(self):
        """Set job launcher options"""
        self.job.launcher.options += [
            f'--map-by l3cache:PE={self.num_cpus_per_task}', '--use-hwthread-cpus',
        ]
        

@rfm.simple_test
class HplMklTest(RunOnlyBenchmarkBase, HplMixin, MultiRunMixin):
    """Main class of HPL test based on MKL"""

    descr = 'HPL reference benchmark based on Intel MKL library'

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['intel-hpl']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        
    @run_after('setup')
    def set_num_tasks(self):
        """Set number of tasks for job"""
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
            self.num_cpus_per_task = self.current_partition.processor.num_cpus
            self.job.launcher.command = patch_launcher_command
            self.job.launcher.options = [f'-n {self.num_nodes}', '-ppn 1']
        else:
            self.num_tasks_per_node = 1
            self.num_cpus_per_task = self.current_partition.processor.num_cpus
        self.num_tasks = self.num_nodes * self.num_tasks_per_node
        self.hpl_args = [self.num_nodes, self.num_nodes]  # [num_nodes, num_tasks]
        
    @run_after('setup')
    def set_omp_threads(self):
        """Set number of OpenMP threads"""
        self.env_vars = {
            'OMP_NUM_THREADS': str(self.num_cpus_per_task),
        }
        
    @run_after('setup')
    def set_executable(self):
        """Set executable name"""
        self.executable = '$XHPL_BIN'
