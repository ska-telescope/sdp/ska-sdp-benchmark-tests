# HPCG benchmark

This app contains HPCG benchmark test. More details on benchmark, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#hpcg-benchmark).

The Jupyter notebook `HPCG.ipynb` can be used to tabulate and plot `Gflop/s` metric that are extracted during benchmarking tests. The notebook must be invoked from the `ska-sdp-benchmark-tests` root directory.
