# pylint: disable=C0301
"""
Intel MPI Benchmarks
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

Intel MPI Benchmarks (IMB) are used to measure application-level latency and bandwidth,
particularly over a high-speed interconnect, associated with a wide variety of MPI communication
patterns with respect to message size.

.. note::
  Currently, only benchmarks from ``IMB-MPI1`` components are included in the test.


.. _mpi benchmarks:

Included benchmarks
~~~~~~~~~~~~~~~~~~~~~~~~

Currently, the test includes following benchmarks:

- Pingpong
- Uniband
- Biband
- Sendrecv
- Allreduce
- Alltoall
- Allgather

By default all the above listed benchmarks will be run by the test. However, the user can choose
subset of these benchmarks at the runtime using CLI. This will be discussed in :ref:`imb usage`.


.. _mpi process:

Number of MPI processes
~~~~~~~~~~~~~~~~~~~~~~~~

By default benchmarks like Uniband, Biband, *etc*, are run with MPI processes varying from 2, 4, 8
and so on until the number of physical cores on the nodes. In order to reduce total number of
benchmarks, only two runs for each benchmark is chosen

- Run with 1 MPI process per node
- Run with ``N`` MPI processes per node where ``N`` is number of physical cores.

Effectively using this configuration, we are running test that establish upper and lower bounds of
benchmark metrics and thus minimising the time required for benchmarks to run.

Test configuration
~~~~~~~~~~~~~~~~~~~~~~~~

The only file that is needed for the test to run is placed in ``src/`` folder which provides the
list of message sizes to be tested in the benchmark. The file must be as follows:

.. include:: ../../../apps/level0/cpu/imb/src/msglens
   :literal:

If we want to test for more message sizes, simply add new lines in the file and place it in
``src/`` folder.

Test variables
~~~~~~~~~~~~~~~~~~~~~~~~

Different variables are available for the user to change the runtime configuration of the tests.
They are listed as follows:

- ``variants``: Benchmark variants that can be chosen as listed in :ref:`mpi benchmarks` (default: All benchmarks listed in :ref:`mpi benchmarks`).
- ``mem``: Memory allocated per MPI process (default: 1).
- ``timeout``: Timeout for running the benchmark for each message size (default: 2).

The variables ``mem`` and ``timeout`` are specific to IMB and more details about these variables
can be found in the `documentation <https://www.intel.com/content/www/us/en/develop/documentation/imb-user-guide/top/benchmark-methodology/command-line-control.html>`__.

.. tip::
  For benchmarks like Alltoall, Allgather, Allreduce involving many nodes, runs with bigger message
  sizes might timeout. In this case increase the ``timeout`` variable. Similarly, nodes with many
  cores and smaller memory size can pose problems when running benchmarks. As stated
  in :ref:`mpi process`, as many MPI processes as number of physical cores are used for benchmark
  runs. So, if the node has ``N`` physical cores and less than ``N GB`` of DRAM, benchmarks will
  fail due to lack of sufficient memory. In this case reduce the ``mem`` variable which reduces
  the memory allocated for each MPI process.

All these variables can be configured at the runtime from the CLI using ``-S`` flag of ReFrame.
It will be discussed in :ref:`imb usage`.

Test parameterisation
~~~~~~~~~~~~~~~~~~~~~~~~

The tests are parameterised based on the variable ``tot_nodes``. The current default value is 2.
This variable can **only** be configured from CLI using environment variable ``IMBTEST_NODES``.
Based on the ``tot_nodes`` value, the closest power of 2 is estimated and parameterised tests on
different number of nodes generated in the powers of 2 are used. For instance, if ``tot_nodes`` is
64, tests are run on 2, 4, 8, 16, 32 and 64 nodes. For each run, all the requested benchmarks will
be executed with different number of MPI processes as described in :ref:`mpi process`.

If the user wants to restrict number of nodes to only few runs, we can do it using ``-t`` flag on
the CLI.

.. _imb usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/imb/reframe_imb.py --exec-policy=serial --run --performance-report

.. important::
  It is absolutely necessary to use ``--exec-policy=serial`` option while running these benchmarks.
  By default ReFrame will execute tests in asynchronous mode, where all tests are executed at the
  same time. As we are interested in network latency and bandwidth metrics, it is advised to run
  these benchmarks serially so that they do not interfere with each other.


We can choose benchmark variants from CLI. For example, if we want to run only Uniband and Biband
benchmarks:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/imb/reframe_imb.py --exec-policy=serial --run --performance-report -S variants="Uniband","Biband"

Similarly other variables can also be configured from CLI. To use ``mem`` as 0.5 and ``timeout``
as 3.0:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/imb/reframe_imb.py --exec-policy=serial --run --performance-report -S mem=0.5 -S timeout=3.0

To set the total number of nodes from CLI using ``IMBTEST_NODES`` environment variable, use following:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  IMBTEST_NODES=16 reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/imb/reframe_imb.py --exec-policy=serial --run --performance-report

Finally, to select only few parameterised tests, we can use ``-t`` flag. For example, if
``tot_nodes`` is set to 16 and if we want to run only tests where number of nodes are 8 and 16,
we can do following:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  IMBTEST_NODES=16 reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/imb/reframe_imb.py --exec-policy=serial --run --performance-report -t 8$ -t 16$

All the above mentioned CLI flags can be used together without any side effects.

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os
import math

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.typecheck as types  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error
from modules.base_class import RunOnlyBenchmarkBase
from modules.reframe_extras import MultiRunMixin

from modules.utils import filter_systems_by_env
from reframe.core.builtins import variable, run_after, run_before, performance_function, parameter
from reframe.core.backends import getlauncher
# pylint: disable=C0116,E0401,E0602,E1101,W0201

# Global variables
# A dict of different benchmarks and index where we need to parse the output
# e.g. Uniband_bw can be found at line[2] after we split line in list of items
BENCHMARK_VARS = {
    'Uniband': [('Uniband_bw', 2), ],
    'Biband': [('Biband_bw', 2), ],
    'Sendrecv': [('Sendrecv_bw', 5), ('Sendrecv_lat', 4)],
    'Allreduce': [('Allreduce_tp', 4), ],
    'Alltoall': [('Alltoall_tp', 4), ],
    'Allgather': [('Allgather_tp', 4), ],
}


class ImbMixin(rfm.RegressionMixin):
    """Common test attributes for IMB test"""

    # Variants of the tests
    variants = variable(types.List[str], value=[
        'Uniband', 'Biband', 'Sendrecv', 'Allreduce', 'Alltoall', 'Allgather'
    ])
    # Memory allocated per process
    mem = variable(float, value=1.0)
    # Timeout for each message size
    timeout = variable(float, value=2.0)

    @run_after('init')
    def test_settings(self):
        self.exclusive_access = True
        self.time_limit = '0d2h00m0s'
        self.variant_attrs = {}
        self.valid_prog_environs = [
            'imb'
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        for variant in self.variants:
            self.variant_attrs[variant] = BENCHMARK_VARS[variant]

    @run_after('setup')
    def set_git_commit_tag(self):
        """Fetch git commit hash"""
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {f'git={git_ref}'}

    @run_after('setup')
    def get_l3_cache(self):
        """Get L3 cache size in MB and line size"""
        caches = self.current_partition.processor.topology['caches']
        for cache in caches:
            for name, value in cache.items():
                if name == 'type' and value == 'L3':
                    self.l3_cache_size = cache['size'] / 1e6
                    self.l3_cahce_line = cache['linesize']

    @run_after('setup')
    def set_num_tasks(self):
        """Set number of tasks for job"""
        self.num_tasks_per_node = 1
        self.num_tasks = self.num_nodes * self.num_tasks_per_node

    @run_after('setup')
    def get_msg_lens(self):
        """Read input message lengths"""
        self.msg_lens = open(
            os.path.join(self.prefix, self.sourcesdir, 'msglens'), 'r'
        ).read().splitlines()

    @run_after('setup')
    def set_executable(self):
        """Set executable name"""
        if self.current_system.name == "cscs-daint":
                    self.executable = "$IMB_MPI1_BIN"
        else:
            self.executable = 'IMB-MPI1'

    @run_before('run')
    def export_env_vars(self):
        """Export env variables using OMPI_MCA param for OpenMPI"""
        # This is needed for g5k clusters as env vars are not exported to all nodes in
        # the reservation
        self.env_vars['OMPI_MCA_mca_base_env_list'] = \
            f'\"PATH;LD_LIBRARY_PATH;{";".join(self.env_vars.keys())}\"'

    @run_before('run')
    def set_executable_opts(self):
        """Set executable options"""
        # Use the -off_cache flag to avoid cache re-use
        self.executable_opts = [
            f'-mem {self.mem}', f'-time {self.timeout}',
            '-msglen ./msglens',
            f'-off_cache {self.l3_cache_size},{self.l3_cahce_line}',
            f'-npmin {self.num_tasks}',
        ] + self.variants

    
    @run_before('run')
    def add_launcher_options(self):
        """Add job launcher commands"""
        # 
        if self.current_system.name == 'licallo':
            self.job.launcher = getlauncher('mpiexec.hydra')()  
            self.launcher_name = self.job.launcher
        else:
            self.launcher_name = self.job.launcher.registered_name
        # Add mpi processes for one proc per node case
        # process-per-node is 1 here
        if self.launcher_name == 'mpirun':
            self.job.launcher.options = [
                f'-npernode {self.num_tasks_per_node}'
            ]
        elif self.launcher_name == 'mpiexec':
            self.job.launcher.options = [
                f'-ppn {self.num_tasks_per_node}'
            ]
        elif self.launcher_name == 'srun':
            self.job.launcher.options = [
                f'--nodes={self.num_nodes}',
                f'--ntasks={self.num_tasks}',
                f'--tasks-per-node={self.num_tasks_per_node}',
            ]
        elif self.launcher_name == 'mpiexec.hydra':
            self.job.launcher.options = [
                f'-n {self.num_nodes}'
            ]

    @run_before('run')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'variants={self.variants}',
            f'num_tasks={self.num_tasks}',
            str(self.num_nodes),
        }

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:

        .. code-block: text

            # e.g.
            # #----------------------------------------------------------------
            # # Benchmarking Alltoall
            # # #processes = 60
            # #----------------------------------------------------------------

        """
        self.sanity_patterns = sn.all(
            [sn.assert_found(f'# Benchmarking {variant}', self.stdout) for variant in self.variants]
            + [sn.assert_found('# All processes entering MPI_Finalize', self.stdout)]
        )

    def parse_stdout(self, msg_len, var, ind):
        """Read stdout file to parse perf variables"""
        with open(self.stdout.evaluate()) as f:
            for line in f:
                if f'# Benchmarking {var}' in line:
                    line = next(f)
                    expect = f'# #processes ='
                    if expect in line:
                        while '#' in line:
                            line = next(f)
                        while line != '':
                            row = line.strip().split()
                            if int(row[0]) == msg_len:
                                return float(row[ind])
                            else:
                                line = next(f)
            return 0.0

    @performance_function('Mbytes/sec')
    def extract_bw(self, msg_len=0, var='PingPong', ind=3):
        """Performance extraction function for bandwidth"""
        return self.parse_stdout(msg_len, var, ind)

    @performance_function('usec')
    def extract_time(self, msg_len=0, var='PingPong', ind=2):
        """Performance extraction function for latency"""
        return self.parse_stdout(msg_len, var, ind)

    @run_before('performance')
    def set_perf_patterns(self):
        """
        Set performance variables. Sample stdout

        .. code-block:: text

            # e.g.
            #---------------------------------------------------
            # Benchmarking Uniband
            # #processes = 4
            #---------------------------------------------------
                   #bytes #repetitions   Mbytes/sec      Msg/sec
                        0         1000         0.00      9233819

        """
        for variant, subvars in self.variant_attrs.items():
            for var_name, ind in subvars:
                for msg_len in self.msg_lens:
                    if 'bw' in var_name:
                        self.perf_variables['_'.join([var_name, msg_len])] = self.extract_bw(
                            msg_len=int(msg_len), var=variant, ind=ind
                        ) 
                    if 'tp' in var_name:
                        self.perf_variables['_'.join([var_name, msg_len])] = self.extract_time(
                            msg_len=int(msg_len), var=variant, ind=ind
                        )

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf variables"""
        self.reference = {
            '*': {
                '*': (None, None, None, 'Mbytes/sec'),
            }
        }


@rfm.simple_test
class ImbPingpongTest(RunOnlyBenchmarkBase, ImbMixin, MultiRunMixin):
    """Main class of IMB Pingpong test"""

    descr = 'Intel MPI benchmark PingPong test to measure latency and bandwidth of interconnects'

    # Variables to define number of nodes
    # Pingpong test is always done with 2 nodes
    num_nodes = variable(int, value=2)

    @run_before('run')
    def set_executable_opts(self):
        """Set executable options"""
        # Use the -off_cache flag to avoid cache re-use
        self.executable_opts = [
            f'-mem {self.mem}', f'-time {self.timeout}',
            '-msglen ./msglens',
            'PingPong',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. We override the method in Mixin Class. Example stdout:

        .. code-block: text

            # e.g.
            # #----------------------------------------------------------------
            # # Benchmarking Alltoall
            # # #processes = 60
            # #----------------------------------------------------------------
            # We are running two versions of each test. So we will have each statement twice in
            # stdout except for Pingpong
            # We are running three tests in total so we should get three MPI_Finalize statements

        """
        self.sanity_patterns = sn.all(
            [sn.assert_found(f'# Benchmarking PingPong', self.stdout)] +
            [sn.assert_found(f'# All processes entering MPI_Finalize', self.stdout)]
                                      )

    @run_before('performance')
    def set_perf_patterns(self):
        """
        Set performance variables. We override the method in Mixin Class. Sample stdout

        .. code-block:: text

            # e.g.
            # #---------------------------------------------------
            # # Benchmarking PingPong
            # # #processes = 2
            # #---------------------------------------------------
            #        #bytes #repetitions      t[usec]   Mbytes/sec
            #             0         1000         3.51         0.00
            # #---------------------------------------------------

        """
        self.perf_variables = {
            'PingPong_bw_0': self.extract_bw(),
            'PingPong_lat_0': self.extract_time(),
        }
        for msg_len in self.msg_lens[1:]:
            self.perf_variables['PingPong_bw_' + msg_len] = self.extract_bw(
                msg_len=int(msg_len), var='PingPong', ind=3
            )
            self.perf_variables['PingPong_lat_' + msg_len] = self.extract_time(
                msg_len=int(msg_len), var='PingPong', ind=2
            )


@rfm.simple_test
class ImbOneCoreTests(RunOnlyBenchmarkBase, ImbMixin, MultiRunMixin):
    """Main class of all IMB variants tests using one core per node"""

    descr = ('Intel MPI benchmarks to measure latency and bandwidth of interconnects '
             'using single core per node')

    # Variables to define number of nodes
    tot_nodes = variable(int, value=int(os.getenv('IMBTEST_NODES', 2)))

    # Parameterise number of nodes
    # Find the closest power of 2
    expo = math.floor(math.log(int(tot_nodes)) / math.log(2)) + 1
    num_nodes = parameter(1 << i for i in range(1, int(expo)))


@rfm.simple_test
class ImbAllCoreTests(RunOnlyBenchmarkBase, ImbMixin, MultiRunMixin):
    """Main class of IMB variants tests using all cores per node"""

    descr = ('Intel MPI benchmarks to measure latency and bandwidth of interconnects '
             'using alls cores per node')

    # Variables to define number of nodes
    tot_nodes = variable(int, value=int(os.getenv('IMBTEST_NODES', 2)))

    # Parameterise number of nodes
    # Find the closest power of 2
    expo = math.floor(math.log(int(tot_nodes)) / math.log(2)) + 1
    num_nodes = parameter(1 << i for i in range(1, int(expo)))

    @run_after('setup')
    def set_num_tasks(self):
        """Set number of tasks for job"""
        self.num_tasks_per_node = (self.current_partition.processor.num_cpus //
                                   self.current_partition.processor.num_cpus_per_core)
        self.num_tasks = self.num_nodes * self.num_tasks_per_node
