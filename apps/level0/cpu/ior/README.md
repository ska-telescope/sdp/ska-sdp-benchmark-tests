# IOR benchmark

This app contains IOR benchmark test. More details on benchmark, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#ior-benchmark).

The Jupyter notebook `IOR.ipynb` can be used to tabulate and plot read and write bandwidths that are extracted during benchmarking tests. The notebook must be invoked from the `ska-sdp-benchmark-tests` root directory.
