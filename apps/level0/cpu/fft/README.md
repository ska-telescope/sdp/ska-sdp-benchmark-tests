# Basic numpy operations benchmarks

This app contains the benchmarks for basic numpy operations. More details on implemented benchmarks, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#sample-numpy-operations).

The Jupyter notebook `NUMPY.ipynb` can be used to plot all the benchmark metrics that are extracted during benchmarking tests. The notebook must be invoked from the current directory. If not, please change the repository root and ReFrame root directories in the notebook preamble.
