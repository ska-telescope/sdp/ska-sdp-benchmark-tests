# pylint: disable=C0301
"""
STREAM benchmark
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

STREAM is used to measure the sustainable memory bandwidth of high performance computers.
The source code is available `here <http://www.cs.virginia.edu/stream/ref.html>`__.

.. note::
  Currently, the implemented test uses only Intel compiler that is optimized for Intel processors.
  A generic GNU compiled stream test will be added in the future.

Test configuration
~~~~~~~~~~~~~~~~~~~~~~~~

STREAM benchmark uses 3 arrays of size ``N`` to perform different kernels. The most relevant and
interesting kernel is "Triad" kernel. In the test we use the size of the arrays in such a way that
they occupy 60 % of the system memory. In this way, we are sure that caching effects are avoided
while running the benchmark.

The ``Makefile`` in the ``src/`` folder contains all the optimized compiler flags used for Intel
compiler to extract maximum peformance.


Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/stream/reframe_stream.py --run --performance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
from modules.base_class import BenchmarkBase
from modules.reframe_extras import MultiRunMixin
from reframe.core.backends import getlauncher  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error

from modules.utils import filter_systems_by_env
from reframe.core.builtins import variable, parameter, run_after, run_before, performance_function


# pylint: disable=C0116,E0401,E0602,E1101,W0201
        
        
@rfm.simple_test
class StreamTest(BenchmarkBase, MultiRunMixin):
    """Main class of Stream test based on Intel compiler"""

    descr = 'Stream memory bandwidth benchmark'

    # Variables to define number of nodes
    num_nodes = variable(int, value=1)
    
    # Variants of the test
    toolchain = parameter(['intel', 'gnu'])

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            f'{self.toolchain}-stream'
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        self.exclusive_access = True
        self.time_limit = '0d1h00m0s'
        # Cross compilation might not be always possible on Grid50000 clusters
        if 'g5k' in self.current_system.name:
            self.build_locally = False

    @run_after('setup')
    def set_git_commit_tag(self):
        """Fetch git commit hash"""
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {f'git={git_ref}'}

    @run_after('setup')
    def set_num_tasks_reservation(self):
        """Set number of tasks for job reservation"""
        # This method sets number of tasks for the reservation. Here ONLY resources are reserved
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
        else:
            self.num_tasks_per_node = self.current_partition.processor.num_cpus
        self.num_tasks = self.num_nodes * self.num_tasks_per_node

    @run_after('setup')
    def set_env_vars(self):
        """Set OpenMP environment variables"""
        num_physical_cores = (self.current_partition.processor.num_cpus //
                              self.current_partition.processor.num_cpus_per_core)
        self.env_vars = {
            'OMP_NUM_THREADS': str(num_physical_cores),
            'KMP_AFFINITY': 'granularity=fine,compact,1,0',
        }

    @run_after('setup')
    def get_array_size(self):
        """Set array size to be 60% of main memory"""
        try:
            sys_memory = int(self.current_partition.extras['mem'])
        except (KeyError, AttributeError):
            self.skip(msg='Memory info in system partition not found. '
                          'Please add memory info at current_partition.extras["mem"]')
        self.array_size = int(0.6 * sys_memory / 8 / 3)

    @run_after('setup')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'array_size={self.array_size}',
        }

    @run_after('setup')
    def set_launcher(self):
        """Set launcher to local to avoid appending mpirun or srun"""
        self.job.launcher = getlauncher('local')()

    @run_before('compile')
    def build_executable(self):
        """Set build system and config options"""
        self.build_system = 'Make'
        self.build_system.options = [
            f'stream.{self.toolchain}',
            f'CXXFLAGS=\"-DSTREAM_ARRAY_SIZE={self.array_size}\"',
        ]

    @run_before('run')
    def export_env_vars(self):
        """Export env variables using OMPI_MCA param for OpenMPI"""
        # This is needed for g5k clusters as env vars are not exported to all nodes in
        # the reservation
        self.env_vars['OMPI_MCA_mca_base_env_list'] = \
            f'\"PATH;LD_LIBRARY_PATH;{";".join(self.env_vars.keys())}\"'

    @run_before('run')
    def set_executable(self):
        """Set executable"""
        self.executable = f'./stream.{self.toolchain}'

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:

        .. code-block:: text

            # -------------------------------------------------------------
            # Solution Validates: avg error less than 1.000000e-13 on all three arrays
            # -------------------------------------------------------------

        """
        self.sanity_patterns = sn.assert_found(
            'Solution Validates: avg error less than', self.stdout
        )

    @performance_function('MB/s')
    def extract_bw(self, kind='Copy'):
        """
        Performance function to extract bandwidth. Sample stdout:

        .. code-block:: text

            # Function    Best Rate MB/s  Avg time     Min time     Max time
            # Copy:           42037.6     0.003859     0.003806     0.004004
            # Scale:          41047.7     0.003917     0.003898     0.003942
            # Add:            45138.5     0.005347     0.005317     0.005372
            # Triad:          46412.1     0.005202     0.005171     0.005238

        """
        return sn.extractsingle(rf'{kind}:\s+(?P<value>\S+)\s+\S+', self.stdout, 'value', float)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            'Copy': self.extract_bw(),
            'Scale': self.extract_bw(kind='Scale'),
            'Add': self.extract_bw(kind='Add'),
            'Triad': self.extract_bw(kind='Triad'),
        }

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf values"""
        self.reference = {
            '*': {
                'Copy': (None, None, None, 'MB/s'),
                'Scale': (None, None, None, 'MB/s'),
                'Add': (None, None, None, 'MB/s'),
                'Triad': (None, None, None, 'MB/s'),
            }
        }
