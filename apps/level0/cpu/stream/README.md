# STREAM benchmark

This app contains STREAM memory bandwidth benchmark test. More details on benchmark, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#stream-benchmark).

The Jupyter notebook `STREAM.ipynb` can be used to tabulate and plot memory banddwidths for different kernels that are extracted during benchmarking tests. The notebook must be invoked from the `ska-sdp-benchmark-tests` root directory.
