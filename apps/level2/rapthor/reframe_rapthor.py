# pylint: disable=C0301
"""
Rapthor benchmark
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

This test runs the steps of Rapthor, the SKA-low observation pipeline whose source code can be found here : https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal

Test parameterisation
~~~~~~~~~~~~~~~~~~~~~~~~

Currently, the test supports one parametrization corresponding to the steps that one wants to perform in the pipeline. "full" value stands for running the whole pipeline while "calibrate_1" and "predict_1" only run the corresponding steps. One must therefore always specify a tag as there is no point in both running the full pipeline and part of it.

Environment variables
~~~~~~~~~~~~~~~~~~~~~~~~

Currently, this test does not install the Rapthor and its dependencies. Two environment variables must therefore be specified :
-RAPTHOR_DIR corresponding to the installation path of Rapthor.
-INPUT_MS_PATH corresponding to the path of the input data folder (often called midbands.ms)
Furthermore the dependencies must have been previously installed/loaded. It can be done by following the recipe detailed in the README.md

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test (performing only the first calibration step) can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level2/rapthor --run --tag calibrate_1 --performance-report


But first let's see the tests generated by ReFrame using ``--list`` command as follows:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level2/rapthor --list

If one wants to run the whole pipeline, we can use the value "full" and the``--tags`` CLI option to do so.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level2/rapthor --tag full --run --performance-report



Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301


import reframe as rfm
import reframe.utility.sanity as sn
from reframe.core.backends import getlauncher
from modules.base_class import RunOnlyBenchmarkBase
from modules.utils import filter_systems_by_env, emit_conda_activate_cmds
from reframe.core.builtins import parameter

ENV_NAME = "rfm_RapthorTest"

@rfm.simple_test
class RapthorRunTest(RunOnlyBenchmarkBase):

    mode = parameter(['full','calibrate_1','predict_1'])

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [ 'rapthor', ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)

        self.maintainer = [ 'Aristide Doussot (aristide.doussot@obspm.fr)' ]

        self.tags |= {self.mode}

    @run_before('run')
    def set_launcher(self):
        """Set launcher to local to avoid appending mpirun or srun to python command"""
        self.job.launcher = getlauncher('local')()

    @run_before('run')
    def setup_env(self):
        """Setup external schaap-spack environment"""
        self.prerun_cmds += [
            'module purge',
            'module load gcc/9.3.0/gcc/9.4.0',
            'module load gcc/9.4.0/dp3/5.4',
            'module load gcc/9.4.0/wsclean/3.2',
            'module load gcc/9.4.0/py-pip/21.3.1',
            'module load gcc/9.4.0/py-numpy/1.22.4',
            'module load gcc/9.4.0/python/3.9.12',
            'module load gcc/9.4.0/py-setuptools/59.4.0',
            'module load gcc/9.4.0/py-cython/0.29.30',
            'module load gcc/9.4.0/py-wheel/0.37.0',
            'module load gcc/9.4.0/dysco/1.2',
            'source $RAPTHOR_DIR/venv/bin/activate',
            'export TMP_CONDA_PREFIX=$CONDA_PREFIX', #quick fix so that everybeam files required in DP3 are not looked after in miniconda3/envs but in spack packages
            'unset CONDA_PREFIX'
        ]
        self.postrun_cmds += [
            'export CONDA_PREFIX=$TMP_CONDA_PREFIX',
            'unset TMP_CONDA_PREFIX',
        ]



    @run_before('run')
    def set_executable(self):
        """Set executable and its options"""
        self.executable = 'python3'
        self.executable_opts = ['$RAPTHOR_DIR/src/ska_sdp_wflow_low_selfcal/pipeline/main.py',
                                '--dp3_path $DP3_SKA_ROOT/bin/DP3',
                                '--wsclean_path $WSCLEAN_ROOT/bin/wsclean',
                                '--input_ms $INPUT_MS_PATH',
                                '--work_dir $RAPTHOR_DIR/rapthor_working_dir',
                                '--imaging_taper_gaussian 0.004deg',
                                '--imaging_size 5000',
                                '--imaging_scale 0.001658792',
                                '--calibration_nchannels 1',
                                ]
        if self.mode in ["predict_1","calibrate_1"]:
            self.executable_opts += [f'--resume_from_operation {self.mode}',
                                     '--run_single_operation True',]
        if self.mode == "full":
            self.executable_opts += ['--run_single_operation False',]
            
        
    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns"""
        self.sanity_patterns = sn.all([
            sn.assert_not_found('error', self.stderr),
            sn.assert_found(f'Done {self.mode}', self.stderr),
        ])
