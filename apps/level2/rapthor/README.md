# Rapthor pipeline benchmark

    This app contains the benchmark for the SKA-low pipeline named Rapthor.

## Dependency issue

    The app does not currently install the code nor its dependencies and only use what must already be setup in the cluster. We will therefore present here a list of instructions to install everything that is required, namely :

    - The Rapthor code (https://confluence.skatelescope.org/pages/viewpage.action?spaceKey=SE&title=Initial+Tests+Rapthor+Calibration+Pipeline)
    - Its dependencies using the Spack package manager (https://confluence.skatelescope.org/pages/viewpage.action?pageId=213080719)
    - The input data (https://confluence.skatelescope.org/pages/viewpage.action?spaceKey=SE&title=Initial+Tests+Rapthor+Calibration+Pipeline)

    Please note that in the following <name> must be replace by the user choice of name or system-dependant information.

### Rapthor dependencies installation

    This section is a summary of the installation process detailed in https://confluence.skatelescope.org/pages/viewpage.action?pageId=213080719 . As the required LOFAR codes and the Spack package manager are frequently updated, issues may occur when using recently updated version. We thus settle on given versions of all the dependecies up to the GCC compiler itself, all of them being installed using Spack v0.18.1. For issue management as well as performance comparison we advise the user to also use the same versions.

    **Spack installation**

```bash
    git clone https://github.com/spack/spack.git -b v0.18.1 <new spack directory>
    source <new spack directory>/share/spack/setup-env.sh
```

    **GCC 9.4.0 installation**

```bash
    spack env create <gcc940>
    spack env activate <gcc940> -p
```

    Then modify the newly created environment yaml file (`./var/spack/environments/gcc940/spack.yaml`):
```
    spack:
    specs:
    - matrix:
        - [gcc@9.4.0]
        - ['%gcc@<version available on your system>']
    view: false
    concretizer:
        unify: true
    modules:
        default:
        enable:
        - tcl
        tcl:
            hash_length: 0
            naming_scheme: ${COMPILERNAME}/${COMPILERVER}/${PACKAGE}/${VERSION}
            all:
            environment:
                set:
                '{name}_ROOT': '{prefix}'
```    

    Then we let Spack find the compiler, install the environment and we refresh the module list

```bash
    spack compiler find
    spack install [-j <number of core available>]
    spack module tcl refresh -y
    source <new spack directory>/share/spack/setup-env.sh
    spack env deactivate
```

    **LOFAR softwares installation**

    Load the compiler, create the environment and add the spack recipe for dependencies installation (named schaap-spack)

```bash
    module load gcc/<version available on your system>/gcc/9.4.0
    spack env create <lofar>
    spack env activate <lofar> -p
    mkdir git@git.astron.nl:RD/schaap-spack.git <spack recipe directory>
```
    Then modify the newly created environment yaml file (`./var/spack/environments/lofar/spack.yaml`) to specify which software to install:
```
spack:
  specs:
  - matrix:
    - [hdf5+cxx+hl+threadsafe~mpi, wsclean@3.2^idg@1.1.0+cuda+python, dp3@5.4, dysco@1.2]
    - ['%gcc@9.4.0']
  view: false
  concretizer:
    unify: true
  modules:
    default:
      enable:
      - tcl
      tcl:
        hash_length: 0
        naming_scheme: ${COMPILERNAME}/${COMPILERVER}/${PACKAGE}/${VERSION}
        all:
          environment:
            set:
              '{name}_ROOT': '{prefix}'
```

    Then we add the recipe to spack, let spack find the installed compiler, install the environment and we refresh the module list

```bash
    spack compiler find
    spack install [-j <number of core available>]
    spack module tcl refresh -y
    source <new spack directory>/share/spack/setup-env.sh
    spack env deactivate
```

### Rapthor installation

    One just need to clone the repository on which the Rapthor pipeline is currently developped here

```bash
    git clone --recursive git@gitlab.com:ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal.git
```

### Input data

    There are two types of datasets available a standard one and the average one. The average data is lighter and its computation faster, it is obtained by averaging the standard dataset over frequencies. Both can be found here :
    https://console.cloud.google.com/storage/browser/ska1-simulation-data/Rapthor%20test%20data%20set?pageState=(%22StorageObjectListTable%22:(%22f%22:%22%255B%255D%22))&prefix=&forceOnObjectsSortingFiltering=false

    In any case one also need to download the input skymodels here : https://confluence.skatelescope.org/display/SE/Initial+Tests+Rapthor+Calibration+Pipeline

### Cluster setup

    Now that every dependency is setup, the last step is to setup the environment, specify the path to the input data (INPUT_MS_PATH) and the path to the Rapthor code (RAPHTOR_DIR). It can be done in multiple ways but one way is to perform all these actions in the "prepare_cmds" variable in the cluster config file.

```python
            'prepare_cmds': [
                'export RAPTHOR_DIR=<path/to/rapthor/folder>',
                'export INPUT_MS_PATH=<path/to/input/data/folder>',
                'source <new spack directory>/share/spack/setup-env.sh',
            ],
```